jQuery(document).ready(function () {

                if (jQuery(window).width() < 768){
                    jQuery(".chart").addClass("chart-mobile");
                    jQuery(".chart").removeClass("chart");
                } else {
                    jQuery(".chart-mobile").addClass("chart");
                    jQuery(".chart-mobile").removeClass("chart-mobile");
                }
            jQuery(window).resize(function(){
                if (jQuery(window).width() < 768){
                    jQuery(".chart").addClass("chart-mobile");
                    jQuery(".chart").removeClass("chart");
                } else {
                    jQuery(".chart-mobile").addClass("chart");
                    jQuery(".chart-mobile").removeClass("chart-mobile");
                }
            });

            jQuery(".add-to-contacts").click(function(){
                jQuery(".add-to-contacts").text("Added");
                jQuery(".add-to-contacts").css("color", "#e8ecf3");
            });

            jQuery(".services_self_block > .col-md-12 > img").click(function(){
                jQuery(this).parent().parent().remove();
            });
            jQuery(".stuffs_self_block > .col-md-12 > img").click(function(){
                jQuery(this).parent().parent().remove();
            });

            jQuery(".een_page_menu li > a").click(function(event){
            	jQuery(this).parent().siblings().removeClass("active_een_menu_item");
            	jQuery(this).parent().addClass("active_een_menu_item");
				event.stopPropagation();
            });


            if(jQuery(window).width() < 768){
                jQuery("#upload_avatar").attr("type","");
                jQuery(".label_upload_avatar").click(function(){
                    jQuery(".choose-picture").removeClass("close-block");
                    jQuery(".choose-picture").css("display", "block");
                });
            } else {
                jQuery("#upload_avatar").attr("type","file");
            }
            jQuery(window).resize(function(){
                jQuery("#upload_avatar").attr("type","");
                if(jQuery(window).width() < 768){
                    jQuery(".label_upload_avatar").click(function(){
                        jQuery(".choose-picture").removeClass("close-block");
                        jQuery(".choose-picture").css("display", "block");
                    });
                } else {
                    jQuery("#upload_avatar").attr("type","file");
                }
            });

            jQuery("#input_registration-code").bind("keypress", function(event){
                var regex = new RegExp("^[0-9]+$");
                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                if (!regex.test(key)) {
                   event.preventDefault();
                   return false;
                }
            });

            jQuery("#input_year").bind("keypress", function(event){
                var regex = new RegExp("^[0-9]+$");
                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                if (!regex.test(key)) {
                   event.preventDefault();
                   return false;
                }
            });
            jQuery("#input_export-percent").bind("keypress", function(event){
                var regex = new RegExp("^[0-9]+$");
                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                if (!regex.test(key)) {
                   event.preventDefault();
                   return false;
                }
            });

            jQuery("#input_registration-code").on("keyup", function(){
                if (jQuery(this).val().length > 10){
                    jQuery(this).val(jQuery(this).val().substr(0, 10));
                }
                if (jQuery(this).val().length == 8 || jQuery(this).val().length == 10) {
                    jQuery(this).parent().parent().find(".error_data").css("display", "none");
                } else {
                    jQuery(this).parent().parent().find(".error_data").css("display", "block");
                }
            });
            jQuery("#input_year").on("keyup", function(){
                var mdate = new Date();
                if (jQuery(this).val().length > 4){
                    jQuery(this).val(jQuery(this).val().substr(0, 4));
                }
                if (jQuery(this).val().length == 4){
                    if (jQuery(this).val() < 1900){
                        jQuery(this).val(1900);
                    }
                    if (jQuery(this).val() > mdate.getFullYear()){
                        jQuery(this).val(mdate.getFullYear());
                    }  
                }
            });
            jQuery("#input_export-percent").on("keyup", function(){
                if (jQuery(this).val() < 0){
                    jQuery(this).val(0);
                }
                if (jQuery(this).val() > 100){
                    jQuery(this).val(100);
                }
            });

            jQuery(".list-kved ul").hide();
            jQuery(".list-kved li").each(function() {
                if(!jQuery(this).children("ul").length){
                    jQuery(this).addClass("compress-choose");
                    if ( jQuery(this).hasClass("expand-choose") || jQuery(this).hasClass("compress-choose")){   
                        jQuery(this).bind("click", function(event){
                            jQuery(this).toggleClass("compress-choose");
                            jQuery(this).toggleClass("expand-choose");
                            if( jQuery(".expand-choose").length > 0 ){
                                jQuery(this).addClass("selected");
                                jQuery(this).parent().addClass("selected");
                            } else {
                                jQuery(this).removeClass("selected");
                                jQuery(this).parent().removeClass("selected");
                            }
                            event.stopPropagation();
                        });
                    }   
                } else {
                    jQuery(this).addClass("compress");
                    if ( jQuery(this).hasClass("expand") || jQuery(this).hasClass("compress")){ 
                        jQuery(this).bind("click", function(event){         
                            jQuery(this).children("ul").slideToggle(300);
                            jQuery(this).toggleClass("expand");
                            jQuery(this).toggleClass("compress");
                            event.stopPropagation();
                        });
                    }   
                }
            });

            jQuery("ul.selected").parent("li").css("color","red");

            if ( jQuery(".user_info_email_blocks").find(".inputs_blocks").height() > 70 ){
                jQuery(".user_info_email_blocks").find(".inputs_blocks").addClass("active_inputs_blocks");
            } else {
                jQuery(".user_info_email_blocks").find(".inputs_blocks").removeClass("active_inputs_blocks");
            }
            if ( jQuery(".user_info_feedback_input").find(".inputs_blocks").height() > 70 ){
                jQuery(".user_info_feedback_input").find(".inputs_blocks").addClass("active_inputs_blocks");
            } else {
                jQuery(".user_info_feedback_input").find(".inputs_blocks").removeClass("active_inputs_blocks");
            }
            if ( jQuery(".user_info_phone_blocks").find(".inputs_blocks").height() > 70 ){
                jQuery(".user_info_phone_blocks").find(".inputs_blocks").addClass("active_inputs_blocks");
            } else {
                jQuery(".user_info_phone_blocks").find(".inputs_blocks").removeClass("active_inputs_blocks");
            }
            if ( jQuery(".user_info_skype_blocks").find(".inputs_blocks").height() > 70 ){
                jQuery(".user_info_skype_blocks").find(".inputs_blocks").addClass("active_inputs_blocks");
            } else {
                jQuery(".user_info_skype_blocks").find(".inputs_blocks").removeClass("active_inputs_blocks");
            }
            if ( jQuery(".user_info_work_phone_blocks").find(".inputs_blocks").height() > 70 ){
                jQuery(".user_info_work_phone_blocks").find(".inputs_blocks").addClass("active_inputs_blocks");
            } else {
                jQuery(".user_info_work_phone_blocks").find(".inputs_blocks").removeClass("active_inputs_blocks");
            }

        jQuery(".choose-button").click(function(){
            jQuery(this).parent().parent().find(".avatar-block__choose:nth-child(2)").css("display", "block");
        });
        jQuery(".avatar-block__choose:nth-child(1)").find(".close-button").click(function(){
            jQuery(this).parent().addClass("close-block");
        });
        jQuery(".avatar-block__choose:nth-child(2)").find(".close-button").click(function(){
            jQuery(this).parent().css("display", "none");
        });
        
        /* раскрывание языкового меню */
        jQuery(".langs").click(function(){
            jQuery(".langs").toggleClass("lang_menu_triger");
        });

        /* перенос в меню языкового переключателя */
        if (jQuery(window).width() < 769) {
            jQuery("#menu-primary-menu-1").append(jQuery(".langs"));
            jQuery("#menu-primary-menu-1").append(jQuery(".phone_number_in_mob_menu"));
        }

        /* кнопка меню */
        jQuery('#nav-icon2').click(function(){
            jQuery(this).toggleClass('open');
        });

        jQuery(".dropdownmenu").click(function(){
            jQuery(this).toggleClass("open_header_menu");
        });

        jQuery("body").on("click", ".navbar-toggle", function () {
            jQuery(".navbar-toggle").toggleClass("active_menu_button");
            jQuery(".navbar_toggle_name").text("Закрити меню");
            jQuery(".navbar_toggle_name").css("width","69px");
            jQuery(".langs").css("visibility","visible");
        });

        jQuery("body").on("click", ".active_menu_button", function () {
            jQuery(".navbar_toggle_name").text("Меню сайту");
            jQuery(".navbar_toggle_name").css("width","58px");
        });

        var zerif_footer_phone = jQuery(".zerif-footer-phone").text();      

        jQuery(".phone_number_in_mob_menu").text(zerif_footer_phone);
        

        /* поппапы при регистрации/авторизации */
        jQuery(".navbar-toggle").click(function(){
            jQuery(".autorization_overflow").fadeOut(); 
            jQuery(".sign_in_popup").fadeOut(); 
            jQuery(".forgot_password_in_popup").fadeOut();
            jQuery(".enter_autorization").removeClass("active_autorization_button");    
            jQuery(".registration_autorization").removeClass("active_autorization_button");
            jQuery(".repair_password_succes_popup").fadeOut();  
            jQuery(".registration_on_epo").fadeOut();
        });

        jQuery(".enter_autorization").click(function(){
            jQuery(".enter_autorization").addClass("active_autorization_button");
            jQuery(".registration_autorization").removeClass("active_autorization_button");
            jQuery(".autorization_overflow").fadeIn();
            jQuery(".sign_in_popup").fadeIn();  
            jQuery(".registration_on_epo").fadeOut(0);          
        });

        jQuery(".registration_autorization").click(function(){
            jQuery(this).addClass("active_autorization_button");
            jQuery(".autorization_overflow").fadeIn();          
        });

        jQuery(".autorization_overflow").click(function(){
            jQuery(".autorization_overflow").fadeOut(); 
            jQuery(".sign_in_popup").fadeOut();     
            jQuery(".enter_autorization").removeClass("active_autorization_button");    
            jQuery(".registration_autorization").removeClass("active_autorization_button");
            jQuery(".registration_on_epo").fadeOut();   
        });

        jQuery(".close_autorization_popup").click(function(){
            jQuery(".autorization_overflow").fadeOut(); 
            jQuery(".sign_in_popup").fadeOut(); 
            jQuery(".forgot_password_in_popup").fadeOut();
            jQuery(".enter_autorization").removeClass("active_autorization_button");    
            jQuery(".registration_autorization").removeClass("active_autorization_button");
            jQuery(".repair_password_succes_popup").fadeOut();  
            jQuery(".registration_on_epo").fadeOut();   
        })

        jQuery(".autorization_password_input_wrapp").find(".fa-eye-slash").click(function(){
            jQuery(this).parent().find("input").attr("type", "text");
            jQuery(this).fadeOut(0);
            jQuery(".autorization_password_input_wrapp").find(".fa-eye").fadeIn(0);
        });

        jQuery(".autorization_password_input_wrapp").find(".fa-eye").click(function(){
            jQuery(this).parent().find("input").attr("type", "password");
            jQuery(this).fadeOut(0);
            jQuery(".autorization_password_input_wrapp").find(".fa-eye-slash").fadeIn(0);
        });

        jQuery(".autorization_forgot_password").click(function(){
            jQuery(".sign_in_popup").fadeOut(0);
            jQuery(".forgot_password_in_popup").fadeIn();
            jQuery(".autorization_overflow").fadeIn(0); 
        });

        jQuery(".forgot_password_in_popup").find("img").eq(0).click(function(){
            jQuery(".autorization_overflow").fadeIn(0); 
            jQuery(".sign_in_popup").fadeIn();
            jQuery(".forgot_password_in_popup").fadeOut(0);
        });

        jQuery(".repair_password_succes_popup").find("img").eq(0).click(function(){
            jQuery(".repair_password_succes_popup").fadeOut(0); 
            jQuery(".forgot_password_in_popup").fadeIn();
        });

        jQuery(".registration_autorization").click(function(){
            jQuery(".registration_autorization").addClass("active_autorization_button");
            jQuery(".enter_autorization").removeClass("active_autorization_button");
            jQuery(".autorization_overflow").fadeIn();
            jQuery(".registration_on_epo").fadeIn();
            jQuery(".sign_in_popup").fadeOut(0);                
        });

        /* меню событий */
        var mh = 0;
        jQuery(".event_block").each(function () {
            var h_block = parseInt(jQuery(this).height());
            if(h_block > mh) {
                mh = h_block;
            };
        });
        jQuery(".event_block").height(mh);

        if (jQuery(window).width() < 610) {
            var acth = jQuery(".active_event_block").height();
            jQuery(".active_event_block").height(acth-2);
        }


        jQuery(".service_carousel_item_description").text().slice(0,100);

        jQuery(".active_event_block").height();

        /* стилизация селектов */
        jQuery(".about_user_sex").find(".jq-selectbox__select").click(function(){
            //jQuery(this).toggleClass("clicked_select");
        });

        jQuery(".about_user_sex").find(".jq-selectbox__select").focusout(function(){
            jQuery(this).removeClass("clicked_select");
        });

        jQuery("body").on("click", ".jq-selectbox__select", function () {
            //jQuery(this).toggleClass("clicked_select");
        });

        jQuery("body").on("focusout", ".jq-selectbox__select", function () {
            jQuery(this).removeClass("clicked_select");
        });

        jQuery(".about_user_sex").find("select").styler();

        jQuery(".company_info_year_input").find("select").styler();

        jQuery(".about_company_place").find("select").styler();

        jQuery(".single_service_languge_skills").find("select").styler();

        jQuery(".about_company_place_city").find("select").styler();

        jQuery(".user_info_company_workplace_select").styler();

        jQuery(".user_info_company_workplace_select_aside").styler();

        jQuery(".company_info_full_property_input").find("select").styler();

        jQuery(".company_full_workers_input").find("select").styler();

        jQuery(".company_info_full_turnover").find("select").styler();

        jQuery(".company_stage_select").find("select").styler();

        /*jQuery(".language_skill_wrapp").focusin(function(){
            jQuery(this).removeClass("default_language_skill_wrapp");
        });

        jQuery(".language_skill_wrapp").focusout(function(){
            jQuery(this).addClass("default_language_skill_wrapp");
        });*/

        /* Мультиселект */
        jQuery('.select_multiple').select2();
        /* Мультиселект для языков */
        jQuery('.langs_multiple').select2();

        jQuery('.langs_multiple').select2({allowClear: true, placeholder: 'Введіть мови якими володієте'});
        jQuery('.select2').css('width', '100%');

        /* Добавление полей ввода на странице пользователя */

        jQuery(".user_info_email_blocks").find(".text_after_plus_icon").click(function(){
            jQuery(this).parent().find(".inputs_blocks").append("<div class='inputs_block_inner inputs_block_inner_email'><input type='email' class='col-md-12 col-sm-12 col-xs-12 email'><div class='remove_this_input'><img src='http://projects.make-web.org/epo/wp-content/themes/EPO/images/thrash.svg' alt=''></div><span class='setp_main_email'>Встановити як основний</span></div>" );
            if ( jQuery(this).parent().find(".inputs_blocks").height() > 70 ){
                jQuery(this).parent().find(".inputs_blocks").addClass("active_inputs_blocks");
            } else {
                jQuery(this).parent().find(".inputs_blocks").removeClass("active_inputs_blocks");
            }
        });

        jQuery(".user_info_alernate_email_blocks").find(".text_after_plus_icon").click(function(){
            jQuery(this).parent().find(".inputs_blocks").append("<div class='inputs_block_inner inputs_block_inner_alternate_email'><input type='email' class='col-md-12 col-sm-12 col-xs-12 email'><div class='remove_this_input'><img src='http://projects.make-web.org/epo/wp-content/themes/EPO/images/thrash.svg' alt=''></div></div>" );
            if ( jQuery(this).parent().find(".inputs_blocks").height() > 70 ){
                jQuery(this).parent().find(".inputs_blocks").addClass("active_inputs_blocks");
            } else {
                jQuery(this).parent().find(".inputs_blocks").removeClass("active_inputs_blocks");
            }
        });

        jQuery(".user_info_alernate_phone_blocks").find(".text_after_plus_icon").click(function(){
            jQuery(this).parent().find(".inputs_blocks").append("<div class='inputs_block_inner inputs_block_inner_phone'><input type='email' class='col-md-12 col-sm-12 col-xs-12 email'><div class='remove_this_input'><img src='http://projects.make-web.org/epo/wp-content/themes/EPO/images/thrash.svg' alt=''></div></div>" );
            if ( jQuery(this).parent().find(".inputs_blocks").height() > 70 ){
                jQuery(this).parent().find(".inputs_blocks").addClass("active_inputs_blocks");
            } else {
                jQuery(this).parent().find(".inputs_blocks").removeClass("active_inputs_blocks");
            }
        });

        jQuery(".user_info_website_blocks").find(".text_after_plus_icon").click(function(){
            jQuery(this).parent().find(".inputs_blocks").append("<div class='inputs_block_inner inputs_block_inner_website'><input type='text' class='col-md-12 col-sm-12 col-xs-12 website'><div class='remove_this_input'><img src='http://projects.make-web.org/epo/wp-content/themes/EPO/images/thrash.svg' alt=''></div></div>" );
            if ( jQuery(this).parent().find(".inputs_blocks").height() > 70 ){
                jQuery(this).parent().find(".inputs_blocks").addClass("active_inputs_blocks");
            } else {
                jQuery(this).parent().find(".inputs_blocks").removeClass("active_inputs_blocks");
            }
        });

        jQuery(".user_info_email_blocks_without").find(".text_after_plus_icon").click(function(){
            jQuery(this).parent().find(".inputs_blocks").append("<div class='inputs_block_inner inputs_block_inner_website'><input type='text' class='col-md-12 col-sm-12 col-xs-12 website'><div class='remove_this_input'><img src='http://projects.make-web.org/epo/wp-content/themes/EPO/images/thrash.svg' alt=''></div></div>" );
            if ( jQuery(this).parent().find(".inputs_blocks").height() > 70 ){
                jQuery(this).parent().find(".inputs_blocks").addClass("active_inputs_blocks");
            } else {
                jQuery(this).parent().find(".inputs_blocks").removeClass("active_inputs_blocks");
            }
        });

        jQuery(".user_info_email_blocks").find(".pluss_icon").click(function(){
            jQuery(this).parent().find(".inputs_blocks").append("<div class='inputs_block_inner inputs_block_inner_email'><input type='email' class='col-md-12 col-sm-12 col-xs-12 email'><div class='remove_this_input'><img src='http://projects.make-web.org/epo/wp-content/themes/EPO/images/thrash.svg' alt=''></div></div>" );
            if ( jQuery(this).parent().find(".inputs_blocks").height() > 70 ){
                jQuery(this).parent().find(".inputs_blocks").addClass("active_inputs_blocks");
            } else {
                jQuery(this).parent().find(".inputs_blocks").removeClass("active_inputs_blocks");
            }
        }); 

        jQuery(".user_info_email_blocks").find(".pluss_icon").click(function(){
            jQuery(this).parent().find(".inputs_blocks").append("<div class='inputs_block_inner inputs_block_inner_email'><input type='email' class='col-md-12 col-sm-12 col-xs-12 email'><div class='remove_this_input'><img src='http://projects.make-web.org/epo/wp-content/themes/EPO/images/thrash.svg' alt=''></div></div>" );
            if ( jQuery(this).parent().find(".inputs_blocks").height() > 70 ){
                jQuery(this).parent().find(".inputs_blocks").addClass("active_inputs_blocks");
            } else {
                jQuery(this).parent().find(".inputs_blocks").removeClass("active_inputs_blocks");
            }
        }); 
        jQuery(".user_info_email_blocks_single").find(".pluss_icon").click(function(){
            jQuery(this).parent().find(".inputs_blocks").append("<div class='inputs_block_inner inputs_block_inner_email_single'><input type='email' class='col-md-12 col-sm-12 col-xs-12 email'><div class='remove_this_input'><img src='http://projects.make-web.org/epo/wp-content/themes/EPO/images/thrash.svg' alt=''></div><span class='setp_main_email'>Встановити як основний</span></div>" );
            if ( jQuery(this).parent().find(".inputs_blocks").height() > 70 ){
                jQuery(this).parent().find(".inputs_blocks").addClass("active_inputs_blocks");
            } else {
                jQuery(this).parent().find(".inputs_blocks").removeClass("active_inputs_blocks");
            }
        }); 

        jQuery(".user_info_email_blocks_single").find(".text_after_plus_icon").click(function(){
            jQuery(this).parent().find(".inputs_blocks").append("<div class='inputs_block_inner inputs_block_inner_email_single'><input type='email' class='col-md-12 col-sm-12 col-xs-12 email'><div class='remove_this_input'><img src='http://projects.make-web.org/epo/wp-content/themes/EPO/images/thrash.svg' alt=''></div><span class='setp_main_email'>Встановити як основний</span></div>" );
            if ( jQuery(this).parent().find(".inputs_blocks").height() > 70 ){
                jQuery(this).parent().find(".inputs_blocks").addClass("active_inputs_blocks");
            } else {
                jQuery(this).parent().find(".inputs_blocks").removeClass("active_inputs_blocks");
            }
        }); 

        jQuery(".user_info_email_blocks_eng").find(".pluss_icon").click(function(){
            jQuery(this).parent().find(".inputs_blocks").append("<div class='inputs_block_inner inputs_block_inner_email'><input type='email' class='col-md-12 col-sm-12 col-xs-12 email'><div class='remove_this_input'><img src='http://projects.make-web.org/epo/wp-content/themes/EPO/images/thrash.svg' alt=''></div><span class='setp_main_email_eng'>Set as basic</span></div>" );
            if ( jQuery(this).parent().find(".inputs_blocks").height() > 70 ){
                jQuery(this).parent().find(".inputs_blocks").addClass("active_inputs_blocks");
            } else {
                jQuery(this).parent().find(".inputs_blocks").removeClass("active_inputs_blocks");
            }
        }); 

        jQuery(".user_info_email_blocks_eng").find(".text_after_plus_icon").click(function(){
            jQuery(this).parent().find(".inputs_blocks").append("<div class='inputs_block_inner inputs_block_inner_email'><input type='email' class='col-md-12 col-sm-12 col-xs-12 email'><div class='remove_this_input'><img src='http://projects.make-web.org/epo/wp-content/themes/EPO/images/thrash.svg' alt=''></div><span class='setp_main_email_eng'>Set as basic</span></div>" );
            if ( jQuery(this).parent().find(".inputs_blocks").height() > 70 ){
                jQuery(this).parent().find(".inputs_blocks").addClass("active_inputs_blocks");
            } else {
                jQuery(this).parent().find(".inputs_blocks").removeClass("active_inputs_blocks");
            }
        }); 


        jQuery(".user_info_phone_blocks_eng").find(".text_after_plus_icon").click(function(){
            jQuery(this).parent().find(".inputs_blocks").append( "<div class='inputs_block_inner inputs_block_inner_phone'><input type='text' class='col-md-12 col-sm-12 col-xs-12 phone'><div class='remove_this_input'><img src='http://projects.make-web.org/epo/wp-content/themes/EPO/images/thrash.svg' alt=''></div></div>" );
            if ( jQuery(this).parent().find(".inputs_blocks").height() > 70 ){
                jQuery(this).parent().find(".inputs_blocks").addClass("active_inputs_blocks");
            } else {
                jQuery(this).parent().find(".inputs_blocks").removeClass("active_inputs_blocks");
            }
        });

        jQuery(".user_info_phone_blocks_eng").find(".pluss_icon").click(function(){
            jQuery(this).parent().find(".inputs_blocks").append("<div class='inputs_block_inner inputs_block_inner_phone'><input type='text' class='col-md-12 col-sm-12 col-xs-12 phone'><div class='remove_this_input'><img src='http://projects.make-web.org/epo/wp-content/themes/EPO/images/thrash.svg' alt=''></div></div>" );
            if ( jQuery(this).parent().find(".inputs_blocks").height() > 70 ){
                jQuery(this).parent().find(".inputs_blocks").addClass("active_inputs_blocks");
            } else {
                jQuery(this).parent().find(".inputs_blocks").removeClass("active_inputs_blocks");
            }

        }); 

        jQuery(".user_info_phone_blocks").find(".text_after_plus_icon").click(function(){
            jQuery(this).parent().find(".inputs_blocks").append( "<div class='inputs_block_inner inputs_block_inner_phone'><input type='text' class='col-md-12 col-sm-12 col-xs-12 phone'><div class='remove_this_input'><img src='http://projects.make-web.org/epo/wp-content/themes/EPO/images/thrash.svg' alt=''></div></div>" );
            if ( jQuery(this).parent().find(".inputs_blocks").height() > 70 ){
                jQuery(this).parent().find(".inputs_blocks").addClass("active_inputs_blocks");
            } else {
                jQuery(this).parent().find(".inputs_blocks").removeClass("active_inputs_blocks");
            }
        });

        jQuery(".user_info_phone_blocks").find(".pluss_icon").click(function(){
            jQuery(this).parent().find(".inputs_blocks").append("<div class='inputs_block_inner inputs_block_inner_phone'><input type='text' class='col-md-12 col-sm-12 col-xs-12 phone'><div class='remove_this_input'><img src='http://projects.make-web.org/epo/wp-content/themes/EPO/images/thrash.svg' alt=''></div></div>" );
            if ( jQuery(this).parent().find(".inputs_blocks").height() > 70 ){
                jQuery(this).parent().find(".inputs_blocks").addClass("active_inputs_blocks");
            } else {
                jQuery(this).parent().find(".inputs_blocks").removeClass("active_inputs_blocks");
            }

        }); 

        jQuery(".user_info_work_phone_blocks_eng").find(".text_after_plus_icon").click(function(){
            jQuery(this).parent().find(".inputs_blocks").append("<div class='inputs_block_inner inputs_block_inner_workphone'><input type='text' class='col-md-12 col-sm-12 col-xs-12 work_phone_mask'><div class='remove_this_input'><img src='http://projects.make-web.org/epo/wp-content/themes/EPO/images/thrash.svg' alt=''></div></div>" );
            if ( jQuery(this).parent().find(".inputs_blocks").height() > 70 ){
                jQuery(this).parent().find(".inputs_blocks").addClass("active_inputs_blocks");
            } else {
                jQuery(this).parent().find(".inputs_blocks").removeClass("active_inputs_blocks");
            }
        });

        jQuery(".user_info_work_phone_blocks_eng").find(".pluss_icon").click(function(){
            jQuery(this).parent().find(".inputs_blocks").append("<div class='inputs_block_inner inputs_block_inner_workphone'><input type='text' class='col-md-12 col-sm-12 col-xs-12 work_phone_mask'><div class='remove_this_input'><img src='http://projects.make-web.org/epo/wp-content/themes/EPO/images/thrash.svg' alt=''></div></div>" );
            if ( jQuery(this).parent().find(".inputs_blocks").height() > 70 ){
                jQuery(this).parent().find(".inputs_blocks").addClass("active_inputs_blocks");
            } else {
                jQuery(this).parent().find(".inputs_blocks").removeClass("active_inputs_blocks");
            }
        }); 

        jQuery(".user_info_work_phone_blocks").find(".text_after_plus_icon").click(function(){
            jQuery(this).parent().find(".inputs_blocks").append("<div class='inputs_block_inner inputs_block_inner_workphone'><input type='text' class='col-md-12 col-sm-12 col-xs-12 work_phone_mask'><div class='remove_this_input'><img src='http://projects.make-web.org/epo/wp-content/themes/EPO/images/thrash.svg' alt=''></div></div>" );
            if ( jQuery(this).parent().find(".inputs_blocks").height() > 70 ){
                jQuery(this).parent().find(".inputs_blocks").addClass("active_inputs_blocks");
            } else {
                jQuery(this).parent().find(".inputs_blocks").removeClass("active_inputs_blocks");
            }
        });

        jQuery(".user_info_work_phone_blocks").find(".pluss_icon").click(function(){
            jQuery(this).parent().find(".inputs_blocks").append("<div class='inputs_block_inner inputs_block_inner_workphone'><input type='text' class='col-md-12 col-sm-12 col-xs-12 work_phone_mask'><div class='remove_this_input'><img src='http://projects.make-web.org/epo/wp-content/themes/EPO/images/thrash.svg' alt=''></div></div>" );
            if ( jQuery(this).parent().find(".inputs_blocks").height() > 70 ){
                jQuery(this).parent().find(".inputs_blocks").addClass("active_inputs_blocks");
            } else {
                jQuery(this).parent().find(".inputs_blocks").removeClass("active_inputs_blocks");
            }
        }); 

        jQuery(".user_info_skype_blocks").find(".text_after_plus_icon").click(function(){
            jQuery(this).parent().find(".inputs_blocks").append( "<div class='inputs_block_inner inputs_block_inner_skype'><input type='text' class='col-md-12 col-sm-12 col-xs-12 skype'><div class='remove_this_input'><img src='http://projects.make-web.org/epo/wp-content/themes/EPO/images/thrash.svg' alt=''></div></div>" );
            if ( jQuery(this).parent().find(".inputs_blocks").height() > 70 ){
                jQuery(this).parent().find(".inputs_blocks").addClass("active_inputs_blocks");
            } else {
                jQuery(this).parent().find(".inputs_blocks").removeClass("active_inputs_blocks");
            }
        });

        jQuery(".user_info_skype_blocks").find(".pluss_icon").click(function(){
            jQuery(this).parent().find(".inputs_blocks").append( "<div class='inputs_block_inner inputs_block_inner_skype'><input type='text' class='col-md-12 col-sm-12 col-xs-12 skype'><div class='remove_this_input'><img src='../images/thrash.svg' alt=''></div></div>" );
            if ( jQuery(this).parent().find(".inputs_blocks").height() > 70 ){
                jQuery(this).parent().find(".inputs_blocks").addClass("active_inputs_blocks");
            } else {
                jQuery(this).parent().find(".inputs_blocks").removeClass("active_inputs_blocks");
            }
        });

        jQuery("body").on("click", ".remove_this_input", function () {
            
            if (jQuery(this).parent().parent().find(".inputs_block_inner").length <= 2){
                jQuery(this).parent().parent().parent().find(".inputs_blocks").removeClass("active_inputs_blocks"); 
            }
            jQuery(this).parent().remove();
        });
        /*end*/

        jQuery(".add_company_button").click(function(){
            jQuery(this).parent().html("<div class='remove_this_company' style='top:0; color: #aaa;'><i class='fa fa-times'></i></div><div class='company_thank_box_title'>Ваш запрос отправлен на модерацию!</div>");
        });
        
        /* Добавление компании */
        jQuery("body").on("click", ".logged_user_add_company_button", function () {
            jQuery(this).hide();
            jQuery(this).parent().append('<div class="company_info_box"><span class="company_side_line"></span><div class="remove_this_company" style="color: #aaa;"><i class="fa fa-times"></i></div><div class="company_inputs_block"><label for="">Найменування юридичної особи</label><input type="text" placeholder="напр.Ексім-меблі 3000"><label for="">Реєстраційний код (ЄРДПОУ або ІПН)</label><input type="text"><label for="">Ваша посада</label><input type="text"><label for="">Повна назва посади</label><input type="text"></div><input type="button" class="add_company_button" value="Додати компанію"></div>')
       
            jQuery(".add_company_button").click(function(){
                jQuery(this).parent().html("<span class='company_side_line'></span><div class='remove_this_company' style='top: 0;color: #aaa;'><i class='fa fa-times'></i></div><div class='company_thank_box_title'>Ваш запрос отправлен на модерацию!</div>");
            });
        });     
        /*end*/


        /* Попап изменения пароля */
        jQuery(".user_greeting_wrap").find(".change_pass").click(function(){
            jQuery(".overflow_kry").fadeIn(300);
            jQuery(".change_pass_popup").fadeIn(300);
            jQuery(this).css("border", "0");
        });

        jQuery(".change_pass_popup_head").find("img").click(function(){
            jQuery(".overflow_kry").fadeOut(300);
            jQuery(".change_pass_popup").fadeOut(300);
        });

        jQuery(".popup_delet_company").find("img").click(function(){
            jQuery(".overflow_kry").fadeOut(300);
            jQuery(".popup_delet_company").fadeOut(300);
        });

        jQuery(".delet_this_company_popup").find("img").click(function(){
            jQuery(".overflow_kry").fadeOut(300);
            jQuery(".delet_this_company_popup").fadeOut(300);
        });

        jQuery(".delet_this_company_button").click(function(){
            jQuery(".delet_this_company_confirm").fadeIn();
            /*jQuery(".overflow_kry").fadeIn(300);
            jQuery(".delet_this_company_popup").fadeIn(300);
            jQuery(this).css("border", "0");*/
        });

        jQuery(".delet_this_company_confirm").find("input").eq(1).click(function(){
            jQuery(".delet_this_company_confirm").fadeOut(300);
        });

        jQuery(".delet_this_company_popup").find(".dont_delete").click(function(){
            jQuery(".overflow_kry").fadeOut(300);
            jQuery(".delet_this_company_popup").fadeOut(300);
            jQuery(this).css("border", "0");
        });

        jQuery(".overflow_kry").click(function(){
            jQuery(".overflow_kry").fadeOut(300);
            jQuery(".change_pass_popup").fadeOut(300);
            jQuery(".popup_delet_company").fadeOut(300);
            jQuery(".delet_this_company_popup").fadeOut(300);
            jQuery(".popup_for_search_HS").fadeOut(300);    
            jQuery(".popup_for_search_KVED").fadeOut(300);  
            jQuery(".popup_for_add_participant").fadeOut(300);
            jQuery(".service_learn_more_popup").fadeOut(300);
        });

        jQuery("body").on("click", ".remove_this_company", function () {
            jQuery(".overflow_kry").css("display","none!important");
            jQuery(".logged_user_add_company_button").show();
            jQuery(this).parent().remove();
        }); 

        jQuery(".change_pass_popup").find("div").find(".fa-eye-slash").click(function(){
            jQuery(this).fadeOut(0);
            jQuery(this).parent().find(".fa-eye").fadeIn(0);
            jQuery(this).parent().find("input").attr("type", "text");
        });
        jQuery(".change_pass_popup").find("div").find(".fa-eye").click(function(){
            jQuery(this).fadeOut(0);
            jQuery(this).parent().find(".fa-eye-slash").fadeIn(0);
            jQuery(this).parent().find("input").attr("type", "password");
        });

        jQuery(".quit_from_user_dashboard").click(function(){
            jQuery(".sign_out_confirm").fadeIn(300);
        });

        jQuery(".sign_out_confirm").find("input").eq(1).click(function(){
            jQuery(".sign_out_confirm").fadeOut(300);
        });
        /* end */

        /* Маски для телефонов */
        jQuery(".phone").mask('+38 (000) 000-00-00');

        jQuery(".work_phone_mask").mask('(044) 000-00-00');
        /* end */

        /* залогиненый пользователь */
        var default_user_photo = 'url("http://projects.make-web.org/epo/wp-content/themes/EPO/images/default_user_image.png%20?%3E")'
        var curent_user_photo = jQuery('.user_photo').css("background-image");
        

        jQuery(".logged_user_dashboard").click(function(){
            jQuery(this).fadeOut(0);
            jQuery('.empty_user_dashboard').fadeIn(0);
            jQuery('.user_photo_overflow').fadeIn(0);
            jQuery('.user_photo_wrapp').find('span').html("Видалити фото");
            jQuery('.user_photo_wrapp').find('img').addClass("logged_user_photo_margin");
            jQuery('.user_photo_wrapp').find('.upload_image').attr("src", "http://projects.make-web.org/epo/wp-content/themes/EPO/images/garbage_bucket.png");
            jQuery('.user_photo').find('img').attr("src", "http://projects.make-web.org/epo/wp-content/themes/EPO/images/user_photo.jpg");
            jQuery('.user_company_workplace_block').fadeIn(0);

            /* company side */
            jQuery(".company_info_box").fadeOut(0);
            jQuery(".add_company_button").fadeOut(0);
            jQuery(".logged_user").fadeIn(0);
            jQuery(".logged_user").find(".company_info_box").fadeIn(0);
        });

        jQuery(".remove_this_company").hide();
        jQuery(".active_company_info_box").find(".remove_this_company").show();
        jQuery(".company_info_box").click(function(){
            jQuery(".company_info_box").removeClass("active_company_info_box");
            jQuery(this).addClass("active_company_info_box");
            jQuery(".remove_this_company").hide();
            jQuery(this).find(".remove_this_company").show();
                                
        });
        
        jQuery("body").on("click", ".setp_main_email", function () {
            jQuery(this).parent().parent().find("span").removeClass("active_feedback_info");
            jQuery(this).parent().parent().find("span").html("Встановити як основний");
            jQuery(this).parent().find("span").html("Встановити як основний");
            jQuery(this).addClass("active_feedback_info");
            jQuery(this).html("Основний Email");
        }); 

        jQuery("body").on("click", ".setp_main_phone", function () {
            jQuery(this).parent().parent().find("span").removeClass("active_feedback_info");
            jQuery(this).parent().parent().find("span").html("Встановити як основний");
            jQuery(this).parent().find("span").html("Встановити як основний");
            jQuery(this).addClass("active_feedback_info");
            jQuery(this).html("Основний номер");
        }); 

        jQuery("body").on("click", ".setp_main_workphone", function () {
            jQuery(this).parent().parent().find("span").removeClass("active_feedback_info");
            jQuery(this).parent().parent().find("span").html("Встановити як основний");
            jQuery(this).parent().find("span").html("Встановити як основний");
            jQuery(this).addClass("active_feedback_info");
            jQuery(this).html("Основний номер");
        }); 

        jQuery("body").on("click", ".setp_main_email_eng", function () {
            jQuery(this).parent().parent().find("span").removeClass("active_feedback_info");
            jQuery(this).parent().parent().find("span").html("Set as basic");
            jQuery(this).parent().find("span").html("Set as basic");
            jQuery(this).addClass("active_feedback_info");
            jQuery(this).html("Basic Email");
        }); 

        jQuery("body").on("click", ".setp_main_phone_eng", function () {
            jQuery(this).parent().parent().find("span").removeClass("active_feedback_info");
            jQuery(this).parent().parent().find("span").html("Set as basic");
            jQuery(this).parent().find("span").html("Set as basic");
            jQuery(this).addClass("active_feedback_info");
            jQuery(this).html("Basic mobile phone");
        }); 

        jQuery("body").on("click", ".setp_main_workphone_eng", function () {
            jQuery(this).parent().parent().find("span").removeClass("active_feedback_info");
            jQuery(this).parent().parent().find("span").html("Set as basic");
            jQuery(this).parent().find("span").html("Set as basic");
            jQuery(this).addClass("active_feedback_info");
            jQuery(this).html("Basic workphone");
        }); 

        jQuery("body").on("click", ".setp_main_skype", function () {
            jQuery(this).parent().parent().find("span").removeClass("active_feedback_info");
            jQuery(this).parent().parent().find("span").html("Встановити як основний");
            jQuery(this).parent().find("span").html("Встановити як основний");
            jQuery(this).addClass("active_feedback_info");
            jQuery(this).html("Основний Skype");
        }); 

        jQuery("body").on("click", ".setp_main_website", function () {
            jQuery(this).parent().parent().find("span").removeClass("active_feedback_info");
            jQuery(this).parent().parent().find("span").html("Встановити як основний");
            jQuery(this).parent().find("span").html("Встановити як основний");
            jQuery(this).addClass("active_feedback_info");
            jQuery(this).html("Основний веб сайт");
        }); 

        /* Попап при удалении компании */   
        jQuery("body").on("click", ".remove_this_company", function () {
            jQuery(".delete_company_confirm").fadeOut(0);       
            jQuery(this).parent().find(".delete_company_confirm").fadeIn(300);      
        }); 

        jQuery(".delete_company_confirm").find(".confirm_delet_company").click(function(){          
            jQuery(this).parent().parent().remove();
        });

        jQuery(".delete_company_confirm").find(".dont_delete_company").click(function(){
            jQuery(this).parent().fadeOut();
        });

        /*jQuery(".overflow_for_delet_aside_elem").click(function(){
            jQuery(this).fadeOut(0);
            jQuery(".popup_delet_company").fadeOut(300);
            jQuery(".header").css("z-index", "99");
            jQuery(".user_company_info").css("z-index", "3");
            jQuery(".header").removeClass("setzin_for_delet_company");
            jQuery(".user_self_info_wrapp").removeClass("setzin_for_delet_company");
            jQuery(".user_greeting_wrap").removeClass("setzin_for_delet_company");
        });

        jQuery(".confirm_delete_company_aside").click(function(){
            jQuery(this).parent().parent().remove()
            jQuery(this).parent().remove();
            jQuery(".overflow_kry").fadeOut(300);
        });

        jQuery(".dont_delete_company_aside").click(function(){
            jQuery(".overflow_for_delet_aside_elem").fadeOut(0);            
            jQuery(".popup_delet_company").fadeOut(300);
            jQuery(".header").css("z-index", "99");
            jQuery(".user_company_info").css("z-index", "4");
            jQuery(".header").removeClass("setzin_for_delet_company");
            jQuery(".user_self_info_wrapp").removeClass("setzin_for_delet_company");
            jQuery(".user_greeting_wrap").removeClass("setzin_for_delet_company");
        });

        jQuery(".popup_delet_company").find("img").click(function(){
            jQuery(".overflow_for_delet_aside_elem").fadeOut(0);            
            jQuery(".popup_delet_company").fadeOut(300);
            jQuery(".header").css("z-index", "99");
            jQuery(".user_company_info").css("z-index", "4");
            jQuery(".header").removeClass("setzin_for_delet_company");
            jQuery(".user_self_info_wrapp").removeClass("setzin_for_delet_company");
            jQuery(".user_greeting_wrap").removeClass("setzin_for_delet_company");
        });*/
        /* конец */

        /* открытие и закрытие попапа с поисковиком */
        jQuery(".company_services_block").find(".pluss_icon").click(function(){
            jQuery(".header_overflow").fadeIn(300);
            jQuery(".overflov_for_search").fadeIn(300);
            jQuery(".popup_for_search_KVED").fadeIn(300);
            jQuery(".company_greeting_wrap").addClass("setzin_for_delet_company");
        });

        jQuery(".company_services_block").find(".add_info_company_services").click(function(){
            jQuery(".header_overflow").fadeIn(300);
            jQuery(".overflov_for_search").fadeIn(300);
            jQuery(".popup_for_search_KVED").fadeIn(300);
            jQuery(".company_greeting_wrap").addClass("setzin_for_delet_company");
        });

        jQuery(".company_stuffs_block").find(".pluss_icon").click(function(){
            jQuery(".header_overflow").fadeIn(300);
            jQuery(".overflov_for_search").fadeIn(300);
            jQuery(".popup_for_search_HS").fadeIn(300); 
            jQuery(".company_greeting_wrap").addClass("setzin_for_delet_company");
        });

        jQuery(".company_stuffs_block").find(".add_info_company_stuffs").click(function(){
            jQuery(".header_overflow").fadeIn(300);
            jQuery(".overflov_for_search").fadeIn(300);
            jQuery(".popup_for_search_HS").fadeIn(300); 
            jQuery(".company_greeting_wrap").addClass("setzin_for_delet_company");
        });

        jQuery(".close_search_popup").click(function(){
            jQuery(".header_overflow").fadeOut(0);
            jQuery(".header_overflow").fadeOut(0);
            jQuery(".overflov_for_search").fadeOut(0);
            jQuery(".popup_for_search_KVED").fadeOut(0);
            jQuery(".popup_for_search_HS").fadeOut(0);  
            jQuery(".company_greeting_wrap").removeClass("setzin_for_delet_company");
        });

        jQuery(".overflov_for_search").click(function(){
            jQuery(".header_overflow").fadeOut(0);
            jQuery(".overflov_for_search").fadeOut(0);
            jQuery(".popup_for_search_KVED").fadeOut(0);
            jQuery(".popup_for_search_HS").fadeOut(0);  
            jQuery(".company_greeting_wrap").removeClass("setzin_for_delet_company");
        });

        jQuery(".header_overflow").click(function(){
            jQuery(".header_overflow").fadeOut(0);
            jQuery(".overflov_for_search").fadeOut(0);
            jQuery(".popup_for_search_KVED").fadeOut(0);
            jQuery(".popup_for_search_HS").fadeOut(0);  
            jQuery(".company_greeting_wrap").removeClass("setzin_for_delet_company");
        });
        /* конец */

        /* добавление инпутов на странице компании в блок "средства связи" */
        jQuery(".company_info_email_blocks").find(".text_after_plus_icon").click(function(){
            jQuery(this).parent().find(".inputs_blocks").append( "<div class='inputs_block_inner inputs_block_inner_email'><input type='email' class='col-md-12 col-sm-12 col-xs-12 email'><div class='remove_this_input'><img src='../images/thrash.svg' alt=''></div><span class='setp_main_email'>Встановити як основний</span></div>" );
        });

        jQuery(".company_info_email_blocks").find(".pluss_icon").click(function(){
            jQuery(this).parent().find(".inputs_blocks").append( "<div class='inputs_block_inner inputs_block_inner_email'><input type='email' class='col-md-12 col-sm-12 col-xs-12 email'><div class='remove_this_input'><img src='/images/thrash.svg' alt=''></div><span class='setp_main_email'>Встановити як основний</span></div>" );
        }); 

        jQuery(".company_info_work_phone_blocks").find(".text_after_plus_icon").click(function(){
            jQuery(this).parent().find(".inputs_blocks").append( "<div class='inputs_block_inner inputs_block_inner_workphone'><input type='text' class='col-md-12 col-sm-12 col-xs-12 work_phone_mask'><div class='remove_this_input'><img src='http://projects.make-web.org/epo/wp-content/themes/EPO/images/thrash.svg' alt=''></div><span class='setp_main_workphone'>Встановити як основний</span></div>" );
        });

        jQuery(".company_info_work_phone_blocks").find(".pluss_icon").click(function(){
            jQuery(this).parent().find(".inputs_blocks").append( "<div class='inputs_block_inner inputs_block_inner_workphone'><input type='text' class='col-md-12 col-sm-12 col-xs-12 work_phone_mask'><div class='remove_this_input'><img src='http://projects.make-web.org/epo/wp-content/themes/EPO/images/thrash.svg' alt=''></div><span class='setp_main_workphone'>Встановити як основний</span></div>" );
        });

        jQuery(".company_info_website_blocks").find(".text_after_plus_icon").click(function(){
            jQuery(this).parent().find(".inputs_blocks").append( "<div class='inputs_block_inner'><input type='text' class='col-md-12 col-sm-12 col-xs-12'><div class='remove_this_input'><img src='../images/thrash.svg' alt=''></div><span class='setp_main_website'>Встановити як основний</span></div>" );
        });

        jQuery(".company_info_website_blocks").find(".pluss_icon").click(function(){
            jQuery(this).parent().find(".inputs_blocks").append( "<div class='inputs_block_inner'><input type='text' class='col-md-12 col-sm-12 col-xs-12'><div class='remove_this_input'><img src='http://projects.make-web.org/epo/wp-content/themes/EPO/images/thrash.svg' alt=''></div><span class='setp_main_website'>Встановити як основний</span></div>" );
        });
        /* конец */

        /* Если страница компании заполнена */
        jQuery(".company_page_full").click(function(){
            jQuery(this).fadeOut(0);
            jQuery('.empty_user_dashboard').fadeIn(0);
            jQuery('.user_photo_overflow').fadeIn(0);
            jQuery('.user_photo_wrapp').find('span').html("Видалити фото");
            jQuery('.user_photo_wrapp').find('img').addClass("logged_user_photo_margin");
            jQuery('.user_photo_wrapp').find('.upload_image').attr("src", "http://projects.make-web.org/epo/wp-content/themes/EPO/images/garbage_bucket.png");
            jQuery('.user_photo').find('img').attr("src", "http://projects.make-web.org/epo/wp-content/themes/EPO/images/credi_logo.png");
            /* company side */
            jQuery(".company_info_box").fadeOut(0);
            jQuery(".logged_company").fadeIn(0);
            jQuery(".logged_company").find(".company_info_box").fadeIn(0);

        });
        /* конец */

        /* Приглашение колеги */

        jQuery(".add_company_button").click(function(){
            jQuery(this).parent().html("<div class='company_info_box_title'>Ваш запрос отправлен на модерацию!</div>");
        });
        
        /* Добавление компании */ 
        jQuery("body").on("click", ".add_colleague", function () {
            jQuery(this).fadeOut(0);
            jQuery(this).parent().append('<div class="company_info_box"><div class="company_inputs_block"><label for="">П.І.Б.</label><input type="text" placeholder="напр. Сергій Петренко"><label for="">E-mail</label><input type="text"></div><input type="button" class="add_colleague_button" value="Надіслати запрошення"></div><div class="add_colleague"><img src="http://projects.make-web.org/epo/wp-content/themes/EPO/images/add_company_button.png" alt="">Запросити колегу</div>')
        });     
        /* конец */

        jQuery(".add_colleague_button").click(function(){
            jQuery(this).parent().html("<div class='remove_this_company' style='top:0;'><i class='fa fa-times'></i></div><div class='company_thank_box_title'>Ваш запрос отправлен на модерацию!</div>");
        });

        /* Удаление колеги */
        jQuery("body").on("click", ".remove_this_worker", function () {
            jQuery(".delete_company_confirm").fadeOut(0);       
            jQuery(this).parent().find(".delete_company_confirm").fadeIn(300);      
        }); 

        jQuery(".delete_company_confirm").find(".confirm_delet_company").click(function(){          
            jQuery(this).parent().parent().remove();
        });

        jQuery(".delete_company_confirm").find(".dont_delete_company").click(function(){
            jQuery(this).parent().fadeOut();
        });
        /*jQuery("body").on("click", ".remove_this_worker", function () {
            jQuery(".overflow_kry").fadeIn(300);
            jQuery(this).parent().find(".popup_delet_worker").fadeIn(300);
        });

        jQuery(".popup_delet_worker").find(".col-md-5").eq(0).click(function(){
            jQuery(this).parent().parent().remove()
            jQuery(this).remove();
            jQuery(".overflow_kry").fadeOut(300);
        });

        jQuery(".popup_delet_worker").find("img").click(function(){
            jQuery(".overflow_kry").fadeOut(300);
            jQuery(".popup_delet_worker").fadeOut(300);
        });

        jQuery(".popup_delet_worker").find(".col-md-5").eq(1).click(function(){
            jQuery(".overflow_kry").fadeOut(300);
            jQuery(".popup_delet_worker").fadeOut(300);
        });*/

        /*jQuery("body").on("click", ".remove_this_worker", function () {
            jQuery(this).parent().find(".overflow_for_delet_aside_elem").fadeIn(300);
            jQuery(this).parent().find(".popup_delet_worker").fadeIn(300);          
            jQuery(".header").css("z-index", "4");
            jQuery(".user_company_info").css("z-index", "5");
            jQuery(".user_self_info_wrapp").addClass("setzin_for_delet_company");
            jQuery(".user_greeting_wrap").addClass("setzin_for_delet_company"); 
            jQuery(".company_greeting_wrap").addClass("setzin_for_delet_company");      
        }); 

        jQuery(".overflow_for_delet_aside_elem").click(function(){
            jQuery(this).fadeOut(0);
            jQuery(".popup_delet_worker").fadeOut(0);       
            jQuery(".header").css("z-index", "99");
            jQuery(".user_company_info").css("z-index", "3");
            jQuery(".header").removeClass("setzin_for_delet_company");
            jQuery(".user_self_info_wrapp").removeClass("setzin_for_delet_company");
            jQuery(".user_greeting_wrap").removeClass("setzin_for_delet_company");
            jQuery(".company_greeting_wrap").removeClass("setzin_for_delet_company");
        });

        jQuery(".confirm_delete_worker_aside").click(function(){
            jQuery(this).parent().parent().remove()
            jQuery(this).parent().remove();
            jQuery(".overflow_kry").fadeOut(300);
        });

        jQuery(".dont_delete_worker_aside").click(function(){
            jQuery(".overflow_for_delet_aside_elem").fadeOut(0);            
            jQuery(".popup_delet_worker").fadeOut(300);
            jQuery(".header").css("z-index", "99");
            jQuery(".user_company_info").css("z-index", "4");
            jQuery(".header").removeClass("setzin_for_delet_company");
            jQuery(".user_self_info_wrapp").removeClass("setzin_for_delet_company");
            jQuery(".user_greeting_wrap").removeClass("setzin_for_delet_company");
            jQuery(".company_greeting_wrap").removeClass("setzin_for_delet_company");
        });

        jQuery(".popup_delet_worker").find("img").click(function(){
            jQuery(".overflow_for_delet_aside_elem").fadeOut(0);            
            jQuery(".popup_delet_worker").fadeOut(0);
            jQuery(".header").css("z-index", "99");
            jQuery(".user_company_info").css("z-index", "4");
            jQuery(".header").removeClass("setzin_for_delet_company");
            jQuery(".user_self_info_wrapp").removeClass("setzin_for_delet_company");
            jQuery(".user_greeting_wrap").removeClass("setzin_for_delet_company");
            jQuery(".company_greeting_wrap").removeClass("setzin_for_delet_company");
        });*/
        /* конец */

        /* страница сотрудника */
        jQuery(".delet_this_collegue_button").click(function(){
            jQuery(".delet_this_collegue_confirm").fadeIn(300);
            /*jQuery(".overflow_kry").fadeIn(300);
            jQuery(".delet_this_company_popup").fadeIn(300);
            jQuery(this).css("border", "0");*/
        });

        jQuery(".delet_this_collegue_confirm").find("input").eq(1).click(function(){
            jQuery(".delet_this_collegue_confirm").fadeOut(300);
        });
        /* конец */

        /* Страница с обытиями */
            
            jQuery(".filter_item").children("span").click(function(){
                jQuery(this).parent().siblings().removeClass("active_filter");
                jQuery(this).parent().addClass("active_filter");
                jQuery(".overflow_events_kry").fadeIn();
                jQuery(".filter_item_chosen_block").fadeOut(0);
                jQuery(this).parent().find(".filter_item_chosen_block").fadeIn();
            });
        
        jQuery(".filter_item").find("img").on("click", function(){
            jQuery(this).parent().removeClass("active_filter");
            jQuery(".overflow_events_kry").fadeOut();   
                jQuery(".filter_item_chosen_block").fadeOut(0);
        });

        jQuery(".overflow_events_kry").on("click", function(){
            jQuery(".filter_item").removeClass("active_filter");
            jQuery(".overflow_events_kry").fadeOut();   
            jQuery(".filter_item_chosen_block").fadeOut();  
        });
        
        jQuery('.filter_chosen_item').on("click", function(){
            jQuery(this).parent().find(".filter_chosen_item").removeClass("check_active");
            jQuery(this).addClass("check_active");
            checkbox.prop("checked", true);
            jQuery('.filter_chosen_item').click(function(){
                var checkbox = jQuery(this).find('input[type=checkbox]');
                if(checkbox.prop("checked")){
                    jQuery(this).removeClass("check_active");
                    checkbox.prop("checked", false);
                }else{
                    //jQuery(".filter_chosen_item").removeClass("check_active");    
                    //jQuery(this).addClass("check_active");
                    //checkbox.prop("checked", true);
                }
            });
        });     
        

        /* Счетчик символов в textarea */
        var characters_Limit1 = 500;
        jQuery('.company_textarea').eq(0).keyup(function() {
            if (jQuery(this).val().length > characters_Limit1) {
                //jQuery(this).val(jQuery(this).val().substr(0, characters_Limit1));
                jQuery(this).parent().find('.char_count').css("color", "#f9492e");
            }
            jQuery(this).parent().find('.char_count').text(this.value.replace(/{.*}/g, '').length);
        });

        var characters_Limit2 = 400;
        jQuery('.company_textarea').eq(1).keyup(function() {
            if (jQuery(this).val().length > characters_Limit2) {
                //jQuery(this).val(jQuery(this).val().substr(0, characters_Limit2));
                jQuery(this).parent().find('.char_count').css("color", "#f9492e");
            }
            jQuery(this).parent().find('.char_count').text(this.value.replace(/{.*}/g, '').length);
        });

        var characters_Limit3 = 200;
        jQuery('.company_textarea').eq(2).keyup(function() {
            if (jQuery(this).val().length > characters_Limit3) {
                //jQuery(this).val(jQuery(this).val().substr(0, characters_Limit3));
                jQuery(this).parent().find('.char_count').css("color", "#f9492e");
            }
            jQuery(this).parent().find('.char_count').text(this.value.replace(/{.*}/g, '').length);
        });
 
        var characters_Limit4 = 2000;
        jQuery(".single_event_reg_step_questions").find('.company_textarea').keyup(function() {
            if (jQuery(this).val().length > characters_Limit4) {
                //jQuery(this).val(jQuery(this).val().substr(0, characters_Limit4));
                jQuery(this).parent().find('.char_count').css("color", "#f9492e");
            }
            jQuery(this).parent().find('.char_count').text(this.value.replace(/{.*}/g, '').length);
        });

        var characters_Limit5 = 500;
        jQuery(".company_textarea4").keyup(function() {
            if (jQuery(this).val().length > characters_Limit5) {
                //jQuery(this).val(jQuery(this).val().substr(0, characters_Limit5));
                jQuery(this).parent().find('.char_count').css("color", "#f9492e");
            }
            jQuery(this).parent().find('.char_count').text(this.value.replace(/{.*}/g, '').length);
        });

        var characters_Limit6 = 4000;
        jQuery(".company_textarea5").keyup(function() {
            if (jQuery(this).val().length > characters_Limit6) {
                //jQuery(this).val(jQuery(this).val().substr(0, characters_Limit6));
                jQuery(this).parent().find('.char_count').css("color", "#f9492e");
            }
            jQuery(this).parent().find('.char_count').text(this.value.replace(/{.*}/g, '').length);
        });

        var characters_Limit7 = 600;
        jQuery(".company_textarea6").keyup(function() {
            if (jQuery(this).val().length > characters_Limit7) {
                //jQuery(this).val(jQuery(this).val().substr(0, characters_Limit6));
                jQuery(this).parent().find('.char_count').css("color", "#f9492e");
            }
            jQuery(this).parent().find('.char_count').text(this.value.replace(/{.*}/g, '').length);
        });

        /* попап добавления участника на странице события */
        jQuery(".empty_participant").click(function(){
            jQuery(".overflow_kry").fadeIn();
            jQuery(".popup_for_add_participant").fadeIn();
        });

        jQuery(".popup_for_add_participant").find(".popup_for_add_participant_title").find("img").click(function(){
            jQuery(".popup_for_add_participant").fadeOut();
            jQuery(".overflow_kry").fadeOut();
        });
        /*  конец */

        jQuery("body").on("click", ".fa-chevron-down", function () {
            jQuery(this).removeClass("fa-chevron-down");
            jQuery(this).addClass("fa-chevron-up");
        });

        jQuery("body").on("click", ".fa-chevron-up", function () {
            jQuery(this).removeClass("fa-chevron-up");
            jQuery(this).addClass("fa-chevron-down");
        }); 

        /* Страница отдельной услуги */
        var h = jQuery(".open_selection_user_bloks_wrpp").height();
        jQuery(".selection_user_bloks_wrapp").css("margin-top", h+10);

        jQuery("body").on("click", ".fa-chevron-down", function () {
            jQuery(this).parent().parent().parent().find(".selection_user_bloks_wrapp").fadeIn();
        });

        jQuery("body").on("click", ".fa-chevron-up", function () {
            jQuery(this).parent().parent().parent().find(".selection_user_bloks_wrapp").fadeOut();
        }); 

        var learn_more_popup_h = jQuery(".service_learn_more_popup").height()
        jQuery(".service_learn_more_popup").css({
            "top":"50%",
            "margin-top": learn_more_popup_h/-2
        })

        jQuery(".service_learn_more").click(function(){
            jQuery(".service_learn_more_popup").fadeIn();
            jQuery(".overflow_kry").fadeIn();
        });

        jQuery(".service_learn_more_popup").find("img").click(function(){
            jQuery(".service_learn_more_popup").fadeOut();
            jQuery(".overflow_kry").fadeOut();
        });
  
        /*  конец */

        jQuery('.services_carousel').owlCarousel({
            loop:true, 
            margin:10, 
            nav:true, 
            navText : ["<img src='http://projects.make-web.org/epo/wp-content/themes/EPO/images/left-arrow.svg'/>","<img src='http://projects.make-web.org/epo/wp-content/themes/EPO/images/left-arrow.svg'/>"],
            items:4,
            autoplay:false, 
            smartSpeed:1000, 
            autoplayTimeout:2000, 
            responsive:{ 
                0:{
                    items:1
                },
                767:{
                    items:2
                },
                1200:{
                    items:3
                },
                1201:{
                    items:4
                }
            }
        });     
    }); 