jQuery(document).ready(function($){
	//connect bxslider for ourteam
	$('.sidebar-ourteam').bxSlider({
	    slideWidth: 220,
	    minSlides: 1,
	    maxSlides: 4,
		slideMargin: 10
		
	});


	//Themth for contact form
	$('#pirate-forms-contact-subject').replaceWith('<select name="your-subject" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required form-control" id="pirate-forms-contact-subject" aria-required="true" aria-invalid="false">\
			<option value="" disabled selected>Оберіть тему</option>\
			<option value="Загальна">Загальна</option>\
			<option value="G2B платформа">G2B платформа</option>\
			<option value="Бізнес-можливості">Бізнес-можливості</option>\
			<option value="Консалтинг">Консалтинг</option>\
			<option value="Освіта для експортеріва">Освіта для експортерів</option>\
			<option value="Інформація та аналітика">Інформація та аналітика</option>\
			<option value="Міжнародні державні закупівлі">Міжнародні державні закупівлі</option>\
			<option value="Запити ЗМІ">Запити ЗМІ</option>\
		</select>');

	//Themth for contact form
	$('#pirate-forms-contact-subject-en').replaceWith('<select name="your-subject" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required form-control" id="pirate-forms-contact-subject" aria-required="true" aria-invalid="false">\
			<option value="" disabled selected>Select a topic</option>\
			<option value="Загальна">General</option>\
			<option value="G2B платформа">G2B Platform</option>\
			<option value="Бізнес-можливості">Business Opportunities</option>\
			<option value="Консалтинг">Consulting</option>\
			<option value="Освіта для експортеріва">Educational Platform</option>\
			<option value="Інформація та аналітика">Information and Analytics</option>\
			<option value="Міжнародні державні закупівлі">International Public Procuremen</option>\
			<option value="Запити ЗМІ">Media requests</option>\
		</select>');

	//button loupe 
	function hideSearch(e){
	    if(!$(e.target).hasClass('search-field')) {

	      	$('.widget_search > form').hide();
	      	$('.search-icon__img').show();
			$(document).off('click', hideSearch);
	    }
	}

	$('.search-icon__img').on('click', function(){
		var button_loupe = $(this);

		button_loupe.hide();
		$('.widget_search > form').css('display','inline-block');
		$('.widget_search > form .search-field').focus();
		setTimeout(function(){
			$(document).on('click', hideSearch);
		});

	});

$(function() {
  $('#c-profile').on('click', function(e) {
  	$('#analytics_material .category-country_profile').fadeIn(function() {
        console.log($(e.target).is(':visible'));
      });
    $('#analytics_material .category-taryfne-i-netaryfne-regulyuvannya').fadeOut(function() {
        console.log($(e.target).is(':visible'));
      });
    $('#analytics_material .category-gajdy').fadeOut(function() {
        console.log($(e.target).is(':visible'));
      });

  });
});

$(function() {
  $('#tar-reg').on('click', function(e) {
  	$('#analytics_material .category-country_profile').fadeOut(function() {
        console.log($(e.target).is(':visible'));
      });
    $('#analytics_material .category-taryfne-i-netaryfne-regulyuvannya').fadeIn(function() {
        console.log($(e.target).is(':visible'));
      });
    $('#analytics_material .category-gajdy').fadeOut(function() {
        console.log($(e.target).is(':visible'));
      });

  });
});


$(function() {
  $('#guide').on('click', function(e) {
  	$('#analytics_material .category-country_profile').fadeOut(function() {
        console.log($(e.target).is(':visible'));
      });
    $('#analytics_material .category-taryfne-i-netaryfne-regulyuvannya').fadeOut(function() {
        console.log($(e.target).is(':visible'));
      });
    $('#analytics_material .category-gajdy').fadeIn(function() {
        console.log($(e.target).is(':visible'));
      });

  });
});

$("#anal-switcher span").click(function(e) {
  e.preventDefault();
  $("#anal-switcher span").removeClass('active');
  $(this).addClass('active');
})

});