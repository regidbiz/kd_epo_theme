��          �               0     Q   >  _   �  0   �     !  %   /     U     d  \   {     �     �  
   �     �  	     �    D   �  b   �  �   C  B      "   C  7   f     �  ,   �  �   �     z     �     �  !   �     �   <span class="meta-nav">&larr;</span> Older posts <span class="posted-on">Posted on %1$s</span><span class="byline"> by %2$s</span> It looks like nothing was found at this location. Maybe try one of the links below or a search? Newer posts <span class="meta-nav">&rarr;</span> Nothing Found Oops! That page can&rsquo;t be found. Posted in %1$s Search Results for: %s Sorry, but nothing matched your search terms. Please try again with some different keywords. Subject Tagged %1$s Your Email Your Message Your Name Project-Id-Version: Zerif Lite
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-08-11 11:35+0000
PO-Revision-Date: 2017-08-11 11:36+0000
Last-Translator: Stanislav <s.kovynov@gmail.com>
Language-Team: Ukrainian
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_n:1,2;_x:1,2c;_ex:1,2c
X-Poedit-Basepath: ../
X-Poedit-SearchPath-0: .
X-Poedit-SearchPath-1: inc
X-Poedit-SearchPath-2: sections
Language: uk
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10 >= 2 && n%10<=4 &&(n%100<10||n%100 >= 20)? 1 : 2)
X-Generator: Loco - https://localise.biz/ <span class="meta-nav">&larr;</span> Попередні записи <span class="posted-on">Опубліковано %1$s</span><span class="byline"> до %2$s</span> Нажаль, сторінки не існує. Перейдіть на <a href="https://epo.org.ua/">головну сторінку</a>, або скористайтесь пошуком. Наступні записи <span class="meta-nav">&rarr;</span> Нічого не знайдено Вибачте, сторінку не знайдено. Категорія %1$s Результати пошуку для: %s На жаль нічого не знайдено. Будь-ласка використайте інші ключові слова для пошуку. Тема Теги %1$s Ваш Email Ваше повідомлення Ваше ім'я 