<?php
new EpoProfile;

class EpoProfile{

	public function __construct(){	
		$this->execute_profile();
	}
	
	protected function execute_profile(){ 
	
		add_action('after_setup_theme', function(){
			if (!current_user_can('administrator') && !is_admin()) {
			  show_admin_bar(false);
			}		
		});
	
		add_action('wp_enqueue_scripts', function(){
			wp_register_script('epo_profile_js_mlmvn', get_template_directory_uri() . '/classes/epo_profile/js/epo_profile.js', array( 'jquery' ), '1.0.9', true );
			$epo_profile_js_mlmvn = array(
				'wp_ajax_url' 	=> admin_url( 'admin-ajax.php' )
			);
			wp_localize_script( 'epo_profile_js_mlmvn', 'epo_profile_js_mlmvn', $epo_profile_js_mlmvn );
			wp_enqueue_script( 'epo_profile_js_mlmvn' );			
		});
		
		add_action( 'init', function(){
			add_rewrite_rule( 'epo_profile/([^/]+)', 'index.php?epo_profile=$matches[1]', 'top' );
		} );	
				
		add_filter( 'query_vars', function( $vars){
			$vars[] = 'epo_profile';
			return $vars;	
		} );

		add_action( 'template_redirect', function(){
			if ( get_query_var( 'epo_profile' ) ) {
				$template_key = get_query_var( 'epo_profile' );
				switch($template_key){
					case 'dashboard':
						add_filter( 'template_include', function() {
							return get_template_directory() . '/classes/epo_profile/templates/dashboard.php';
						});
					break;
					case 'services_list':
						add_filter( 'template_include', function() {
							return get_template_directory() . '/classes/epo_profile/templates/services_list.php';
						});
					break;		
					case 'checking_idea_page':
						add_filter( 'template_include', function() {
							return get_template_directory() . '/classes/epo_profile/templates/checking_idea_page.php';
						});
					break;	
					case 'een_page':
						add_filter( 'template_include', function() {
							return get_template_directory() . '/classes/epo_profile/templates/een_page.php';
						});
					break;	
					case 'mentorstvo_page':
						add_filter( 'template_include', function() {
							return get_template_directory() . '/classes/epo_profile/templates/mentorstvo_page.php';
						});
					break;	
					case 'internationalization_bussiness_page':
						add_filter( 'template_include', function() {
							return get_template_directory() . '/classes/epo_profile/templates/internationalization_bussiness_page.php';
						});
					break;	
					case 'sourcing_service_page':
						add_filter( 'template_include', function() {
							return get_template_directory() . '/classes/epo_profile/templates/sourcing_service_page.php';
						});
					break;	
					case 'readliness_assessment_page':
						add_filter( 'template_include', function() {
							return get_template_directory() . '/classes/epo_profile/templates/readliness_assessment_page.php';
						});
					break;	
					case 'events_for_user':
						add_filter( 'template_include', function() {
							return get_template_directory() . '/classes/epo_profile/templates/events_for_user.php';
						});
					break;	
					case 'single_event_page':
						add_filter( 'template_include', function() {
							return get_template_directory() . '/classes/epo_profile/templates/single_event_page.php';
						});
					break;	
					case 'company':
						add_filter( 'template_include', function() {
							return get_template_directory() . '/classes/epo_profile/templates/company.php';
						});
					break;	
					case 'companiya':
						add_filter( 'template_include', function() {
							return get_template_directory() . '/classes/epo_profile/templates/companiya.php';
						});
					break;	
					case 'colleague':
						add_filter( 'template_include', function() {
							return get_template_directory() . '/classes/epo_profile/templates/colleague.php';
						});
					break;	
					case 'search-companies':
						add_filter( 'template_include', function() {
							return get_template_directory() . '/classes/epo_profile/templates/search-companies.php';
						});
					break;	
					case 'search-partner':
						add_filter( 'template_include', function() {
							return get_template_directory() . '/classes/epo_profile/templates/search-partner.php';
						});
					break;	
					case 'service_testing_page':
						add_filter( 'template_include', function() {
							return get_template_directory() . '/classes/epo_profile/templates/service_testing_page.php';
						});
					break;	
					case 'thanks_order_service':
						add_filter( 'template_include', function() {
							return get_template_directory() . '/classes/epo_profile/templates/thanks_order_service.php';
						});
					break;			
					case 'activation_user':
						add_filter( 'template_include', function() {
							return get_template_directory() . '/classes/epo_profile/templates/activation_user.php';
						});					
					break;
					case 'reset_pass':
						add_filter( 'template_include', function() {
							return get_template_directory() . '/classes/epo_profile/templates/reset_pass.php';
						});					
					break;					
				}
			}			
		} );
		
		add_action("wp_ajax_header_profile_login_form", [$this,'header_profile_login_form']);
		add_action("wp_ajax_nopriv_header_profile_login_form", [$this,'header_profile_login_form']);
		
		add_action("wp_ajax_header_profile_login_remind", [$this,'header_profile_login_remind']);
		add_action("wp_ajax_nopriv_header_profile_login_remind", [$this,'header_profile_login_remind']);

		add_action("wp_ajax_header_profile_login_registration", [$this,'header_profile_login_registration']);
		add_action("wp_ajax_nopriv_header_profile_login_registration", [$this,'header_profile_login_registration']);		

		add_action("wp_ajax_page_profile_change_password", [$this,'page_profile_change_password']);
		add_action("wp_ajax_nopriv_page_profile_change_password", [$this,'page_profile_change_password_nopriv']);		
	
	}
	
	public function page_profile_change_password(){
		$res = [];
		$data_form = $_POST['data_form'];
		$data_form_arr = [];
		parse_str($data_form, $data_form_arr);
		$old_pass = $data_form_arr['old_pass'];
		$new_pass_1 = $data_form_arr['new_pass_1'];
		$new_pass_2 = $data_form_arr['new_pass_2'];
		$uid = get_current_user_id();
		$user = get_user_by( 'ID', $uid ); 
		if ( wp_check_password( $old_pass, $user->data->user_pass, $user->ID) ){
			if($new_pass_1 == $new_pass_2){
				wp_set_password( $new_pass_1, $uid );
				$res['msg'] = 'ok';
			}else{
				$res['msg'] = 'Паролі не співпадають';
			}
		}else{
			$res['msg'] = 'Старий пароль введено неправильно!';
		}
		wp_send_json($res);
	}
	
	public function page_profile_change_password_nopriv(){
		$res = [];
		$res['msg'] = 'У вас немає прав змінювати пароль!';
		wp_send_json($res);
	}
	
	public function header_profile_login_form(){
		
		$res = [];
		$data_form = [];
		parse_str($_POST['data_form'], $data_form);
		
		$email = $data_form['autorization_email'];
		$password = $data_form['autorization_password'];
		
		$user = get_user_by( 'email', $email ); 
		if( $user ) {
			if ( wp_check_password( $password, $user->data->user_pass, $user->ID) ){
				$res['msg'] = 'ok';
				$res['url'] = get_home_url() . '/epo_profile/dashboard/';  
				wp_set_current_user( $user->ID, $user->user_login );
				wp_set_auth_cookie( $user->ID );
				do_action( 'wp_login', $user->user_login );					
			}else{
				$res['msg'] = 'Пароль введено неправильно. Спробуйте ще раз ...';
			}
		}else{
			$res['msg'] = 'Користувача з таким email (' . $email . ') не знайдено ...';
		}
		
		wp_send_json($res);
		
	}
	
	public function header_profile_login_remind(){
		
		$res = [];
		$data_form = [];
		parse_str($_POST['data_form'], $data_form);		
		
		$email = $data_form['forgot_pass_email'];
		$user = get_user_by( 'email', $email ); 
		if( $user ) {
			$user_id = $user->ID;
			$code = sha1( $user_id . time() );
			$activation_url = get_home_url() . '/epo_profile/reset_pass/';
			$activation_link = add_query_arg( array( 'key' => $code, 'user' => $user_id ), $activation_url);
			add_user_meta( $user_id, 'has_to_be_reset', $code, true );
			$message = '<p>На вашу e-mail адресу створено запит на відновлення паролю.</p> <p>Для відновлення перейдіть за посиланням <a href="' . $activation_link . '" target="_blank">' . $activation_link . '</a></p> <p>Якщо Ви не створювали запит, будь ласка проігноруйте цей лист.</p> <p><br>З повагою</p> <p>Команда Офісу з просування експорту</p>';
			$headers .= 'From: Export Promotion Office <noreply@epo.org.ua>' . "\r\n";
			$headers .= "Reply-To: ". strip_tags("noreply@epo.org.ua") . "\r\n";
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
			if(wp_mail($email, 'Відновлення паролю на сайті epo.org.ua', $message, $headers)){
				$res['msg'] = 'ok';
				$res['text'] = 'На email ' . $to . ' відправлено лист з інструкцією по відновленню доступу.<p></p>';
			}else{
				$res['msg'] = 'Виникла технічна помилка при відправці листа з інструкцією по відновленню доступа. Спробуйте пізніше ...';
			}			
			
		}else{
			$res['msg'] = 'Користувача з таким email (' . $email . ') не знайдено.';
		}

		wp_send_json($res);		
		
	}
	
	public function header_profile_login_registration(){
		
		$res = [];
		$data_form = [];
		parse_str($_POST['data_form'], $data_form);		
		
		$email = $data_form['registration_password'];
		$fio = $data_form['reg_name'];	
		$fio_arr = explode(" ", $fio);
		
		$user = get_user_by( 'email', $email ); 
		if( $user ) {
			$res['msg'] = 'Користувач з таким email (' . $email . ') вже існує або очікує активацію ...';
		}else{
			
			if(class_exists('CrmApi')){
				$api = new CrmApi;
				$crm_auth = $api->crm_auth();
				if($crm_auth){
					
					$emails = [$email];
					if($api->check_email_exist_on_contacts($emails) == false){
					
						$first_name = '';
						$last_name = '';			
						
						$user_login = sanitize_user($email);
						if(isset($fio_arr[0]) && $fio_arr[0] != ''){
							$first_name = sanitize_user($fio_arr[0]);
						}
						if(isset($fio_arr[1]) && $fio_arr[1] != ''){
							$last_name = sanitize_user($fio_arr[1]);
						}		
						if(isset($fio_arr[2]) && $fio_arr[2] != ''){
							$last_name .= ' ' . sanitize_user($fio_arr[2]);
						}				
						
						$default_newuser = array(
							'user_pass' =>  wp_generate_password( 12, false ),
							'user_login' => $user_login,
							'user_email' => $email,
							'first_name' => $first_name,
							'last_name' => $last_name,
							'role' => 'pending'
						);

						$user_id = wp_insert_user($default_newuser);
						if ( $user_id && !is_wp_error( $user_id ) ) {
							$code = sha1( $user_id . time() );
							$activation_url = get_home_url() . '/epo_profile/activation_user/';
							$activation_link = add_query_arg( array( 'key' => $code, 'user' => $user_id ), $activation_url);
							add_user_meta( $user_id, 'has_to_be_activated', $code, true );
							add_user_meta( $user_id, 'has_to_be_crm', '0', true );
							$message = '<p>Доброго дня.</p> <p>Дякуємо, за реєстрацію на сайті Офісу з просування експорту.</p> <p><br>Для завершення реєстрації, будь ласка, перейдіть за посиланням <a href="' . $activation_link . '" target="_blank">' . $activation_link . '</a></p> <p><br>З повагою</p> <p>Команда Офісу з просування експорту</p>';
							$headers .= 'From: Export Promotion Office <noreply@epo.org.ua>' . "\r\n";
							$headers .= "Reply-To: ". strip_tags("noreply@epo.org.ua") . "\r\n";
							$headers .= "MIME-Version: 1.0\r\n";
							$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
							if(wp_mail($email, 'Активація аккаунту на сайті epo.org.ua', $message, $headers)){
								$res['msg'] = 'ok';
								$res['text'] = 'На email ' . $email . ' відправлено лист з інструкцією по активації аккаунту.<p></p>';
							}else{
								$res['msg'] = 'Виникла технічна помилка при відправці листа з інстукцією по активації аккаунту. Зверніться в службу підтримки сайту ...';
							}
						}else{
							$res['msg'] = 'Виникла технічна помилка при створенні нового користувача. Спробуйте пізніше ...';
						}

					}else{
						$res['msg'] = 'Користувач з таким email (' . $email . ') вже існує або очікує активацію ...';
					}
					
				}else{
					$res['msg'] = 'Виникла технічна помилка при спробі з\'єднатися з CRM. Спробуйте пізніше ...';
				}
			}else{
				$res['msg'] = 'Виникла технічна помилка при спробі з\'єднатися з CRM. Спробуйте пізніше ...';
			}
			
		}
		
		wp_send_json($res);		
	}	
	
}