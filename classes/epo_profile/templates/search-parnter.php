<?php 
if(!is_user_logged_in()){
	wp_redirect(get_home_url(), 302);
}
get_header();  
?>
<div class="clear"></div>
</header>
	<div class="content">
		
		<div class="container">
			<div class="search-partner">
				<div class="search-partner__title">Partner search</div>
				<div class="search-partner__navigation">
					<ul>
						<li class="search-partner__item_active"><a href="#">search by industry</a></li>
						<li><a href="#">search by product code</a></li>
					</ul>
				</div>
				<div class="search-partner__info">Specify the specialization of the company by choosing 1-5 appropriate industries.</div>
				<div class="search-partner__form">
					<form action="">
						<input class="search-partner__input" type="text" placeholder="Enter the name of the product, service or industry">
					</form>
				</div>
				<div class="search-partner__results">
					
				<div class="main_search_wrapp">
					
					<div class="search_results">	
						<ul class="list-kved">
							<li class="list-kved__item">
								<div class="list-kved__point"></div>
								<span class="list-kved__title">80 - Услуги бизенса</span>
								<ul>
									<li class="list-kved__item">
										<span class="list-kved__point"></span>
										<span class="list-kved__title">80 - Услуги бизенсу</span>
									</li>
									<li class="list-kved__item">
										<span class="list-kved__point"></span>
										<span class="list-kved__title">Финансовые и страховые услуги</span>
										<ul>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Бумага, печатные и издательские услуги</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Досуг развлечения  и туризм</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Информационные технологии, компьютеры, программное обеспечение, Интернет, исследовани и разработки</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Металлы, станки, машины, металлообработка</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Минералы, руды, полезные ископаемые</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Образование, обучение, общественные институты</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Розничаная торговля и дистрибуци</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Сельское хозяйство, продукты питания</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Строительство</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Текстиль, одежда, кожа, часы, ювелизрные изделия</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Транспорт и логистика</span>
											</li>
										</ul>
									</li>		
								</ul>
							</li>
						</ul>
						<div class="company_services_block col-md-12 col-sm-12 col-xs-12">
						<div class="services_self_block col-md-6 col-sm-6 col-xs-12">
							<div class="col-md-12">
								<img src="<?php print get_stylesheet_directory_uri(); ?>/images/close_stuff_serv_button.png" alt="">
								<div class="service_name">8214029 - Banking services on long-term loans</div>
							</div>
						</div>
						<div class="services_self_block col-md-6 col-sm-6 col-xs-12">
							<div class="col-md-12">
								<img src="<?php print get_stylesheet_directory_uri(); ?>/images/close_stuff_serv_button.png" alt="">
								<div class="service_name">8214031 - Banking services for medium-term and short-term loans</div>
							</div>
						</div>
						<div class="services_self_block col-md-6 col-sm-6 col-xs-12">
							<div class="col-md-12">
								<img src="<?php print get_stylesheet_directory_uri(); ?>/images/close_stuff_serv_button.png" alt="">
								<div class="service_name">8214039 - Banking services for large sub-commodity loans (commodity letters of credit)</div>
							</div>
						</div>
						<div class="services_self_block col-md-6 col-sm-6 col-xs-12">
							<div class="col-md-12">
								<img src="<?php print get_stylesheet_directory_uri(); ?>/images/close_stuff_serv_button.png" alt="">
								<div class="service_name">8214041 - Banking services for export and trade loans</div>
							</div>
						</div>
					</div>
					</div>
					<div class="clear_kry"></div>
					<div class="save_button_wrapp">	
						<input type="submit" value="Зберегти зміни">
					</div>
				</div>
			</div>
		</div>

	</div>
	<div class="clear"></div>
<style>
	.header{
		position: fixed;
	}
	.content-left-wrap{
		padding-top: 0;
	}
	body{
		background-color: #f4f4f4;
	}
	.container{
		padding: 0!important;
	}
</style>
<?php get_footer();  ?>