<?php 
if(!is_user_logged_in()){
	wp_redirect(get_home_url(), 302);
}
get_header();  
if(class_exists('CrmApi')){
	$api = new CrmApi;
	$crm_auth = $api->crm_auth();
	$current_user = wp_get_current_user();
	$usr_api_arr = $api->get_contact_data_by_email($current_user->user_email);	
}else{
	$crm_auth = false;	
	$usr_api_arr = [];
}
?>
<div class="clear"></div>
</header>
<div id="content" class="site-content">

	<div class="container">

		<div class="content-left-wrap col-md-12">			
			<div class="user_events_blocks col-md-9 col-sm-12 col-xs-12">
				<div class="event_block active_event_block">
					<a href="#">
						<div class="event_block_title">
							<p>Особиста інформація</p><img src="<?php print get_stylesheet_directory_uri(); ?>/images/arr_right.svg" alt="">
						</div>
						<div class="clear_kry"></div>
						<div class="event_block_desc">
							Редагуйте інформацію про себе та свою компанію
						</div>
					</a>
				</div>
				<div class="event_block">
					<a href="<?php echo home_url();?>/epo_profile/services_list/">
						<div class="event_block_title">
							<p>Послуги</p><img src="<?php print get_stylesheet_directory_uri(); ?>/images/arr_right.svg" alt="">
						</div>
						<div class="clear_kry"></div>
						<div class="event_block_desc">
							Отримайте оцінку свого бізнесу та коментарі від експертів
						</div>
					</a>
				</div>
				<div class="event_block">
					<a href="<?php echo home_url();?>/epo_profile/events_for_user/">
						<div class="event_block_title">
							<p>Події</p><img src="<?php print get_stylesheet_directory_uri(); ?>/images/arr_right.svg" alt="">
						</div>
						<div class="clear_kry"></div>
						<div class="event_block_desc">
							Найактуальніші події та нетворкінг для експортерів
						</div>
					</a>
				</div>
				<hr>
			</div>
			<div class="clear_kry"></div>
			<div class="user_dash_block col-md-12">
				<div class="overflow_kry"></div>
				<div class="change_pass_popup">
					<form action="" method="post" class="change_pass_form">
						<div class="change_pass_popup_head for_disable">
							<span>Зміна паролю</span>
							<img src="<?php print get_stylesheet_directory_uri(); ?>/images/close_button.svg" alt="">
						</div>	
						<div class="autorization_tatus" style="padding: 20px 0 0 0; margin-bottom: -10px;"></div>
						<div class="change_pass_popup_current for_disable">
							<label for="">Поточний пароль</label>
							<input class="hiden_pass" type="password" name="old_pass" required>
							<i class="fa fa-eye-slash" aria-hidden="true"></i>
							<i class="fa fa-eye" aria-hidden="true"></i>
						</div>
						<div class="autorization_error_massenge pass_not_match"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>Паролі не співпадають</div>
						<div class="change_pass_popup_new for_disable">
							<label for="">Новий пароль</label>
							<input class="hiden_pass" type="password" name="new_pass_1" required>
							<i class="fa fa-eye-slash" aria-hidden="true"></i>
							<i class="fa fa-eye" aria-hidden="true"></i>
						</div>
						<div class="change_pass_popup_new_confirm for_disable">
							<label for="">Повторіть новий пароль</label>
							<input class="hiden_pass" type="password" name="new_pass_2" required>
							<i class="fa fa-eye-slash" aria-hidden="true"></i>
							<i class="fa fa-eye" aria-hidden="true"></i>
						</div>
						<input type="submit" value="Змінити пароль" class="for_disable">
					</form>
				</div>
				<div class="user_greeting_wrap col-md-9 col-sm-12 col-xs-12">	
					<div class="user_greeting col-md-9 col-sm-8 col-xs-8 nopadding">
						Ласкаво просимо, <br>
						<span class="user_greeting_name_block"> <?php if(isset($usr_api_arr['Name'])){ print $usr_api_arr['Name']; } ?></span>!
						<div style="cleaR:both;"></div>
						<?php
						/*
						<pre>
						<?php print_r($usr_api_arr); ?>
						</pre>
						*/
						?>
					</div>
					<div class="user_navigaton">
						<div class="change_pass">Зміна паролю</div>
						<a href="<?php echo wp_logout_url( home_url() ); ?>" class="quit_from_user_dashboard">Вихід</a>
					</div>
					<div class="fill_block_top"></div>
				</div>
				<div class="clear_kry"></div>
				<div class="main_info_about_user col-md-12 col-sm-12 col-xs-12">
					<div class="user_self_info_wrapp col-md-9 col-sm-12 col-xs-12">
						<div class="user_self_info col-md-12 col-sm-12 col-xs-12">	
							<div class="user_info_block_title">Загальна інформація</div>
							<div class="about_user_blocks">
								<div class="user_photo_wrapp">
									<div class="user_photo">
										<div class="user_photo_overlay"></div>
										<img src="<?php print get_stylesheet_directory_uri(); ?>/images/contact.png" alt="">
									</div>
									<div class="clear_kry"></div>
									<input type="file" id="upload_avatar" class="upload_photo" style="display:none;">
									<label class="label_upload_avatar" for="upload_avatar" ><img class="upload_image" src="<?php print get_stylesheet_directory_uri(); ?>/images/upload_button.png" alt=""><span>Завантажити фото</span></label>
								</div>
								<div class="avatar-block">
									<div class="avatar-block__choose choose-picture">
										<div class="close-button"><i class="icon-bar-close"></i><i class="icon-bar-close"></i></div>
										<div class="choose-button">
											<input type="file" id="upload_avatar_mobile" class="upload_photo" style="display:none;">
											<label for="upload_avatar_mobile">
												<span>Завантажити фото</span>
											</label>
										</div>
									</div>
									<div class="avatar-block__choose">
										<div class="close-button"><i class="icon-bar-close"></i><i class="icon-bar-close"></i></div>
										<div class="download-block">
											<div class="download-block__image"><img src="<?php print get_stylesheet_directory_uri(); ?>/images/simple-line-icons.png" alt=""></div>
											<div class="download-block__info">
												<div class="title">
 													<span class="title__name">my_visa_photo.jpg <span class="title__name_size">(353 kb)</span>
													<img class="title__delete" src="<?php print get_stylesheet_directory_uri(); ?>/images/thrash.svg" alt="delete avatar">
												</div>
												<div class="progress-bar"></div>
											</div>
										</div>
									</div>
									</div>
								<div class="user_info_wrapp">
									<div class="user-block">
										<div class="user-block__title">П.І.Б</div>
										<div class="user-block__content">
											<input type="text" id="user_name" name="user_name" value="<?php if(isset($usr_api_arr['Name'])){ print $usr_api_arr['Name']; } ?>">
										</div>
									</div>
									<div class="user-block">
										<div class="user-block__title">Стать</div>
										<div class="user-block__content about_user_sex">
											<select data-placeholder="Оберіть" name="user_gender">
												<option disabled selected></option>
												<option <?php if(isset($usr_api_arr['Gender']) && $usr_api_arr['Gender'] == 'Чоловіча'){ print 'selcted="selected"'; } ?>>
													Чоловіча
												</option>	
												<option <?php if(isset($usr_api_arr['Gender']) && $usr_api_arr['Gender'] == 'Жіноча'){ print 'selcted="selected"'; } ?>>
													Жіноча
												</option>
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="clear_kry"></div>
						
						<?php 
						if(isset($usr_api_arr['ContactCareer'])){ 
						
							foreach($usr_api_arr['ContactCareer'] as $ContactCareerItem){
								?>
								<div class="user_company_workplace_block col-md-12 col-sm-12 col-xs-12" style="display: block !important;">
									<div class="user_info_block_title">Роль у компанії "<span class="company_name"><?php print $ContactCareerItem['AccountName']; ?></span>"</div>
									<div class="workplace_user_blocks">
										<div class="user_info_wrapp">
											<div class="user-block">
												<div class="user-block__title">Посада</div>									
												<div class="user-block__content">
													<select name="" class="user_info_company_workplace_select" data-placeholder="Оберіть посаду">
														<option value="<?php print $ContactCareerItem['JobTitle']; ?>"><?php print $ContactCareerItem['JobTitle']; ?></option>
														<option value="Фiзисна особа пiдприємець">Член правління</option>
														<option value="Фінасновий директор">Фінасновий директор</option>
														<option value="Експортний менеджер">Експортний менеджер</option>
													</select>
												</div>
											</div>
											<div class="user-block">
												<div class="user-block__title"><?php print $ContactCareerItem['FullJobTitle']; ?></div>
												<div class="user-block__content">
													<input type="text">
												</div>
											</div>								
										</div>
									</div>
								</div>								
								<?php
							}
							
						} 
						?>
						
						<div class="user_feedback_block col-md-12 col-sm-12 col-xs-12">	
							<div class="user_info_block_title">Засоби зв'язку</div>
							<div class="about_user_blocks">
								<div class="user_info_wrapp">
									<div class="user_info_email_blocks">
										<div class="user-block__title">Email</div>									
										<div class="user-block__content user_info_feedback_input">
											<div class="inputs_blocks">
												 <?php 
												 if(isset($ContactCommunication['UsrMainMail'])){
													 
												 }
												 if(isset($usr_api_arr['ContactCommunication'])){ 
													foreach($usr_api_arr['ContactCommunication'] as $ContactCommunication){
														if($ContactCommunication['type'] == 'Email'){
															?>
															<div class="inputs_block_inner inputs_block_inner_email">
																<input type="email" class="col-md-12 col-sm-12 col-xs-12 email" value="<?php print $ContactCommunication['value']; ?>">
																<div class="remove_this_input"><img src="http://epo.org.ua/wp-content/themes/KD_EPO/images/thrash.svg" alt=""></div>
																<span class="setp_main_email">Встановити як основний</span>
															</div>																
															<?php
														}
													}
												 } 
												 ?>																					
											</div>
											<img class="pluss_icon" src="<?php print get_stylesheet_directory_uri(); ?>/images/add.svg" alt="">
											<span class="text_after_plus_icon">Додати e-mail</span>
										</div>
									</div>
									<div class="user_info_phone_blocks col-md-12 col-sm-12 col-xs-12">
										<div class="user-block__title">Мобільний телефон</div>
										<div class="user-block__content user_info_feedback_input">
											<div class="inputs_blocks">
												 <?php 
												 if(isset($usr_api_arr['ContactCommunication'])){ 
													foreach($usr_api_arr['ContactCommunication'] as $ContactCommunication){
														if($ContactCommunication['type'] == 'Мобильный телефон'){
															?>
															<div class="inputs_block_inner inputs_block_inner_email">
																<input type="text" class="col-md-12 col-sm-12 col-xs-12 phone" value="<?php print $ContactCommunication['value']; ?>">
																<div class="remove_this_input"><img src="http://epo.org.ua/wp-content/themes/KD_EPO/images/thrash.svg" alt=""></div>
															</div>																
															<?php
														}
													}
												 } 
												 ?>											
											</div>
											<img class="pluss_icon" src="<?php print get_stylesheet_directory_uri(); ?>/images/add.svg" alt="">
											<span class="text_after_plus_icon">Додати телефон</span>
										</div>
									</div>
									<div class="user_info_work_phone_blocks col-md-12 col-sm-12 col-xs-12">
										<div class="user-block__title">Робочий телефон</div>
										<div class="user-block__content user_info_feedback_input">
											<div class="inputs_blocks">
												 <?php 
												 if(isset($usr_api_arr['ContactCommunication'])){ 
													foreach($usr_api_arr['ContactCommunication'] as $ContactCommunication){
														if($ContactCommunication['type'] == 'Рабочий телефон'){
															?>
															<div class="inputs_block_inner inputs_block_inner_email">
																<input type="text" class="col-md-12 col-sm-12 col-xs-12 work_phone_mask" value="<?php print $ContactCommunication['value']; ?>">
																<div class="remove_this_input"><img src="http://epo.org.ua/wp-content/themes/KD_EPO/images/thrash.svg" alt=""></div>
															</div>																
															<?php
														}
													}
												 } 
												 ?>												
											</div>
											<img class="pluss_icon" src="<?php print get_stylesheet_directory_uri(); ?>/images/add.svg" alt="">
											<span class="text_after_plus_icon">Додати телефон</span>
										</div>
									</div>
									<div class="user_info_skype_blocks col-md-12 col-sm-12 col-xs-12">
										<div class="user-block__title">Skype</div>
										<div class="user-block__content user_info_feedback_input">
											<div class="inputs_blocks">
												 <?php 
												 if(isset($usr_api_arr['ContactCommunication'])){ 
													foreach($usr_api_arr['ContactCommunication'] as $ContactCommunication){
														if($ContactCommunication['type'] == 'Skype'){
															?>
															<div class="inputs_block_inner inputs_block_inner_email">
																<input type="text" class="col-md-12 col-sm-12 col-xs-12 skype" value="<?php print $ContactCommunication['value']; ?>">
																<div class="remove_this_input"><img src="http://epo.org.ua/wp-content/themes/KD_EPO/images/thrash.svg" alt=""></div>
															</div>																
															<?php
														}
													}
												 } 
												 ?>												
											</div>
											<img class="pluss_icon" src="<?php print get_stylesheet_directory_uri(); ?>/images/add.svg" alt="">
											<span class="text_after_plus_icon">Додати skype</span>
										</div>
									</div>
									<div class="user_check_dispatch col-md-12 col-sm-12 col-xs-12">
										<div class="user-block__title">&nbsp;</div>
										<div class="user-block__content user_checkbox_wrapp">
											<input type="checkbox" class="checkbox" id="checkbox" /><label for="checkbox"></label>
											<span class="text_after_checkbox">Згоден отримувати розсилку</span>
										</div>
									</div>
								</div>
								<div class="user_info_submit_block col-md-12 col-sm-12 col-xs-12">
									<div class="col-md-9 col-sm-9 col-xs-8"></div>
									<div class="col-md-3 col-sm-3 col-xs-4">
										<input type="submit" value="Зберегти зміни">
									</div>
								</div>
							</div>
						</div>
						<div class="fill_block_white"></div>						
					</div>

					<div class="user_company_info col-md-3  col-sm-12 col-xs-12">
						<div class="user_info_block_title">Мої компанії</div>
    						<div class="overlay_companies"></div>
						<div class="clear_kry"></div>
						<!--  Если пользователь залогиненый -->	
						<div class="logged_user" style="display: block !important;">
							<?php
							foreach($usr_api_arr['ContactCareer'] as $ContactCareerItem){
								?>
								<div class="company_info_box active_company_info_box col-md-12 col-sm-12 col-xs-12">
									<span class="company_side_line"></span>
									<div class='remove_this_company'>
										<img src="<?php print get_stylesheet_directory_uri(); ?>/images/thrash.svg" alt="">
									</div>
									<img src="<?php print $ContactCareerItem['AccountLogo']; ?>" alt="" class="user_logged_company_img">
									<div class="logged_company_names_wrapp">
										<span class="logged_company_name"><?php print $ContactCareerItem['AccountName']; ?></span>
										<span class="logged_company_reg_num"><?php print $ContactCareerItem['UsrEdrpouCode']; ?></span>									
									</div>
									<div class="clear_kry"></div>
									<a href="<?php echo home_url();?>/storinka-kompaniyi/">Оновити інформацію</a>
								</div>	
								<div class="clear_kry"></div>							
								<?php
							}	
							?>
							<div class="logged_user_add_company_button">
								<img src="<?php print get_stylesheet_directory_uri(); ?>/images/add_company_button.png" alt="">Додати компанію
							</div>
						</div>				
					</div>
					
					
					<div class="fill_block"></div>					
				</div>
			</div>
			<div class="fill_block_bottom"></div>
						<div class="clear_kry"></div>
		</div>
		<div class="clear"></div>
	</div>
	<style>
		.header{
			position: fixed;
		}

		.content-left-wrap{
			padding-top: 0;
		}

		footer{
			margin-top: 0;
		}

		@media only screen and (max-width: 767px){
			#footer {
				padding-top: 0;
				margin-top: 0px;
			}
		}
	</style>
<?php get_footer();  ?>