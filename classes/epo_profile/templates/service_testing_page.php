<?php 
if(!is_user_logged_in()){
	wp_redirect(get_home_url(), 302);
}
get_header();  
?>
<div class="clear"></div>
</header>
<div class="overflow_events_kry"></div>
<div id="content" class="site-content">
	<div class="container">
		<div class="content-left-wrap company_testing col-md-12">
			<div class="testing_page_head col-md-9 col-sm-12 col-xs-12">
				<div class="testing_page_title col-md-8 col-sm-7 col-xs-9">Результати тесту</div>
				<div class="testing_page_sub_title col-md-10 col-sm-7 col-xs-9">Дякуємо за проходження тесту. Нижче показано загальний й проміжні результати, а також список порад, що допоможуть Вам краще підготуватися до експорту.</div>
				<div class="testing_page_head_result_block middle_test_result"><span class="test_result">65</span>/100 <span>результат</span></div>
				<div class="fill_block_top"></div>
			</div>
			<div class="test_info_wrapp testing_page_main_content col-md-9 col-sm-12 col-xs-12">
				<div class="chart">
					<div class="chart-titles">
						<div class="chart-title">Підприємство</div>
						<div class="chart-title">Веб-сайт</div>
						<div class="chart-title">Розвиток</div>
						<div class="chart-title">Виробничий процес</div>
						<div class="chart-title">Розуміння та плани</div>
						<div class="chart-title">Конкуренція</div>
						<div class="chart-title">Споживчі ринки</div>
						<div class="chart-title">Продажи та маркетинг</div>
						<div class="chart-title">Продукція та ціни</div>
						<div class="chart-title">Канали дистрибуції</div>
						<div class="chart-title">Покращення та логістика</div>
						<div class="chart-title">Ресурси та знання</div>
					</div>
					<div class="chart-block">
						<div class="chart-block__inner">
							<ul class="chart-dividing">
								<li class="chart-dividing__line"></li>
								<li class="chart-dividing__line"></li>
								<li class="chart-dividing__line"></li>
								<li class="chart-dividing__line"></li>
								<li class="chart-dividing__line"></li>
								<li class="chart-dividing__line"></li>
								<li class="chart-dividing__line"></li>
								<li class="chart-dividing__line"></li>
								<li class="chart-dividing__line"></li>
								<li class="chart-dividing__line"></li>
							</ul>
							<ul class="chart-data">
								<li class="chart-data__bar" style="width:15%; height: 15%;"><span class="chart-data__percent">15%</span></li>
								<li class="chart-data__bar" style="width:8%; height: 8%;"><span class="chart-data__percent">8%</span></li>
								<li class="chart-data__bar" style="width:83%; height: 83%;"><span class="chart-data__percent">83%</span></li>
								<li class="chart-data__bar" style="width:100%; height: 100%;"><span class="chart-data__percent">100%</span></li>
								<li class="chart-data__bar" style="width:100%; height: 100%;"><span class="chart-data__percent">100%</span></li>
								<li class="chart-data__bar" style="width:25%; height: 25%;"><span class="chart-data__percent">25%</span></li>
								<li class="chart-data__bar" style="width:55%; height: 55%;"><span class="chart-data__percent">55%</span></li>
								<li class="chart-data__bar" style="width:78%; height: 78%;"><span class="chart-data__percent">78%</span></li>
								<li class="chart-data__bar" style="width:55%; height: 55%;"><span class="chart-data__percent">55%</span></li>
								<li class="chart-data__bar" style="width:33%; height: 33%;"><span class="chart-data__percent">33%</span></li>
								<li class="chart-data__bar" style="width:45%; height: 45%;"><span class="chart-data__percent">45%</span></li>
								<li class="chart-data__bar" style="width:65%; height: 65%;"><span class="chart-data__percent">65%</span></li>
							</ul>
							<ul class="chart-grade">
								<li class="chart-grade__percent">0%</li>
								<li class="chart-grade__percent">10%</li>
								<li class="chart-grade__percent">20%</li>
								<li class="chart-grade__percent">30%</li>
								<li class="chart-grade__percent">40%</li>
								<li class="chart-grade__percent">50%</li>
								<li class="chart-grade__percent">60%</li>
								<li class="chart-grade__percent">70%</li>
								<li class="chart-grade__percent">80%</li>
								<li class="chart-grade__percent">90%</li>
								<li class="chart-grade__percent">100%</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="testing_graph_block col-md-12"></div>
				<div class="testing_result_text_block col-md-12">
					<div class="testing_result_text_block_title">Підприємство<span class="result_percents bad_test_result_text">15%</span></div>
					<div class="testing_result_text_block_content">
						<p>Вихід на нові ринки вимагає залучення окремих ресурсів. Відсутність експортного відділу може суттєво завадити Вашій компанії досягти успіху на зовнішніх ринках через неможливість належним чином приділити увагу питанню інтернаціоналізації бізнесу.</p>
					</div>
				</div>
				<div class="testing_result_text_block col-md-12">
					<div class="testing_result_text_block_title">Веб-сайт<span class="result_percents very_bad_test_result_text">8%</span></div>
					<div class="testing_result_text_block_content">
						<p>Позиціювання в медіапросторі — обов'язковий компонент у рамках интернационалізаії бізнесу, вам необхідно створити сайт
						компанії, що включатиме англомовну версію і каталоги продукції. Відсутність веб-сторінки компанії рівнозначне відсутності візитної картки на Вашій зустрічі з кліснтом.</p>
						<p>Вам необхідно розмістити на сайті компанії інформацпо про продукцію англійською мовою, щоб потенційні партнери могли
						ознайомишсь з нею без залучеиня перекладачів</p>
					</div>
				</div>
				<div class="testing_result_text_block col-md-12">
					<div class="testing_result_text_block_title">Розвиток<span class="result_percents perfect_test_result_text">83%</span></div>
					<div class="testing_result_text_block_content">
						<p>Вихід на зовнішні ринки супроводжується високим рівием конкуреицїї, Вам потрібно бути впевненим у якості товарів/послу
							Вашого підприємства. можливо, варто інвесгувати у виробництво і його майбутню стабільність, перш ніж поширювати свою
						присутність на зовнішні ринки.</p>
					</div>
				</div>
				<div class="testing_result_text_block col-md-12">
					<div class="testing_result_text_block_title">Конкуренція<span class="result_percents bad_test_result_text">25%</span></div>
					<div class="testing_result_text_block_content">
						<p>Експорт є засобом створення додатковою попиту на продукти або послуги Вашої компанії. Вам варто пересвідчитись, що ви
						зможете його задовольнити.</p>
					</div>
				</div>
				<div class="testing_result_text_block col-md-12">
					<div class="testing_result_text_block_title">Споживачі та ринки<span class="result_percents middle_test_result_text">55%</span></div>
					<div class="testing_result_text_block_content">
						<p>Для реалізаціі товарів/послуг необхідно чітко визначити їх цільову аудиторію. Залежно від результату визначення Ви можете
						побудувати політику позиціювання і дистрибуції товарів/послуг.</p>

						<p>Необхідно критично оцінити власний продукт і зрозуміти, які його властивості можуть суперечити вподобанням вашої цільової
						аудиторії.</p>

						<p>Окупніпь зусиль із інтернацноналізації бізнесу, ваші прибутки, рентабельність, а іноді й існування компаніі залежить в тому числі
						від меж майбутнього попиту. Визначте ці межі перед тим як виходити назовнішній ринок.</p>

						<p>Сертифікація, стандарти, мита та інші регуляторні обмеження можуть слугувати вирішальними факторами при розгляді виходу
						на той чи інший ринок. Чи впевнені Ви, що обраний рииок є прийнятим для Вас в рамках згаданих Факторів?</p>
					</div>
				</div>
				<div class="testing_result_text_block col-md-12">
					<div class="testing_result_text_block_title">Продажі та маркетинг<span class="result_percents good_test_result_text">78%</span></div>
					<div class="testing_result_text_block_content">
						<p>Навіть якщо Ви плануєте реалізовувати свої товари/послуги за допомогою партнера за кордоном, ніхто не знає Ваш продукт
						краще за Вас, отже, саме Ваша компания має розробити план продажів.</p>

						<p>Втілення плану устпішного виходу на закордонні ринки вимагає залучення значних ресурсів. Визначення необхідного рівня
						продажів на новому ринку є передумовою того, що залучені ресурси не будуть витрачені даремно.</p>
					</div>
				</div>
				<div class="testing_result_text_block col-md-12">
					<div class="testing_result_text_block_title">Продукція та ціни<span class="result_percents middle_test_result_text">55%</span></div>
					<div class="testing_result_text_block_content">
						<p>Вам треба точно визначитж які складові впливатимуть на майбутню ціну товару/послуги, а також яким чином цей вплив буде
						реалізовано.</p>
					</div>
				</div>
				<div class="testing_result_text_block col-md-12">
					<div class="testing_result_text_block_title">Канали дистрибуції<span class="result_percents bad_test_result_text">33%</span></div>
					<div class="testing_result_text_block_content">
						<p>Після започаткування співробітництва із закордонним партнером Вам потрібно мати план дій щодо контролю та стимулюванням
						збуту своїх товарів/послуг. Чи спрямовуватиме Ваш партнер зусилля на просування продукції саме Башої компанії?</p> 

						<p>Сервісне обслуговувания вашої продукцй або відповідальність за якість наданих послут - важливий фактор нецінової
						конкуренції, тому потрібно точно визначити шляхи йото здійснения.</p>
					</div>
				</div>
				<div class="testing_result_text_block col-md-12">
					<div class="testing_result_text_block_title">Покращення та логістика<span class="result_percents bad_test_result_text">45%</span></div>
					<div class="testing_result_text_block_content">
						<p>Логістика - один з ключових фактрів виходу на зовнішні ринки і вагома складова ціни. Необхідно точно розуміти, яким чином
						Ви будете доставляти свій продукт на зовнішній ринок.</p>
					</div>
				</div>
				<div class="testing_result_text_block col-md-12">
					<div class="testing_result_text_block_title">Ресурси та знання<span class="result_percents middle_test_result_text">65%</span></div>
					<div class="testing_result_text_block_content">
						<p>Проаналізуйте яких знань не вистачає персоналу Башої компаніі для здійснення ефективної експортної діяльності.</p>

						<p>Вихід на нові ринки вимагає додаткових знань та навичок. Визначте перелік ресурсів, які допоможуть Вам отримати іх.
						Подумайте, чи варто залучати допомогу фахівців замість того, щоб реалізовувати певні діі самостійно.</p>
					</div>
				</div>
				<div class="test_page_bottom_links_item">
					<div class="print"><i class="fa fa-print" aria-hidden="true"></i><span>Надрукувати результат</span></div>
					<div class="copy"><i class="fa fa-clone" aria-hidden="true"></i><span>Скопіювати посилання</span></div>
					<a href="<?php echo home_url();?>/epo_profile/dashboard/" class="home"><img src="<?php print get_stylesheet_directory_uri(); ?>/images/back_arrow_wth_cir.svg" alt=""><span>До особистого кабінету</span></a>
				</div>
				<div class="fill_block_white"></div>
			</div>	
			<div class="user_company_info services_page_company_aside single_service_page_company_aside testing_page_aside col-md-3  col-sm-12 col-xs-12">
				<div class="user_info_block_title">Дані заявки</div>
				<div class="clear_kry"></div>
				<div class="company_info_box">
					<div class="company_info_box_title">
						Вкажіть дані вашої компанії, щоб отримати доступ до послуг:
					</div>
					<div class="company_inputs_block">
						<label for="">Найменування юридичної особи</label>
						<input type="text" placeholder="напр. Ексім-меблі 3000">
						<label for="">Реєстраційний код (ЄРДПОУ або ІПН)</label>
						<input type="text">
						<label for="">Ваша посада</label>
						<input type="text">
						<label for="">Повна назва посади</label>
						<input type="text">
					</div>
					<input type="button" class="add_company_button" value="Додати компанію">
				</div>
				<div class="sidebar-info">
					<div class="overlay_companies"></div>
					<div class="sidebar-info__worker">
						<div class="worker-name">Валентина Іванівна</div>
						<div class="worker-position">Експортний менеджер</div>
						<div class="worker-phone">+38 (123) 456-78-90</div>
						<div class="worker-email">v.i.lol@credit-agricole.ua</div>
					</div>
					<div class="sidebar-info__company">
						<div class="company-block-image">
							<img src="<?php print get_stylesheet_directory_uri(); ?>/images/credi_logo.png" alt="">
						</div>
						<div class="company-block-info">
							<div class="company-block-info__name">Агріколь</div>
							<div class="company-block-info__phone">12345678</div>
						</div>
					</div>
					<div class="actual-info">
						<div class="actual-info__link"><a href="">Актуалізувати дані</a></div>
						<div class="actual-info__date"><i class="fa fa-calendar-o" aria-hidden="true"></i><span>05/01/2018</span></div>
					</div>
				</div>						
			</div>
			<div class="fill_block"></div>	
		</div>
		<div class="fill_block_bottom"></div>
		<div class="clear_kry"></div>
		<div class="carousel">
			<div class="overlay_companies"></div>
			<div class="services_carosel_title">Інші послуги</div>
			<div class="services_carousel owl-carousel">
				<a href="<?php echo home_url();?>/epo_profile/readliness_assessment_page/">
					<div class="corusel_item">
						<div class="service_carousel_item_title">Оцінка готовності</div>
						<div class="service_carousel_item_description">Оцініть свою готовність до зовнішньоекономічної діяльності. Визначте компетенції, яких вам бракує для досягення успіху.</div>
					</div>
				</a>
				<a href="<?php echo home_url();?>/epo_profile/checking_idea_page/">
					<div class="corusel_item">
						<div class="service_carousel_item_title">Перевірка ідеї</div>
						<div class="service_carousel_item_description">Отримайте допомогу у розробці плану інтернаціоналізації вашого бізнесу.</div>
					</div>
				</a>
				<a href="<?php echo home_url();?>/epo_profile/mentorstvo_page/">
					<div class="corusel_item">
						<div class="service_carousel_item_title">Менторство</div>
						<div class="service_carousel_item_description">Зв'язок з фахівцями, які зможуть надати професійну допомогу із пошуку партнерів та аналізу ринку.</div>
					</div>
				</a>
				<a href="<?php echo home_url();?>/epo_profile/internationalization_bussiness_page/">
					<div class="corusel_item">
						<div class="service_carousel_item_title">Інтернаціоналізація бізнесу</div>
						<div class="service_carousel_item_description">Зв'язок з фахівцями, які зможуть надати професійну допомогу із пошуку партнерів та аналізу ринку.</div>
					</div>
				</a>
				<a href="<?php echo home_url();?>/epo_profile/sourcing_service_page/">
					<div class="corusel_item">
						<div class="service_carousel_item_title">Soursing</div>
						<div class="service_carousel_item_description">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</div>
					</div>
				</a>
				<a href="<?php echo home_url();?>/epo_profile/een_page/">
					<div class="corusel_item">
						<div class="service_carousel_item_title">Enterprise Europe Network</div>
						<div class="service_carousel_item_description">EEN інформує про можливості розвитку в певній сфері, допомагає в залученні інвестицій для виробництва товарів і послуг, залученні нових технологій для підвищення конкурентноспроможності українських товаровиробників та підприємств, які надають послуги розвитку технологій, які були розроблені в Україні за допомогою закордонних партнерів.</div>
					</div>
				</a>			
			</div>
		</div>
		<div class="clear_kry"></div>
	</div>
	<div class="clear"></div>
</div>
<style>
.header{
	position: fixed;
}

.content-left-wrap{
	padding-top: 0;
}
</style>
<?php get_footer();  ?>