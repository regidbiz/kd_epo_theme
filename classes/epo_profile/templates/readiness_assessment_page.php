<?php 
if(!is_user_logged_in()){
	wp_redirect(get_home_url(), 302);
}
get_header();  
?>
<div class="clear"></div>
</header>
<div class="overflow_events_kry"></div>
<div id="content" class="site-content">
	<div class="container">
		<div class="content-left-wrap col-md-12">
			<div class="popup_for_search_KVED">
				<img class="close_search_popup" src="<?php print get_stylesheet_directory_uri(); ?>/images/close_button.svg" alt="">
				<div class="serch_title">Додати КВЕД</div>
				<div class="clear_kry"></div>
				<div class="main_search_wrapp">
					<div class="search_button_wrapp">
						<input type="submit" value=""><i class="fa fa-search" aria-hidden="true"></i>
					</div>
					<input name="s" type="search" placeholder="Введіть назву товару/послуги або КВЕД" class="search_input">
					
					<div class="search_results">	
						<ul class="list-kved">
							<li class="list-kved__item">
								<div class="list-kved__point"></div>
								<span class="list-kved__title">80 - Услуги бизенса</span>
								<ul>
									<li class="list-kved__item">
										<span class="list-kved__point"></span>
										<span class="list-kved__title">80 - Услуги бизенсу</span>
									</li>
									<li class="list-kved__item">
										<span class="list-kved__point"></span>
										<span class="list-kved__title">Финансовые и страховые услуги</span>
										<ul>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Бумага, печатные и издательские услуги</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Досуг развлечения  и туризм</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Информационные технологии, компьютеры, программное обеспечение, Интернет, исследовани и разработки</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Металлы, станки, машины, металлообработка</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Минералы, руды, полезные ископаемые</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Образование, обучение, общественные институты</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Розничаная торговля и дистрибуци</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Сельское хозяйство, продукты питания</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Строительство</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Текстиль, одежда, кожа, часы, ювелизрные изделия</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Транспорт и логистика</span>
											</li>
										</ul>
									</li>		
								</ul>
							</li>
						</ul>
					</div>
					<div class="clear_kry"></div>
					<div class="save_button_wrapp">	
						<input type="submit" value="Зберегти зміни">
					</div>
				</div>
			</div>
			<div class="popup_for_search_HS">
				<img class="close_search_popup" src="<?php print get_stylesheet_directory_uri(); ?>/images/close_button.svg" alt="">
				<div class="serch_title">Додати товари</div>
				<div class="clear_kry"></div>
				<div class="main_search_wrapp">
					<div class="search_button_wrapp">
						<input type="submit" value=""><i class="fa fa-search" aria-hidden="true"></i>
					</div>
					<input name="s" type="search" placeholder="Введіть назву товару/послуги або HS" class="search_input">		
					<div class="search_results">	
						<ul class="list-kved">
							<li class="list-kved__item">
								<div class="list-kved__point"></div>
								<span class="list-kved__title">80 - Услуги бизенса</span>
								<ul>
									<li class="list-kved__item">
										<span class="list-kved__point"></span>
										<span class="list-kved__title">80 - Услуги бизенсу</span>
									</li>
									<li class="list-kved__item">
										<span class="list-kved__point"></span>
										<span class="list-kved__title">Финансовые и страховые услуги</span>
										<ul>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Бумага, печатные и издательские услуги</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Досуг развлечения  и туризм</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Информационные технологии, компьютеры, программное обеспечение, Интернет, исследовани и разработки</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Металлы, станки, машины, металлообработка</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Минералы, руды, полезные ископаемые</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Образование, обучение, общественные институты</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Розничаная торговля и дистрибуци</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Сельское хозяйство, продукты питания</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Строительство</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Текстиль, одежда, кожа, часы, ювелизрные изделия</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Транспорт и логистика</span>
											</li>
										</ul>
									</li>		
								</ul>
							</li>
						</ul>
					</div>
					<div class="clear_kry"></div>
					<div class="save_button_wrapp">	
						<input type="submit" value="Зберегти зміни">
					</div>
				</div>								
			</div>
			<div class="overflow_kry"></div>		
			<div class="service_learn_more_popup">
				<img class="close_service_learn_more_popup" src="<?php print get_stylesheet_directory_uri(); ?>/images/close_button.svg" alt="">
				<div class="service_learn_more_popup_title">Оцінка готовності</div>
				<div class="service_learn_more_popup_content">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus optio a nostrum ratione atque recusandae natus rerum necessitatibus dicta, quos. Minima aperiam quo quod fugiat dolor vero officiis et quasi!</p>
					<ul>
						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
					</ul>
				</div>
			</div>	
			<div class="user_events_blocks col-md-9 col-sm-12 col-xs-12">
				<div class="event_block">
					<a href="<?php echo home_url();?>/epo_profile/dashboard/">
						<div class="event_block_title">
							<p>Особиста інформація</p><img src="<?php print get_stylesheet_directory_uri(); ?>/images/arr_right.svg" alt="">
						</div>
						<div class="clear_kry"></div>
						<div class="event_block_desc">
							Редагуйте інформацію про себе та свою компанію
						</div>
					</a>
				</div>
				<div class="event_block active_event_block">
					<a href="#">
						<div class="event_block_title">
							<p>Послуги</p><img src="<?php print get_stylesheet_directory_uri(); ?>/images/arr_right.svg" alt="">
						</div>
						<div class="clear_kry"></div>
						<div class="event_block_desc">
							Отримайте оцінку свого бізнесу та коментарі від експертів
						</div>
					</a>
				</div>
				<div class="event_block">
					<a href="<?php echo home_url();?>/epo_profile/events_for_user/">
						<div class="event_block_title">
							<p>Події</p><img src="<?php print get_stylesheet_directory_uri(); ?>/images/arr_right.svg" alt="">
						</div>
						<div class="clear_kry"></div>
						<div class="event_block_desc">
							Найактуальніші події та нетворкінг для експортерів
						</div>
					</a>
				</div>
				<hr>
			</div>
		<div class="clear_kry"></div>
		<form action="" class="company_dash_block readiness_assessment_page">
			<div class="company_greeting_wrap col-md-9 col-sm-12 col-xs-12">	
				<div class="single_service_head col-md-8 col-sm-8 col-xs-12 nopadding">
					<div class="service_single_page_title">
						<a href="<?php echo home_url();?>/epo_profile/services_list/">
							<img src="<?php print get_stylesheet_directory_uri(); ?>/images/back_arrow.png" alt="">
						</a>
						<span>Оцінка готовності</span>
					</div>
					<p class="service_single_page_description">Оцініть свою готовність до зовнішньоекономічної діяльності. Визначте компетенції, яких вам бракує для досягнення успіху.</p>
				</div>
				<div class="service_learn_more col-md-4 col-sm-4 col-xs-12">Детальніше про послугу</div>
				<div class="fill_block_top"></div>		
			</div>
			<div class="clear_kry"></div>
			<div class="main_info_about_user col-md-12 col-sm-12 col-xs-12">
				<div class="user_self_info_wrapp col-md-9 col-sm-12 col-xs-12">
					<div class="single_event_reg_step col-md-12 col-sm-12 col-xs-12">
						<div class="single_event_step_title">I. Оберіть контактну особу</div>
						<div class="single_event_participant open_selection_user_bloks_wrpp col-md-6 col-sm-8 col-xs-12">
							<div class="participant_photo_block" style='background-image: url("<?php print get_stylesheet_directory_uri(); ?>/images/user_photo.jpg ?>");background-size:cover; background-repeat:no-repeat; background-position: left center'></div>
							<div class="participant_name_block">Дмитро Носов</div>
							<div class="participant_position_block">Фінансовий директор</div>
							<div class="chevron_wrapp">
								<i class="fa fa-chevron-down" aria-hidden="true"></i>
							</div>
							<div class="selection_user_bloks_wrapp">
								<div class="user_block_in_selection">
									<div class="single_event_participant col-md-12 col-sm-12 col-xs-12">
										<div class="participant_photo_block" style='background-image: url("<?php print get_stylesheet_directory_uri(); ?>/images/default_user_image.svg ?>");background-size:cover; background-repeat:no-repeat; background-position: left center'></div>
										<div class="participant_name_block">Валентина Іванівна</div>
										<div class="participant_position_block">Експортний менеджер</div>
									</div>
								</div>
								<div class="user_block_in_selection">
									<div class="single_event_participant col-md-12 col-sm-12 col-xs-12">
										<div class="participant_photo_block" style='background-image: url("<?php print get_stylesheet_directory_uri(); ?>/images/default_user_image.svg ?>");background-size:cover; background-repeat:no-repeat; background-position: left center'></div>
										<div class="participant_name_block">Валентина Іванівна</div>
										<div class="participant_position_block">Експортний менеджер</div>
									</div>
								</div>
								<div class="user_block_in_selection">
									<div class="single_event_participant col-md-12 col-sm-12 col-xs-12">
										<div class="participant_photo_block" style='background-image: url("<?php print get_stylesheet_directory_uri(); ?>/images/default_user_image.svg ?>");background-size:cover; background-repeat:no-repeat; background-position: left center'></div>
										<div class="participant_name_block">Валентина Іванівна</div>
										<div class="participant_position_block">Експортний менеджер</div>
									</div>
								</div>
							</div>							
						</div>
					</div>
					<div class="single_event_reg_step_info col-md-12 col-sm-12 col-xs-12">
						<div class="user_info_block_title">II. Перевірте актуальність даних</div>

			
						<div class="single_event_participant_info col-md-12 col-sm-12 col-xs-12">
							<div class="user_info_wrapp">
								<div class="user-block">
									<div class="user-block__title">П.І.Б</div>
									<div class="user-block__content">
										<input type="text" id="user_name">
									</div>
								</div>
								<div class="user-block">
									<div class="user-block__title">Посада</div>									
									<div class="user-block__content">
									<!--<input type='text' class='col-md-12 col-sm-12 col-xs-12'>-->
										<select name="" class="user_info_company_workplace_select" data-placeholder="Оберіть посаду">
											<option disabled selected></option>
										<option value="Фiзисна особа пiдприємець">Член правління</option>
											<option value="Фінасновий директор">Фінасновий директор</option>
										<option value="Експортний менеджер">Експортний менеджер</option>
										</select>
									</div>
								</div>
								<div class="user-block">
									<div class="user-block__title">Повна назва посади</div>
									<div class="user-block__content">
										<input type="text">
									</div>
								</div>
								<div class="user-block user_info_email_blocks">
									<div class="user-block__title">Email <i class="info_icon fa fa-info" aria-hidden="true"></i></div>									
									<div class="user-block__content user_info_feedback_input">
										<div class="inputs_blocks">
											<div class="inputs_block_inner inputs_block_inner_email">
												<input type="email" class="col-md-12 col-sm-12 col-xs-12 email">
												<div class="remove_this_input"><img src='<?php print get_stylesheet_directory_uri(); ?>/images/thrash.svg' alt=''></div>
												<span class="setp_main_email">Встановити як основний</span>
											</div>
										</div>
										<img class="pluss_icon" src="<?php print get_stylesheet_directory_uri(); ?>/images/add.svg" alt="">
										<span class="text_after_plus_icon">Додати e-mail</span>
									</div>
								</div>
								<div class="user-block user_info_phone_blocks">
									<div class="user-block__title">Мобільний телефон</div>									
									<div class="user-block__content user_info_feedback_input">
										<div class="inputs_blocks">
											<div class="inputs_block_inner inputs_block_inner_phone">
												<input type="phone" class="col-md-12 col-sm-12 col-xs-12 phone">
												<div class="remove_this_input"><img src='<?php print get_stylesheet_directory_uri(); ?>/images/thrash.svg' alt=''></div>
												<span class="setp_main_phone">Встановити як основний</span>
											</div>
										</div>
										<img class="pluss_icon" src="<?php print get_stylesheet_directory_uri(); ?>/images/add.svg" alt="">
										<span class="text_after_plus_icon">Додати телефон</span>
									</div>
								</div>
								<div class="user-block user_info_work_phone_blocks">
									<div class="user-block__title">Робочий телефон</div>									
									<div class="user-block__content user_info_feedback_input">
										<div class="inputs_blocks">
											<div class="inputs_block_inner inputs_block_inner_workphone">
												<input type="workphone" class="col-md-12 col-sm-12 col-xs-12 workphone">
												<div class="remove_this_input"><img src='<?php print get_stylesheet_directory_uri(); ?>/images/thrash.svg' alt=''></div>
												<span class="setp_main_workphone">Встановити як основний</span>
											</div>
										</div>
										<img class="pluss_icon" src="<?php print get_stylesheet_directory_uri(); ?>/images/add.svg" alt="">
										<span class="text_after_plus_icon">Додати телефон</span>
									</div>
								</div>
							</div>
							<div class="info_save_icons col-md-12 col-sm-12 col-xs-12 nopadding">
								<i class="info_icon fa fa-info" aria-hidden="true"></i>
								<div class="save_event_canges"><i class="fa fa-floppy-o" aria-hidden="true"></i>Зберегти зміни</div>
							</div>
							<div class="clear_kry"></div>
						</div>
					</div>	
					<div class="single_event_reg_step_info col-md-12 col-sm-12 col-xs-12">
						<div class="user_info_block_title">III. Перевірте актуальність данних компанії</div>

			
						<div class="single_event_participant_info col-md-12 col-sm-12 col-xs-12">
							<div class="user_info_wrapp user_info_wrapp_with_border">
								<div class="user-block">
									<div class="user-block__title">Назва офіційна</div>
									<div class="user-block__content">
										<input type="text" id="company_name">
									</div>
								</div>	
								<div class="user-block">
									<div class="user-block__title">Назва альтернативна</div>
									<div class="user-block__content">
										<input type="text" id="alternate_company_name">
									</div>
								</div>	
								<div class="user-block">
									<div class="user-block__title">Реєстраційний код (ЕРДПУ або ІПН)</div>
									<div class="user-block__content user-block__content_code">
										<input type="text" id="code" class="col-md-3 col-sm-3 col-xs-12">
									</div>
								</div>	
								<div class="user-block">
									<div class="user-block__title">Форма власності</div>									
									<div class="user-block__content user-block__content_forma">
										<select name="" class="user_info_company_workplace_select" data-placeholder="Оберіть форму власності">
											<option></option>
											<option value="Закрите акціонерне товариство">Закрите акціонерне товариство</option>
											<option value="Фізична особа підприємець">Фізична особа підприємець</option>
											<option value="Приватне підприємство">Приватне підприємство</option>
											<option value="Відкрите акціонерне товариство">Відкрите акціонерне товариство</option>
											<option value="Державне підприємство">Державне підприємство</option>
										</select>
									</div>
								</div>
								<div class="user-block">
									<div class="user-block__title">Рік заснування</div>									
									<div class="user-block__content user-block__content_year">
										<select name="" class="user_info_company_workplace_select" data-placeholder="Оберіть рік заснування">
											<option></option>
											<?php
											for ($i = 2019; $i > 1900; $i--)
											{
												if ($i == 2019)
												{
													echo '<option value="1993" disabled selected style="color:#bababa;">1993</oplion>';
												}
												else
												{
													echo '<option value="'.$i.'">'.$i.'</oplion>';
												}
											}
											?>
										</select>
									</div>
								</div>
							</div>
							<div class="user_info_wrapp user_info_wrapp_with_border">
								<div class="user-block">
									<div class="user-block__title">Область</div>									
									<div class="user-block__content user-block__content_region">
										<select name="" class="user_info_company_workplace_select" data-placeholder="Оберіть">
											<option></option>
											<option>Київська</option>
											<option>Харківська</option>
											<option>Вінницька</option>
											<option>Волинська</option>
											<option>Дніпропетровська</option>
											<option>Донецька</option>
											<option>Житомирська</option>
											<option>Закарпатська</option>
											<option>Запорізька</option>
											<option>Івано-Франківська</option>
											<option>Кіровоградська</option>
											<option>Луганська</option>
											<option>Львівська</option>
											<option>Миколаївська</option>
											<option>Одеська</option>
											<option>Полтавська</option>
											<option>Рівненська</option>
											<option>Сумська</option>
											<option>Тернопільська</option>
											<option>Херсонська</option>
											<option>Хмельницька</option>
											<option>Черкаська</option>
											<option>Чернівецька</option>
											<option>Чернігівська</option>
											<option>Автономна республіка Крим</option>
										</select>
									</div>
								</div>
								<div class="user-block">
									<div class="user-block__title">Населений пункт</div>									
									<div class="user-block__content user-block__content_locality">
										<select  name="" class="user_info_company_workplace_select" data-placeholder="Оберіть населений пункт" >
											<option></option>									
										</select>
									</div>
								</div>
								<div class="user-block">
									<div class="user-block__title">Індекс</div>
									<div class="user-block__content user-block__content_index">
										<input type="text" id="user_index">
									</div>
								</div>
								<div class="user-block">
									<div class="user-block__title">Адреса</div>
									<div class="user-block__content">
										<input type="text" id="user_address">
									</div>
								</div>
							</div>
							<div class="user_info_wrapp">
								<div class="user-block user_info_alernate_email_blocks">
									<div class="user-block__title">Email</div>									
									<div class="user-block__content user_info_feedback_input">
										<div class="inputs_blocks">
											<div class="inputs_block_inner inputs_block_inner_alternate_email">
												<input type="email" class="col-md-12 col-sm-12 col-xs-12 email">
												<div class="remove_this_input"><img src='<?php print get_stylesheet_directory_uri(); ?>/images/thrash.svg' alt=''></div>
											</div>
										</div>
										<img class="pluss_icon" src="<?php print get_stylesheet_directory_uri(); ?>/images/add.svg" alt="">
										<span class="text_after_plus_icon">Додати e-mail</span>
									</div>
								</div>
								<div class="user-block user_info_alernate_phone_blocks">
									<div class="user-block__title">Телефон</div>									
									<div class="user-block__content user_info_feedback_input">
										<div class="inputs_blocks">
											<div class="inputs_block_inner inputs_block_inner_phone">
												<input type="phone" class="col-md-12 col-sm-12 col-xs-12 phone">
												<div class="remove_this_input"><img src='<?php print get_stylesheet_directory_uri(); ?>/images/thrash.svg' alt=''></div>
											</div>
										</div>
										<img class="pluss_icon" src="<?php print get_stylesheet_directory_uri(); ?>/images/add.svg" alt="">
										<span class="text_after_plus_icon">Додати телефон</span>
									</div>
								</div>
								<div class="user-block user_info_website_blocks">
									<div class="user-block__title">Сайт</div>									
									<div class="user-block__content user_info_feedback_input">
										<div class="inputs_blocks">
											<div class="inputs_block_inner inputs_block_inner_website">
												<input type="text" class="col-md-12 col-sm-12 col-xs-12 website">
												<div class="remove_this_input"><img src='<?php print get_stylesheet_directory_uri(); ?>/images/thrash.svg' alt=''></div>
											</div>
										</div>
										<img class="pluss_icon" src="<?php print get_stylesheet_directory_uri(); ?>/images/add.svg" alt="">
										<span class="text_after_plus_icon">Додати сайт</span>
									</div>
								</div>
							</div>
						</div>		
					</div>
					<div class="company_services_block col-md-12 col-sm-12 col-xs-12">	
						<div class="user_info_block_title">IV. Послуги</div>
						<div class="company_services_block_subtitle">Вкажіть спеціалізацію компанії, обравши<br/> відповідні класи КВЕД</div>
						<div class="services_self_block col-md-6 col-sm-6 col-xs-12">
							<div class="col-md-12">
								<img src="<?php print get_stylesheet_directory_uri(); ?>/images/close_stuff_serv_button.png" alt="">
								<div class="service_name">8211402 - Услуги банковские по долгострочным кредитам</div>
							</div>
						</div>
						<div class="services_self_block col-md-6 col-sm-6 col-xs-12">
							<div class="col-md-12">
								<img src="<?php print get_stylesheet_directory_uri(); ?>/images/close_stuff_serv_button.png" alt="">
								<div class="service_name">8211402 - Услуги банковские по долгострочным кредитам</div>
							</div>
						</div>
						<div class="services_self_block col-md-6 col-sm-6 col-xs-12">
							<div class="col-md-12">
								<img src="<?php print get_stylesheet_directory_uri(); ?>/images/close_stuff_serv_button.png" alt="">
								<div class="service_name">8211402 - Услуги банковские по долгострочным кредитам</div>
							</div>
						</div>
						<div class="clear_kry"></div>
						<img class="pluss_icon" src="<?php print get_stylesheet_directory_uri(); ?>/images/add.svg" alt=""><span class="add_info_company_services">Додати КВЕД</span>
					</div>
					<div class="clear_kry"></div>
					<div class="overflov_for_search"></div>
					<div class="company_stuffs_block col-md-12 col-sm-12 col-xs-12">	
						<div class="user_info_block_title">V. Товари</div>
						<div class="company_stuffs_block_subtitle">Вкажіть HS класи товарів, які виробляє компанія</div>
						<div class="stuffs_self_block col-md-6 col-sm-6 col-xs-12">
							<div class="col-md-12">
								<img src="<?php print get_stylesheet_directory_uri(); ?>/images/close_stuff_serv_button.png" alt="">
								<div class="stuff_name">4003 - Каучук у первинних формах або в пластинах, аркушах чи смужках</div>
							</div>
						</div>
						<div class="stuffs_self_block col-md-6 col-sm-6 col-xs-12">
							<div class="col-md-12">
								<img src="<?php print get_stylesheet_directory_uri(); ?>/images/close_stuff_serv_button.png" alt="">
								<div class="stuff_name">4015 - Вироби одягу та аксесуари для одягу (включаючи рукаыички та рукавиці) для всіх цілей з вулканізованої гуми, крім твердої гуми</div>
							</div>
						</div>
						<div class="stuffs_self_block col-md-6 col-sm-6 col-xs-12">
							<div class="col-md-12">
								<img src="<?php print get_stylesheet_directory_uri(); ?>/images/close_stuff_serv_button.png" alt="">
								<div class="stuff_name">4015 - Вироби одягу та аксесуари для одягу (включаючи рукаыички та рукавиці) для всіх цілей з вулканізованої гуми, крім твердої гуми</div>
							</div>
						</div>
						<div class="clear_kry"></div>
						<img class="pluss_icon" src="<?php print get_stylesheet_directory_uri(); ?>/images/add.svg" alt=""><span class="add_info_company_stuffs">Додати товари</span>
					</div>	
					<div class="info_save_icons col-md-12 col-sm-12 col-xs-12 nopadding">
						<i class="info_icon fa fa-info" aria-hidden="true"></i>
						<div class="save_event_canges"><i class="fa fa-floppy-o" aria-hidden="true"></i>Зберегти зміни</div>
					</div>


					<div class="single_event_reg_step_info col-md-12 col-sm-12 col-xs-12">
						<div class="user_info_block_title">VI. Пройдіть опитування</div>

			
						<div class="single_event_participant_info col-md-12 col-sm-12 col-xs-12">
							<div class="user_info_wrapp user_info_wrapp_with_border">
								<div class="user-block">
									<div class="user-block__title">Наявність експортного відділу</div>
									<div class="user-block__content">
										<div class="filter_item types">	
											<div class="filter_chosen_item">
												<span>
													<input type="checkbox" />		
												</span>
												<label>Так</label>							
											</div>
											<div class="filter_chosen_item">
												<span>
													<input type="checkbox" />		
												</span>
												<label>Ні</label>							
											</div>
										</div>
									</div>
								</div>
								<div class="user-block">
									<div class="user-block__title">Рівень володіння англійською мовою персоналом експортного відділу</div>
									<div class="user-block__content">
										<select name="" class="user_info_company_workplace_select" data-placeholder="Оберіть">
											<option></option>
										</select>
									</div>
								</div>
								<div class="user-block">
									<div class="user-block__title">Чи експортувала компанія свої товари/послуги у 2017 році?</div>
									<div class="user-block__content">
										<div class="filter_item types">	
											<div class="filter_chosen_item">
												<span>
													<input type="checkbox" />		
												</span>
												<label>Так</label>							
											</div>
											<div class="filter_chosen_item">
												<span>
													<input type="checkbox" />		
												</span>
												<label>Ні</label>							
											</div>
										</div>
									</div>
								</div>
								<div class="user-block">
									<div class="user-block__title">Які саме товари/послуги були експортовані</div>
									<div class="user-block__content">
										<input type="text">
									</div>
								</div>
								<div class="user-block">
									<div class="user-block__title">Основні експортні ринки</div>
									<div class="user-block__content">
										<select name="" class="langs_multiple" data-placeholder="" multiple="multiple">
											<option></option>
											<option value="Казахстан">Казахстан</option>
											<option value="Афганістан">Афганістан</option>
										</select>
									</div>
								</div>
								<div class="user-block">
									<div class="user-block__title">Членство в асоціаціях і об'єднаннях</div>
									<div class="user-block__content">
										<input type="text">
									</div>
								</div>
								<div class="user-block">
									<div class="user-block__title">Представництва за кордоном</div>
									<div class="user-block__content">
										<div class="filter_item types">	
											<div class="filter_chosen_item">
												<span>
													<input type="checkbox" />		
												</span>
												<label>Так</label>							
											</div>
											<div class="filter_chosen_item">
												<span>
													<input type="checkbox" />		
												</span>
												<label>Ні</label>							
											</div>
										</div>
									</div>
								</div>
								<div class="user-block">
									<div class="user-block__title">Де саме?</div>
									<div class="user-block__content">
										<select name="" class="langs_multiple" data-placeholder="" multiple="multiple">
											<option></option>
											<option value="Казахстан">Казахстан</option>
											<option value="Афганістан">Афганістан</option>
										</select>
									</div>
								</div>
								<div class="user-block">
									<div class="user-block__title">Регіони цікаві для експорту</div>
									<div class="user-block__content">
										<select name="" class="langs_multiple" data-placeholder="" multiple="multiple">
											<option></option>
											<option value="Казахстан">Казахстан</option>
											<option value="Афганістан">Афганістан</option>
										</select>
									</div>
								</div>
								<div class="user-block">
									<div class="user-block__title">Країни цікаві для експорту</div>
									<div class="user-block__content">
										<select name="" class="langs_multiple" data-placeholder="" multiple="multiple">
											<option></option>
											<option value="Казахстан">Казахстан</option>
											<option value="Афганістан">Афганістан</option>
										</select>
									</div>
								</div>
								<div class="user-block">
									<div class="user-block__title">Наявна сертифікація</div>
									<div class="user-block__content">
										<input type="text">
									</div>
								</div>
							</div>
							<div class="user_info_wrapp user_info_wrapp_with_border">
								<div class="user-block">
									<div class="user-block__title">Чи є у вашої компанії веб-сайт?</div>
									<div class="user-block__content">
										<div class="filter_item types">	
											<div class="filter_chosen_item">
												<span>
													<input type="checkbox" />		
												</span>
												<label>Так</label>							
											</div>
											<div class="filter_chosen_item">
												<span>
													<input type="checkbox" />		
												</span>
												<label>Ні</label>							
											</div>
										</div>
									</div>
								</div>
								<div class="user-block">
									<div class="user-block__title">Чи має він англомовну версію?</div>
									<div class="user-block__content">
										<div class="filter_item types">	
											<div class="filter_chosen_item">
												<span>
													<input type="checkbox" />		
												</span>
												<label>Так</label>							
											</div>
											<div class="filter_chosen_item">
												<span>
													<input type="checkbox" />		
												</span>
												<label>Ні</label>							
											</div>
										</div>
									</div>
								</div>
								<div class="user-block">
									<div class="user-block__title">Посилання на англомовну версію сайту</div>
									<div class="user-block__content">
										<input type="text">
									</div>
								</div>
								<div class="user-block">
									<div class="user-block__title">Чи представлена на Вашому сайті інформація про продукцію/послуги підприємства з деталізованим описом англійською мовою</div>
									<div class="user-block__content">
										<div class="filter_item types">	
											<div class="filter_chosen_item">
												<span>
													<input type="checkbox" />		
												</span>
												<label>Так</label>							
											</div>
											<div class="filter_chosen_item">
												<span>
													<input type="checkbox" />		
												</span>
												<label>Ні</label>							
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="user_info_wrapp user_info_wrapp_with_border">
								<div class="user-block">
									<div class="user-block__title">Яким чином розвивався Ваш бізнес у 2017 році порівняно з 2015 роком?</div>
									<div class="user-block__content">
										<select name="" class="user_info_company_workplace_select" data-placeholder="Оберіть">
											<option></option>
										</select>
									</div>
								</div>
								<div class="user-block">
									<div class="user-block__title">Чи здійснювались на Вашому сайті підприємстві суттєві інвестиції у будівництво, закупівлю нового обладнання протягом 2016-2017рр?</div>
									<div class="user-block__content">
										<div class="filter_item types">	
											<div class="filter_chosen_item">
												<span>
													<input type="checkbox" />		
												</span>
												<label>Так</label>							
											</div>
											<div class="filter_chosen_item">
												<span>
													<input type="checkbox" />		
												</span>
												<label>Ні</label>							
											</div>
										</div>
									</div>
								</div>
								<div class="user-block">
									<div class="user-block__title">Чи запускали Ви нові товари/послуги або робили суттєві зміни і покращення у існуючих товарах/послугах протягом 2016-2017рр?</div>
									<div class="user-block__content">
										<div class="filter_item types">	
											<div class="filter_chosen_item">
												<span>
													<input type="checkbox" />		
												</span>
												<label>Так</label>							
											</div>
											<div class="filter_chosen_item">
												<span>
													<input type="checkbox" />		
												</span>
												<label>Ні</label>							
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="user_info_wrapp user_info_wrapp_with_border user_info_wrapp_with_border_last">
								<div class="user-block">
									<div class="user-block__title">Додаткові коментарі та зауваження</div>
									<div class="user-block__content">
										<textarea name="" class="company_textarea" maxlength="200"></textarea>
									</div>
								</div>
							</div>
						</div>			
						<div class="clear_kry"></div>
					</div>
					<div class="clear_kry"></div>
					<a href="#" class="watch_test_results">Переглянути результати</a>
					<div class="clear_kry"></div>
					<div class="fill_block_white"></div>
				</div>
				<div class="user_company_info services_page_company_aside single_service_page_company_aside col-md-3  col-sm-12 col-xs-12">
					<div class="user_info_block_title">Мої компанії</div>
					<div class="overlay_companies"></div>
					<div class="clear_kry"></div>
					<div class="company_info_box">
						<div class="company_info_box_title">
							Вкажіть дані вашої компанії, щоб отримати доступ до послуг:
						</div>
						<div class="company_inputs_block">
							<label for="">Найменування юридичної особи</label>
							<input type="text" placeholder="напр. Ексім-меблі 3000">
							<label for="">Реєстраційний код (ЄРДПОУ або ІПН)</label>
							<input type="text">
							<label for="">Ваша посада</label>
							<input type="text">
							<label for="">Повна назва посади</label>
							<input type="text">
						</div>
						<input type="button" class="add_company_button" value="Додати компанію">
					</div>	
					<!--  Если пользователь залогиненый -->	
					<div class="logged_user">
						<div class="company_info_box active_company_info_box col-md-12 col-sm-12 col-xs-12">
							<span class="company_side_line"></span>
							<div class='remove_this_company'>
								<img src="<?php print get_stylesheet_directory_uri(); ?>/images/thrash.svg" alt="">
							</div>
							<img src="<?php print get_stylesheet_directory_uri(); ?>/images/credi_logo.png" alt="" class="user_logged_company_img">
							<div class="logged_company_names_wrapp">
								<span class="logged_company_name">Агріколь</span>
								<span class="logged_company_reg_num">12345678</span>									
							</div>
							<div class="clear_kry"></div>
							<a href="<?php echo home_url();?>/epo_profile/company/">Оновити інформацію</a>
							<div class="popup_delet_company">							
								<div class="change_pass_popup_head">
									<span>Ви впевнені що хочете прибрати цю компанію?</span>
									<img src="<?php print get_stylesheet_directory_uri(); ?>/images/close_button.svg" alt="">
								</div>	
								<span class="confirm_delete_company_aside col-md-5 col-sm-5 col-xs-5">Так</span><span class="dont_delete_company_aside col-md-5 col-sm-5 col-xs-5">Ні</span>
							</div>
						</div>	
						<div class="clear_kry"></div>
						<div class="company_info_box col-md-12 col-sm-12 col-xs-12">
							<span class="company_side_line"></span>
							<div class='remove_this_company'>
								<img src="<?php print get_stylesheet_directory_uri(); ?>/images/thrash.svg" alt="">
							</div>
							<img src="<?php print get_stylesheet_directory_uri(); ?>/images/nothumb.png" alt="" class="user_logged_company_img">
							<div class="logged_company_names_wrapp">
								<span class="logged_company_name">ФОП Носов Дмитро Андрійович</span>
								<span class="logged_company_reg_num">12345678</span>
							</div>	
							<div class="clear_kry"></div>
							<a href="">Оновити інформацію</a>	
							<div class="popup_delet_company">							
								<div class="change_pass_popup_head">
									<span>Ви впевнені що хочете прибрати цю компанію?</span>
									<img src="<?php print get_stylesheet_directory_uri(); ?>/images/close_button.svg" alt="">
								</div>	
								<span class="confirm_delete_company_aside col-md-5 col-sm-5 col-xs-5">Так</span><span class="dont_delete_company_aside col-md-5 col-sm-5 col-xs-5">Ні</span>
							</div>					
						</div>
						<div class="clear_kry"></div>
						<div class="logged_user_add_company_button">
							<img src="<?php print get_stylesheet_directory_uri(); ?>/images/add_company_button.png" alt="">Додати компанію
						</div>
					</div>							
				</div>
				<div class="fill_block"></div>
			</div>
		</form>
		<div class="fill_block_bottom"></div>
		<!-- Галерея услуг -->
		<div class="clear_kry"></div>
		<div class="carousel">
			<div class="overlay_companies"></div>
			<div class="services_carosel_title">Інші послуги</div>
			<div class="services_carousel owl-carousel">
				<a href="<?php echo home_url();?>/epo_profile/readliness_assessment_page/">
					<div class="corusel_item">
						<div class="service_carousel_item_title">Оцінка готовності</div>
						<div class="service_carousel_item_description">Оцініть свою готовність до зовнішньоекономічної діяльності. Визначте компетенції, яких вам бракує для досягення успіху.</div>
					</div>
				</a>
				<a href="<?php echo home_url();?>/epo_profile/checking_idea_page/">
					<div class="corusel_item">
						<div class="service_carousel_item_title">Перевірка ідеї</div>
						<div class="service_carousel_item_description">Отримайте допомогу у розробці плану інтернаціоналізації вашого бізнесу.</div>
					</div>
				</a>
				<a href="<?php echo home_url();?>/epo_profile/mentorstvo_page/">
					<div class="corusel_item">
						<div class="service_carousel_item_title">Менторство</div>
						<div class="service_carousel_item_description">Зв'язок з фахівцями, які зможуть надати професійну допомогу із пошуку партнерів та аналізу ринку.</div>
					</div>
				</a>
				<a href="<?php echo home_url();?>/epo_profile/internationalization_bussiness_page/">
					<div class="corusel_item">
						<div class="service_carousel_item_title">Інтернаціоналізація бізнесу</div>
						<div class="service_carousel_item_description">Зв'язок з фахівцями, які зможуть надати професійну допомогу із пошуку партнерів та аналізу ринку.</div>
					</div>
				</a>
				<a href="<?php echo home_url();?>/epo_profile/sourcing_service_page/">
					<div class="corusel_item">
						<div class="service_carousel_item_title">Soursing</div>
						<div class="service_carousel_item_description">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</div>
					</div>
				</a>
				<a href="<?php echo home_url();?>/epo_profile/een_page/">
					<div class="corusel_item">
						<div class="service_carousel_item_title">Enterprise Europe Network</div>
						<div class="service_carousel_item_description">EEN інформує про можливості розвитку в певній сфері, допомагає в залученні інвестицій для виробництва товарів і послуг, залученні нових технологій для підвищення конкурентноспроможності українських товаровиробників та підприємств, які надають послуги розвитку технологій, які були розроблені в Україні за допомогою закордонних партнерів.</div>
					</div>
				</a>			
			</div>
		</div>
		<div class="clear_kry"></div>
	</div>
	<div class="clear"></div>
</div>
	<style>
		.header{
			position: fixed;
		}

		.content-left-wrap{
			padding-top: 0;
		}

		

		/*@media only screen and (max-width: 767px){
			#footer {
				padding-top: 0;
				margin-top: 0px;
			}
		}*/
	</style>
<?php get_footer();  ?>