<?php 
if(!is_user_logged_in()){
	wp_redirect(get_home_url(), 302);
}
get_header();  
?>
<div class="clear"></div>
</header>
<div class="overflow_events_kry"></div>
<div id="content" class="site-content">
	<div class="container">
		<div class="content-left-wrap col-md-12">
			<div class="popup_for_search_KVED">
				<img class="close_search_popup" src="<?php print get_stylesheet_directory_uri(); ?>/images/close_button.svg" alt="">
				<div class="serch_title">Додати КВЕД</div>
				<div class="clear_kry"></div>
				<div class="main_search_wrapp">
					<div class="search_button_wrapp">
						<input type="submit" value=""><i class="fa fa-search" aria-hidden="true"></i>
					</div>
					<input name="s" type="search" placeholder="Введіть назву товару/послуги або КВЕД" class="search_input">
					
					<div class="search_results">	
						<ul class="list-kved">
							<li class="list-kved__item">
								<div class="list-kved__point"></div>
								<span class="list-kved__title">80 - Услуги бизенса</span>
								<ul>
									<li class="list-kved__item">
										<span class="list-kved__point"></span>
										<span class="list-kved__title">80 - Услуги бизенсу</span>
									</li>
									<li class="list-kved__item">
										<span class="list-kved__point"></span>
										<span class="list-kved__title">Финансовые и страховые услуги</span>
										<ul>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Бумага, печатные и издательские услуги</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Досуг развлечения  и туризм</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Информационные технологии, компьютеры, программное обеспечение, Интернет, исследовани и разработки</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Металлы, станки, машины, металлообработка</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Минералы, руды, полезные ископаемые</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Образование, обучение, общественные институты</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Розничаная торговля и дистрибуци</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Сельское хозяйство, продукты питания</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Строительство</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Текстиль, одежда, кожа, часы, ювелизрные изделия</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Транспорт и логистика</span>
											</li>
										</ul>
									</li>		
								</ul>
							</li>
						</ul>
					</div>
					<div class="clear_kry"></div>
					<div class="save_button_wrapp">	
						<input type="submit" value="Зберегти зміни">
					</div>
				</div>
			</div>
			<div class="popup_for_search_HS">
				<img class="close_search_popup" src="<?php print get_stylesheet_directory_uri(); ?>/images/close_button.svg" alt="">
				<div class="serch_title">Додати товари</div>
				<div class="clear_kry"></div>
				<div class="main_search_wrapp">
					<div class="search_button_wrapp">
						<input type="submit" value=""><i class="fa fa-search" aria-hidden="true"></i>
					</div>
					<input name="s" type="search" placeholder="Введіть назву товару/послуги або HS" class="search_input">		
					<div class="search_results">	
						<ul class="list-kved">
							<li class="list-kved__item">
								<div class="list-kved__point"></div>
								<span class="list-kved__title">80 - Услуги бизенса</span>
								<ul>
									<li class="list-kved__item">
										<span class="list-kved__point"></span>
										<span class="list-kved__title">80 - Услуги бизенсу</span>
									</li>
									<li class="list-kved__item">
										<span class="list-kved__point"></span>
										<span class="list-kved__title">Финансовые и страховые услуги</span>
										<ul>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Бумага, печатные и издательские услуги</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Досуг развлечения  и туризм</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Информационные технологии, компьютеры, программное обеспечение, Интернет, исследовани и разработки</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Металлы, станки, машины, металлообработка</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Минералы, руды, полезные ископаемые</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Образование, обучение, общественные институты</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Розничаная торговля и дистрибуци</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Сельское хозяйство, продукты питания</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Строительство</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Текстиль, одежда, кожа, часы, ювелизрные изделия</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point"></span>
												<span class="list-kved__title">Транспорт и логистика</span>
											</li>
										</ul>
									</li>		
								</ul>
							</li>
						</ul>
					</div>
					<div class="clear_kry"></div>
					<div class="save_button_wrapp">	
						<input type="submit" value="Зберегти зміни">
					</div>
				</div>								
			</div>
			<div class="overflow_kry"></div>	
			<div class="popup_for_add_participant">
				<div class="popup_for_add_participant_title">Додати учасників <img src="<?php print get_stylesheet_directory_uri(); ?>/images/close_button.svg" alt=""></div>
				<div class="popup_participant_item_block">
					<div class="popup_add_participant_photo_block" style='background-image: url("<?php print get_stylesheet_directory_uri(); ?>/images/user_photo.jpg ?>");background-size:cover; background-repeat:no-repeat; background-position: left center'></div>				
					<div class="popup_add_participant_name_block">Дмитро Носов</div>
					<div class="popup_add_participant_position_block">Фінансовий директор</div>
					<div class="popup_add_participant_check_block">
						<span></span>
					</div>
				</div>
				<div class="popup_participant_item_block">
					<div class="popup_add_participant_photo_block" style='background-image: url("<?php print get_stylesheet_directory_uri(); ?>/images/default_user_image.svg ?>");background-size:cover; background-repeat:no-repeat; background-position: left center'></div>
					<div class="popup_add_participant_name_block">Валентина Іванівна</div>
					<div class="popup_add_participant_position_block">Експортний менеджер</div>
					<div class="popup_add_participant_check_block">
						<span></span>
					</div>
				</div>
				<div class="popup_participant_item_block">
					<div class="popup_add_participant_photo_block" style='background-image: url("<?php print get_stylesheet_directory_uri(); ?>/images/default_user_image.svg ?>");background-size:cover; background-repeat:no-repeat; background-position: left center'></div>
					<div class="popup_add_participant_name_block">Катя</div>
					<div class="popup_add_participant_position_block">Асистент начальника експортного відділу</div>
					<div class="popup_add_participant_check_block">
						<span></span>
					</div>
				</div>
				<div class="popup_participant_item_block">
					<div class="popup_add_participant_photo_block" style='background-image: url("<?php print get_stylesheet_directory_uri(); ?>/images/default_user_image.svg ?>");background-size:cover; background-repeat:no-repeat; background-position: left center'></div>
					<div class="popup_add_participant_name_block">Катерина Петрівна</div>
					<div class="popup_add_participant_position_block">Асистент начальника експортного відділу</div>
					<div class="popup_add_participant_check_block add_participant_popup_disable_chack">
						<span></span>
					</div>
				</div>
				<div class="warning_massege_block">
					<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>Щоб запросити інших учасників їх потрібно спочатку додати до списку колег у вашій <a href="<?php echo home_url();?>/epo_profile/company/">компанії</a>.
				</div>
				<input type="button" value="Завершити редагування">
			</div>
			<div class="user_events_blocks col-md-9 col-sm-12 col-xs-12">
				<div class="event_block">
					<a href="<?php echo home_url();?>/epo_profile/dashboard/">
						<div class="event_block_title">
							<p>Особиста інформація</p><img src="<?php print get_stylesheet_directory_uri(); ?>/images/arr_right.svg" alt="">
						</div>
						<div class="clear_kry"></div>
						<div class="event_block_desc">
							Редагуйте інформацію про себе та свою компанію
						</div>
					</a>
				</div>
				<div class="event_block">
					<a href="<?php echo home_url();?>/epo_profile/services_list/">
						<div class="event_block_title">
							<p>Послуги</p><img src="<?php print get_stylesheet_directory_uri(); ?>/images/arr_right.svg" alt="">
						</div>
						<div class="clear_kry"></div>
						<div class="event_block_desc">
							Отримайте оцінку свого бізнесу та коментарі від експертів
						</div>
					</a>
				</div>
				<div class="event_block active_event_block">
					<a href="#">
						<div class="event_block_title">
							<p>Події</p><img src="<?php print get_stylesheet_directory_uri(); ?>/images/arr_right.svg" alt="">
						</div>
						<div class="clear_kry"></div>
						<div class="event_block_desc">
							Найактуальніші події та нетворкінг для експортерів
						</div>
					</a>
				</div>
				<hr>
			</div>			
			<div class="clear_kry"></div>
			<div class="single_event_main_wrapp col-md-12">				
				<div class="clear_kry"></div>
				<a href="<?php echo home_url();?>/epo_profile/events_for_user/" class="back_to_events_list col-md-12 col-sm-12">
					<i class="fa fa-long-arrow-left" aria-hidden="true"></i><span>Назад до списку подій</span>
				</a>
				<div class="single_event_info">
					<div class="event_tag col-md-12 col-sm-12 col-xs-12"><span>Виставки</span><span>Торгові місії</span></div>
					<div class="event_title col-md-12 col-sm-12 col-xs-12">Виставка Fruit Logistica 2018</div>
					<div class="single_event_content col-md-12 col-sm-12 col-xs-12">						
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias voluptate, soluta deserunt illum eaque quaerat? Aliquid assumenda sit, placeat, ea dicta quae ipsam quisquam quia, nisi et tenetur numquam modi! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias voluptate, soluta deserunt illum eaque quaerat? Aliquid assumenda sit, placeat, ea dicta quae ipsam quisquam quia, nisi et tenetur numquam modi!</p>

						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias voluptate, soluta deserunt illum eaque quaerat? Aliquid assumenda sit, laceat, ea dicta quae ipsam quisquam quia, nisi et tenetur numquam modi!</p>

						<p>Lorem ipsum dolor</p>

						<ul>
							<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit</li>
							<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit</li>
							<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit</li>
							<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit</li>
						</ul>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias voluptate, soluta deserunt illum eaque quaerat? Aliquid assumenda sit, laceat, ea dicta quae ipsam quisquam quia, nisi et tenetur numquam modi!</p>
					</div>
				</div>
				<div class="single_event_date_time_wrapp">
					<div class="single_event_date_time col-md-12 col-sm-12">
						<div class="single_event_begin col-md-6 col-sm-6 col-xs-6">Початок</div><div class="single_event_end col-md-6 col-sm-6 col-xs-6">Кінець</div>
						<div class="time_block_part time_block_part_one col-md-6 col-sm-6 col-xs-6"><span>07</span>лютого 2018</div><div class="time_block_part col-md-6 col-sm-6 col-xs-6"><span>09</span>лютого 2018</div>
						<div class="timetable_block col-md-12 col-sm-12 col-xs-12">
							Розклад
							<span>11:00 - 19:00</span>
						</div>
						<div class="single_reg_status_block event_reg_open col-md-12 col-sm-12 col-xs-12">	
							Реєстрація відкрита
						</div>
					</div>
				</div>
				<div class="clear_kry"></div>

				<form action="" class="company_dash_block readiness_assessment_page">
				<div class="single_event_reg_steps_wrapp col-md-9 col-sm-12 col-xs-12">
					<div class="single_event_reg_step col-md-12 col-sm-12 col-xs-12">
						<div class="single_event_step_title">I. Оберіть учасників місії</div>
						<div class="single_event_participant event_participant col-md-6 col-sm-6 col-xs-12">
							<div class="participant_photo_block" style='background-image: url("<?php print get_stylesheet_directory_uri(); ?>/images/user_photo.jpg ?>");background-size:cover; background-repeat:no-repeat; background-position: left center'></div>
							<div class="participant_name_block">Дмитро носов</div>
							<div class="participant_position_block">Фінансовий директор</div>
						</div>
						<div class="single_event_participant event_participant col-md-6 col-sm-6 col-xs-12">
							<div class="participant_photo_block" style='background-image: url("<?php print get_stylesheet_directory_uri(); ?>/images/default_user_image.svg ?>");background-size:cover; background-repeat:no-repeat; background-position: left center'></div>
							<div class="participant_name_block">Катя</div>
							<div class="participant_position_block">Асистент начальника експортного відділу</div>
						</div>
						<div class="single_event_participant event_participant col-md-6 col-sm-6 col-xs-12">
							<div class="participant_photo_block" style='background-image: url("<?php print get_stylesheet_directory_uri(); ?>/images/default_user_image.svg ?>");background-size:cover; background-repeat:no-repeat; background-position: left center'></div>
							<div class="participant_name_block">Валентина Іванівна</div>
							<div class="participant_position_block">Експортний менеджер</div>
						</div>
						<div class="single_event_participant event_participant empty_participant col-md-6 col-sm-6 col-xs-12">
							<img src="<?php print get_stylesheet_directory_uri(); ?>/images/add_empty.svg" alt="">
							Додати учасників
						</div>
					</div>
					<div class="single_event_reg_step col-md-12 col-sm-12 col-xs-12">
						<div class="single_event_step_title">II. Особа відповідальна за підготовку місії</div>
						<div class="single_event_participant open_selection_user_bloks_wrpp col-md-7 col-sm-8 col-xs-12">
							<div class="participant_photo_block" style='background-image: url("<?php print get_stylesheet_directory_uri(); ?>/images/user_photo.jpg ?>");background-size:cover; background-repeat:no-repeat; background-position: left center'></div>
							<div class="participant_name_block">Дмитро Носов</div>
							<div class="participant_position_block">Фінансовий директор</div>
							<div class="chevron_wrapp">
								<i class="fa fa-chevron-down" aria-hidden="true"></i>
							</div>
							<div class="selection_user_bloks_wrapp">
								<div class="user_block_in_selection">
									<div class="single_event_participant col-md-12 col-sm-12 col-xs-12">
										<div class="participant_photo_block" style='background-image: url("<?php print get_stylesheet_directory_uri(); ?>/images/default_user_image.svg ?>");background-size:cover; background-repeat:no-repeat; background-position: left center'></div>
										<div class="participant_name_block">Валентина Іванівна</div>
										<div class="participant_position_block">Експортний менеджер</div>
									</div>
								</div>
								<div class="user_block_in_selection">
									<div class="single_event_participant col-md-12 col-sm-12 col-xs-12">
										<div class="participant_photo_block" style='background-image: url("<?php print get_stylesheet_directory_uri(); ?>/images/default_user_image.svg ?>");background-size:cover; background-repeat:no-repeat; background-position: left center'></div>
										<div class="participant_name_block">Валентина Іванівна</div>
										<div class="participant_position_block">Експортний менеджер</div>
									</div>
								</div>
								<div class="user_block_in_selection">
									<div class="single_event_participant col-md-12 col-sm-12 col-xs-12">
										<div class="participant_photo_block" style='background-image: url("<?php print get_stylesheet_directory_uri(); ?>/images/default_user_image.svg ?>");background-size:cover; background-repeat:no-repeat; background-position: left center'></div>
										<div class="participant_name_block">Валентина Іванівна</div>
										<div class="participant_position_block">Експортний менеджер</div>
									</div>
								</div>
							</div>							
						</div>
					</div>
					<div class="single_event_reg_step_info col-md-12 col-sm-12 col-xs-12">					
			
						<div class="single_event_participant_info col-md-12 col-sm-12 col-xs-12">
							<div class="user_info_wrapp">
								<div class="user-block">
									<div class="user-block__title">П.І.Б</div>
									<div class="user-block__content">
										<input type="text" id="user_name">
									</div>
								</div>
								<div class="user-block">
									<div class="user-block__title">Посада</div>									
									<div class="user-block__content">
									<!--<input type='text' class='col-md-12 col-sm-12 col-xs-12'>-->
										<select name="" class="user_info_company_workplace_select" data-placeholder="Оберіть посаду">
											<option disabled selected></option>
										<option value="Фiзисна особа пiдприємець">Член правління</option>
											<option value="Фінасновий директор">Фінасновий директор</option>
										<option value="Експортний менеджер">Експортний менеджер</option>
										</select>
									</div>
								</div>
								<div class="user-block">
									<div class="user-block__title">Повна назва посади</div>
									<div class="user-block__content">
										<input type="text">
									</div>
								</div>
								<div class="user-block user_info_email_blocks">
									<div class="user-block__title">Email <i class="info_icon fa fa-info" aria-hidden="true"></i></div>									
									<div class="user-block__content user_info_feedback_input">
										<div class="inputs_blocks">
											<div class="inputs_block_inner inputs_block_inner_email">
												<input type="email" class="col-md-12 col-sm-12 col-xs-12 email">
												<div class="remove_this_input"><img src='<?php print get_stylesheet_directory_uri(); ?>/images/thrash.svg' alt=''></div>
												<span class="setp_main_email active_feedback_info">Основний email</span>
											</div>
										</div>
										<img class="pluss_icon" src="<?php print get_stylesheet_directory_uri(); ?>/images/add.svg" alt="">
										<span class="text_after_plus_icon">Додати e-mail</span>
									</div>
								</div>
								<div class="user-block user_info_phone_blocks">
									<div class="user-block__title">Мобільний телефон</div>									
									<div class="user-block__content user_info_feedback_input">
										<div class="inputs_blocks">
											<div class="inputs_block_inner inputs_block_inner_phone">
												<input type="phone" class="col-md-12 col-sm-12 col-xs-12 phone">
												<div class="remove_this_input"><img src='<?php print get_stylesheet_directory_uri(); ?>/images/thrash.svg' alt=''></div>
												<span class="setp_main_phone active_feedback_info">Основний номер</span>
											</div>
										</div>
										<img class="pluss_icon" src="<?php print get_stylesheet_directory_uri(); ?>/images/add.svg" alt="">
										<span class="text_after_plus_icon">Додати телефон</span>
									</div>
								</div>
								<div class="user-block user_info_work_phone_blocks">
									<div class="user-block__title">Робочий телефон</div>									
									<div class="user-block__content user_info_feedback_input">
										<div class="inputs_blocks"></div>
										<img class="pluss_icon" src="<?php print get_stylesheet_directory_uri(); ?>/images/add.svg" alt="">
										<span class="text_after_plus_icon">Додати телефон</span>
									</div>
								</div>
								<div class="info_save_icons col-md-12 col-sm-12 col-xs-12 nopadding">
									<i class="info_icon fa fa-info" aria-hidden="true"></i>
									<div class="save_event_canges"><i class="fa fa-floppy-o" aria-hidden="true"></i>Зберегти зміни</div>
								</div>
								<div class="clear_kry"></div>
							</div>
						</div>
					</div>
					<div class="single_event_reg_step col-md-12 col-sm-12 col-xs-12">
						<div class="single_event_step_title">III. Особа відповідальна за підготовку маркетингових матерыалів та PR</div>
						<div class="single_event_participant open_selection_user_bloks_wrpp col-md-6 col-sm-8 col-xs-12">
							<div class="participant_photo_block" style='background-image: url("<?php print get_stylesheet_directory_uri(); ?>/images/user_photo.jpg ?>");background-size:cover; background-repeat:no-repeat; background-position: left center'></div>
							<div class="participant_name_block">Дмитро Носов</div>
							<div class="participant_position_block">Фінансовий директор</div>
							<div class="chevron_wrapp">
								<i class="fa fa-chevron-down" aria-hidden="true"></i>
							</div>
							<div class="selection_user_bloks_wrapp">
								<div class="user_block_in_selection">
									<div class="single_event_participant col-md-12 col-sm-12 col-xs-12">
										<div class="participant_photo_block" style='background-image: url("<?php print get_stylesheet_directory_uri(); ?>/images/default_user_image.svg ?>");background-size:cover; background-repeat:no-repeat; background-position: left center'></div>
										<div class="participant_name_block">Валентина Іванівна</div>
										<div class="participant_position_block">Експортний менеджер</div>
									</div>
								</div>
								<div class="user_block_in_selection">
									<div class="single_event_participant col-md-12 col-sm-12 col-xs-12">
										<div class="participant_photo_block" style='background-image: url("<?php print get_stylesheet_directory_uri(); ?>/images/default_user_image.svg ?>");background-size:cover; background-repeat:no-repeat; background-position: left center'></div>
										<div class="participant_name_block">Валентина Іванівна</div>
										<div class="participant_position_block">Експортний менеджер</div>
									</div>
								</div>
								<div class="user_block_in_selection">
									<div class="single_event_participant col-md-12 col-sm-12 col-xs-12">
										<div class="participant_photo_block" style='background-image: url("<?php print get_stylesheet_directory_uri(); ?>/images/default_user_image.svg ?>");background-size:cover; background-repeat:no-repeat; background-position: left center'></div>
										<div class="participant_name_block">Валентина Іванівна</div>
										<div class="participant_position_block">Експортний менеджер</div>
									</div>
								</div>
							</div>							
						</div>
					</div>
					<div class="single_event_reg_step_info col-md-12 col-sm-12 col-xs-12">								
			
						<div class="single_event_participant_info col-md-12 col-sm-12 col-xs-12">
							<div class="user_info_wrapp">
								<div class="user-block">
									<div class="user-block__title">П.І.Б</div>
									<div class="user-block__content">
										<input type="text" id="user_name">
									</div>
								</div>
								<div class="user-block">
									<div class="user-block__title">Посада</div>									
									<div class="user-block__content">
									<!--<input type='text' class='col-md-12 col-sm-12 col-xs-12'>-->
										<select name="" class="user_info_company_workplace_select" data-placeholder="Оберіть посаду">
											<option disabled selected></option>
										<option value="Фiзисна особа пiдприємець">Член правління</option>
											<option value="Фінасновий директор">Фінасновий директор</option>
										<option value="Експортний менеджер">Експортний менеджер</option>
										</select>
									</div>
								</div>
								<div class="user-block">
									<div class="user-block__title">Повна назва посади</div>
									<div class="user-block__content">
										<input type="text">
									</div>
								</div>
								<div class="user-block user_info_email_blocks">
									<div class="user-block__title">Email <i class="info_icon fa fa-info" aria-hidden="true"></i></div>									
									<div class="user-block__content user_info_feedback_input">
										<div class="inputs_blocks">
											<div class="inputs_block_inner inputs_block_inner_email">
												<input type="email" class="col-md-12 col-sm-12 col-xs-12 email">
												<div class="remove_this_input"><img src='<?php print get_stylesheet_directory_uri(); ?>/images/thrash.svg' alt=''></div>
												<span class="setp_main_email active_feedback_info">Основний email</span>
											</div>
										</div>
										<img class="pluss_icon" src="<?php print get_stylesheet_directory_uri(); ?>/images/add.svg" alt="">
										<span class="text_after_plus_icon">Додати e-mail</span>
									</div>
								</div>
								<div class="user-block user_info_phone_blocks">
									<div class="user-block__title">Мобільний телефон</div>									
									<div class="user-block__content user_info_feedback_input">
										<div class="inputs_blocks">
											<div class="inputs_block_inner inputs_block_inner_phone">
												<input type="phone" class="col-md-12 col-sm-12 col-xs-12 phone">
												<div class="remove_this_input"><img src='<?php print get_stylesheet_directory_uri(); ?>/images/thrash.svg' alt=''></div>
												<span class="setp_main_phone active_feedback_info">Основний номер</span>
											</div>
										</div>
										<img class="pluss_icon" src="<?php print get_stylesheet_directory_uri(); ?>/images/add.svg" alt="">
										<span class="text_after_plus_icon">Додати телефон</span>
									</div>
								</div>
								<div class="user-block user_info_work_phone_blocks">
									<div class="user-block__title">Робочий телефон</div>									
									<div class="user-block__content user_info_feedback_input">
										<div class="inputs_blocks"></div>
										<img class="pluss_icon" src="<?php print get_stylesheet_directory_uri(); ?>/images/add.svg" alt="">
										<span class="text_after_plus_icon">Додати телефон</span>
									</div>
								</div>
								<div class="info_save_icons col-md-12 col-sm-12 col-xs-12 nopadding">
									<i class="info_icon fa fa-info" aria-hidden="true"></i>
									<div class="save_event_canges"><i class="fa fa-floppy-o" aria-hidden="true"></i>Зберегти зміни</div>
								</div>
								<div class="clear_kry"></div>
							</div>
						</div>
					</div>
					<div class="single_event_reg_step single_event_reg_step_company col-md-12 col-sm-12 col-xs-12">
						<div class="single_event_step_title">IV. Компанія</div>						
					</div>
					<div class="single_event_reg_step_info single_event_reg_step_info_company col-md-12 col-sm-12 col-xs-12">
						<div class="single_event_participant_info col-md-12 col-sm-12 col-xs-12">
							<div class="user_info_wrapp user_info_wrapp_with_border">
								<div class="user-block">
									<div class="user-block__title">Назва офіційна</div>
									<div class="user-block__content">
										<input type="text" id="company_name">
									</div>
								</div>	
								<div class="user-block">
									<div class="user-block__title">Назва альтернативна</div>
									<div class="user-block__content">
										<input type="text" id="alternate_company_name">
									</div>
								</div>	
								<div class="user-block">
									<div class="user-block__title">Реєстраційний код (ЕРДПУ або ІПН)</div>
									<div class="user-block__content user-block__content_code">
										<input type="text" id="code" class="col-md-3 col-sm-3 col-xs-12">
									</div>
								</div>	
								<div class="user-block">
									<div class="user-block__title">Форма власності</div>									
									<div class="user-block__content user-block__content_forma">
										<select name="" class="user_info_company_workplace_select" data-placeholder="Оберіть форму власності">
											<option></option>
											<option value="Закрите акціонерне товариство">Закрите акціонерне товариство</option>
											<option value="Фізична особа підприємець">Фізична особа підприємець</option>
											<option value="Приватне підприємство">Приватне підприємство</option>
											<option value="Відкрите акціонерне товариство">Відкрите акціонерне товариство</option>
											<option value="Державне підприємство">Державне підприємство</option>
										</select>
									</div>
								</div>
								<div class="user-block">
									<div class="user-block__title">Рік заснування</div>									
									<div class="user-block__content user-block__content_year">
										<select name="" class="user_info_company_workplace_select" data-placeholder="Оберіть рік заснування">
											<option></option>
											<?php
											for ($i = 2019; $i > 1900; $i--)
											{
												if ($i == 2019)
												{
													echo '<option value="1993" disabled selected style="color:#bababa;">1993</oplion>';
												}
												else
												{
													echo '<option value="'.$i.'">'.$i.'</oplion>';
												}
											}
											?>
										</select>
									</div>
								</div>
							</div>
							<div class="user_info_wrapp user_info_wrapp_with_border">
								<div class="user-block">
									<div class="user-block__title">Область</div>									
									<div class="user-block__content user-block__content_region">
										<select name="" class="user_info_company_workplace_select" data-placeholder="Оберіть">
											<option></option>
											<option>Київська</option>
											<option>Харківська</option>
											<option>Вінницька</option>
											<option>Волинська</option>
											<option>Дніпропетровська</option>
											<option>Донецька</option>
											<option>Житомирська</option>
											<option>Закарпатська</option>
											<option>Запорізька</option>
											<option>Івано-Франківська</option>
											<option>Кіровоградська</option>
											<option>Луганська</option>
											<option>Львівська</option>
											<option>Миколаївська</option>
											<option>Одеська</option>
											<option>Полтавська</option>
											<option>Рівненська</option>
											<option>Сумська</option>
											<option>Тернопільська</option>
											<option>Херсонська</option>
											<option>Хмельницька</option>
											<option>Черкаська</option>
											<option>Чернівецька</option>
											<option>Чернігівська</option>
											<option>Автономна республіка Крим</option>
										</select>
									</div>
								</div>
								<div class="user-block">
									<div class="user-block__title">Населений пункт</div>									
									<div class="user-block__content user-block__content_locality">
										<select  name="" class="user_info_company_workplace_select" data-placeholder="Оберіть населений пункт" >
											<option></option>									
										</select>
									</div>
								</div>
								<div class="user-block">
									<div class="user-block__title">Індекс</div>
									<div class="user-block__content user-block__content_index">
										<input type="text" id="user_index">
									</div>
								</div>
								<div class="user-block">
									<div class="user-block__title">Адреса</div>
									<div class="user-block__content">
										<input type="text" id="user_address">
									</div>
								</div>
							</div>
							<div class="user_info_wrapp user_info_wrapp_with_border user_info_wrapp_no_padding">
								<div class="user-block user_info_alernate_email_blocks">
									<div class="user-block__title">Email</div>									
									<div class="user-block__content user_info_feedback_input">
										<div class="inputs_blocks">
											<div class="inputs_block_inner inputs_block_inner_alternate_email">
												<input type="email" class="col-md-12 col-sm-12 col-xs-12 email">
												<div class="remove_this_input"><img src='<?php print get_stylesheet_directory_uri(); ?>/images/thrash.svg' alt=''></div>
											</div>
										</div>
										<img class="pluss_icon" src="<?php print get_stylesheet_directory_uri(); ?>/images/add.svg" alt="">
										<span class="text_after_plus_icon">Додати e-mail</span>
									</div>
								</div>
								<div class="user-block user_info_alernate_phone_blocks">
									<div class="user-block__title">Телефон</div>									
									<div class="user-block__content user_info_feedback_input">
										<div class="inputs_blocks">
											<div class="inputs_block_inner inputs_block_inner_phone">
												<input type="phone" class="col-md-12 col-sm-12 col-xs-12 phone">
												<div class="remove_this_input"><img src='<?php print get_stylesheet_directory_uri(); ?>/images/thrash.svg' alt=''></div>
											</div>
										</div>
										<img class="pluss_icon" src="<?php print get_stylesheet_directory_uri(); ?>/images/add.svg" alt="">
										<span class="text_after_plus_icon">Додати телефон</span>
									</div>
								</div>
								<div class="user-block user_info_website_blocks">
									<div class="user-block__title">Сайт</div>									
									<div class="user-block__content user_info_feedback_input">
										<div class="inputs_blocks">
											<div class="inputs_block_inner inputs_block_inner_website">
												<input type="text" class="col-md-12 col-sm-12 col-xs-12 website">
												<div class="remove_this_input"><img src='<?php print get_stylesheet_directory_uri(); ?>/images/thrash.svg' alt=''></div>
											</div>
										</div>
										<img class="pluss_icon" src="<?php print get_stylesheet_directory_uri(); ?>/images/add.svg" alt="">
										<span class="text_after_plus_icon">Додати сайт</span>
									</div>
								</div>
							</div>
							<div class="user_info_wrapp een_page_details_list">
								<div class="company_textarea_blocks col-md-12 col-sm-12 col-xs-12">
									<span class="col-md-5 col-sm-5 col-xs-12">General information about the Company</span>
									<textarea name="" class="company_textarea4 col-md-7 col-sm-7 col-xs-12" cols="30" rows="10"></textarea>
									<div class="char_counter_wrapp"><span><span class="char_count">0</span>/500</span></div>
								</div>			
								<div class="company_textarea_blocks col-md-12 col-sm-12 col-xs-12">
									<span class="col-md-5 col-sm-5 col-xs-12">Детальний опис покупців</span>
									<textarea name="" class="company_textarea4 col-md-7 col-sm-7 col-xs-12" cols="30" rows="10"></textarea>
									<div class="char_counter_wrapp"><span><span class="char_count">0</span>/4000</span></div>
								</div>			
								<div class="company_textarea_blocks col-md-12 col-sm-12 col-xs-12">
									<span class="col-md-5 col-sm-5 col-xs-12">Членство в ассоціаціях і об'єднаннях</span>
									<textarea name="" class="company_textarea4 col-md-7 col-sm-7 col-xs-12" cols="30" rows="10"></textarea>
									<div class="char_counter_wrapp"><span><span class="char_count">0</span>/2000</span></div>
								</div>	
							</div>
						</div>	
					</div>
					<div class="company_services_block col-md-12 col-sm-12 col-xs-12">	
						<div class="user_info_block_title">V. Послуги</div>
						<div class="company_services_block_subtitle">Вкажіть спеціалізацію компанії, обравши<br/> відповідні класи КВЕД</div>
						<div class="services_self_block col-md-6 col-sm-6 col-xs-12">
							<div class="col-md-12">
								<img src="<?php print get_stylesheet_directory_uri(); ?>/images/close_stuff_serv_button.png" alt="">
								<div class="service_name">8211402 - Услуги банковские по долгострочным кредитам</div>
							</div>
						</div>
						<div class="services_self_block col-md-6 col-sm-6 col-xs-12">
							<div class="col-md-12">
								<img src="<?php print get_stylesheet_directory_uri(); ?>/images/close_stuff_serv_button.png" alt="">
								<div class="service_name">8211402 - Услуги банковские по долгострочным кредитам</div>
							</div>
						</div>
						<div class="services_self_block col-md-6 col-sm-6 col-xs-12">
							<div class="col-md-12">
								<img src="<?php print get_stylesheet_directory_uri(); ?>/images/close_stuff_serv_button.png" alt="">
								<div class="service_name">8211402 - Услуги банковские по долгострочным кредитам</div>
							</div>
						</div>
						<div class="services_self_block col-md-6 col-sm-6 col-xs-12">
							<div class="col-md-12">
								<img src="<?php print get_stylesheet_directory_uri(); ?>/images/close_stuff_serv_button.png" alt="">
								<div class="service_name">8211402 - Услуги банковские по долгострочным кредитам</div>
							</div>
						</div>
						<div class="services_self_block col-md-6 col-sm-6 col-xs-12">
							<div class="col-md-12">
								<img src="<?php print get_stylesheet_directory_uri(); ?>/images/close_stuff_serv_button.png" alt="">
								<div class="service_name">8211402 - Услуги банковские по долгострочным кредитам</div>
							</div>
						</div>
						<div class="clear_kry"></div>
						<img class="pluss_icon" src="<?php print get_stylesheet_directory_uri(); ?>/images/add.svg" alt=""><span class="add_info_company_services">Додати КВЕД</span>
					</div>
					<div class="clear_kry"></div>
					<div class="overflov_for_search"></div>
					<div class="company_stuffs_block col-md-12 col-sm-12 col-xs-12">	
						<div class="user_info_block_title">VI. Товари</div>
						<div class="company_stuffs_block_subtitle">Вкажить HS класи товарів, які виробляє компанія</div>
						<div class="stuffs_self_block col-md-6 col-sm-6 col-xs-12">
							<div class="col-md-12">
								<img src="<?php print get_stylesheet_directory_uri(); ?>/images/close_stuff_serv_button.png" alt="">
								<div class="stuff_name">4003 - Каучук у первинних формах або в пластинах, аркушах чи смужках</div>
							</div>
						</div>
						<div class="stuffs_self_block col-md-6 col-sm-6 col-xs-12">
							<div class="col-md-12">
								<img src="<?php print get_stylesheet_directory_uri(); ?>/images/close_stuff_serv_button.png" alt="">
								<div class="stuff_name">4015 - Вироби одягу та аксесуари для одягу (включаючи рукаыички та рукавиці) для всіх цілей з вулканізованої гуми, крім твердої гуми</div>
							</div>
						</div>
						<div class="clear_kry"></div>
						<img class="pluss_icon" src="<?php print get_stylesheet_directory_uri(); ?>/images/add.svg" alt=""><span class="add_info_company_stuffs">Додати товари</span>
					</div>	
					<div class="info_save_icons col-md-12 col-sm-12 col-xs-12">
						<i class="info_icon fa fa-info" aria-hidden="true"></i>
						<div class="save_event_canges"><i class="fa fa-floppy-o" aria-hidden="true"></i>Зберегти зміни</div>
					</div>
					<div class="clear_kry"></div>
					<div class="single_event_reg_step een_page_details_list single_event_reg_step_questions col-md-12 col-sm-12 col-xs-12">
						<div class="single_event_step_title">VII. Питання стосовно події</div>	
						<div class="single_event_participant_info user_info_wrapp een_page_details_list">
							<div class="company_textarea_blocks col-md-12 col-sm-12 col-xs-12">
								<span class="col-md-5 col-sm-5 col-xs-12">З ким би ви хотіли зустрітися</span>
								<textarea name="" class="company_textarea4 col-md-7 col-sm-7 col-xs-12" cols="30" rows="10"></textarea>
								<div class="char_counter_wrapp"><span><span class="char_count">0</span>/2000</span></div>
							</div>
						</div>
					</div>
					<div class="clear_kry"></div>
					<input type="submit" class="big_reg_button col-md-12 col-sm-12 col-xs-12" value="Зареєструватись на захід">
					<div class="clear_kry"></div>
				</div>
				</form>
				<div class="single_event_fill"></div>
			</div>
		</div>	
		<div class="clear_kry"></div>
	</div>
	<div class="clear"></div>
</div>
<style>
		.header{
			position: fixed;
		}

		.content-left-wrap{
			padding-top: 0;
		}

		footer{
			margin-top: 0;
		}

		@media only screen and (max-width: 767px){
			#footer {
				padding-top: 0;
				margin-top: 0px;
			}
		}
	</style>
<?php get_footer();  ?>