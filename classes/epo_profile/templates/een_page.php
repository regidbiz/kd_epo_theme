<?php 
if(!is_user_logged_in()){
	wp_redirect(get_home_url(), 302);
}
get_header();  
?>
<div class="clear"></div>
</header>
<div class="overflow_events_kry"></div>
<div id="content" class="site-content">
	<div class="container">
		<div class="content-left-wrap col-md-12">
			<div class="overflow_kry"></div>		
			<div class="service_learn_more_popup">
				<img class="close_service_learn_more_popup" src="<?php print get_stylesheet_directory_uri(); ?>/images/close_button.svg" alt="">
				<div class="service_learn_more_popup_title">Enterprise Europe Network</div>
				<div class="service_learn_more_popup_content">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus optio a nostrum ratione atque recusandae natus rerum necessitatibus dicta, quos. Minima aperiam quo quod fugiat dolor vero officiis et quasi!</p>
					<ul>
						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
					</ul>
				</div>
			</div>	
			<div class="user_events_blocks col-md-9 col-sm-12 col-xs-12">
				<div class="event_block">
					<a href="<?php echo home_url();?>/epo_profile/dashboard/">
						<div class="event_block_title">
							<p>Особиста інформація</p><img src="<?php print get_stylesheet_directory_uri(); ?>/images/arr_right.svg" alt="">
						</div>
						<div class="clear_kry"></div>
						<div class="event_block_desc">
							Редагуйте інформацію про себе та свою компанію
						</div>
					</a>
				</div>
				<div class="event_block active_event_block">
					<a href="<?php echo home_url();?>/epo_profile/services_list/">
						<div class="event_block_title">
							<p>Послуги</p><img src="<?php print get_stylesheet_directory_uri(); ?>/images/arr_right.svg" alt="">
						</div>
						<div class="clear_kry"></div>
						<div class="event_block_desc">
							Отримайте оцінку свого бізнесу та коментарі від експертів
						</div>
					</a>
				</div>
				<div class="event_block">
					<a href="<?php echo home_url();?>/epo_profile/events_for_user/">
						<div class="event_block_title">
							<p>Події</p><img src="<?php print get_stylesheet_directory_uri(); ?>/images/arr_right.svg" alt="">
						</div>
						<div class="clear_kry"></div>
						<div class="event_block_desc">
							Найактуальніші події та нетворкінг для експортерів
						</div>
					</a>
				</div>
				<hr>
			</div>
		</div>	
		<div class="clear_kry"></div>
		<form action="" class="company_dash_block readiness_assessment_page">
			<div class="company_greeting_wrap col-md-9 col-sm-12 col-xs-12">	
				<div class="single_service_head col-md-9 col-sm-8 col-xs-12 nopadding">
					<div class="service_single_page_title">
						<a href="<?php echo home_url();?>/epo_profile/services_list/">
							<img src="<?php print get_stylesheet_directory_uri(); ?>/images/back_arrow.png" alt="">
						</a>
						<span>Enterprise Europe Network</span>
					</div>
					<p class="service_single_page_description">EEN інформує про можливості розвитку в певній сфері, допомагає в залученні інвестицій для виробництва товарів і послуг, залученні нових технологій для підвищення конкурентноспроможності українських товаровиробників та підприємств, які надають послуги розвитку технологій, які були розроблені в Україні за допомогою закордонних партнерів.</p>
				</div>
				<div class="service_learn_more een_service_learn_more col-md-3 col-sm-4 col-xs-12">Детальніше про послугу</div>		
				<div class="fill_block_top"></div>	
			</div>
			<div class="clear_kry"></div>
			<div class="main_info_about_user col-md-12 col-sm-12 col-xs-12">
				<div class="user_self_info_wrapp col-md-9 col-sm-12 col-xs-12">		
					<nav class="een_page_menu">
						<ul>
							<li class="active_een_menu_item"><a href="#">Business offer</a></li>
							<li><a href="#">Business request</a></li>
							<li><a href="#">R&D request</a></li>
							<li><a href="#">Technology offer</a></li>
							<li><a href="#">Technology request</a></li>
						</ul>
					</nav>				
					<div class="single_event_reg_step_info een_page_step_block col-md-12 col-sm-12 col-xs-12">
						<div class="single_event_step_title">I. Details</div>					
						<div class="single_event_participant_info een_page_details_list col-md-12 col-sm-12 col-xs-12">
							<span class="col-md-5 col-sm-5 col-xs-12">Title</span>
							<input type="text" id="title" class="col-md-7 col-sm-7 col-xs-12">
							<div class="clear_kry"></div>
							<div class="company_textarea_blocks col-md-12 col-sm-12 col-xs-12">
								<span class="col-md-5 col-sm-5 col-xs-12">Summary</span>
								<textarea name="" class="company_textarea4 col-md-7 col-sm-7 col-xs-12" cols="30" rows="10"></textarea>
								<div class="char_counter_wrapp"><span><span class="char_count">0</span>/500</span></div>
							</div>
							<div class="clear_kry"></div>
							<div class="company_textarea_blocks col-md-12 col-sm-12 col-xs-12">
								<span class="col-md-5 col-sm-5 col-xs-12">Description (at list 100 characters)</span>
								<textarea name="" class="company_textarea5 col-md-7 col-sm-7 col-xs-12" cols="30" rows="10"></textarea>
								<div class="char_counter_wrapp"><span><span class="char_count">0</span>/4000</span></div>
							</div>
							<div class="clear_kry"></div>
							<div class="company_textarea_blocks col-md-12 col-sm-12 col-xs-12">
								<span class="col-md-5 col-sm-5 col-xs-12">Advantages and Innovations (at list 100 characters)</span>
								<textarea name="" class="company_textarea6 col-md-7 col-sm-7 col-xs-12" cols="30" rows="10"></textarea>
								<div class="char_counter_wrapp"><span><span class="char_count">0</span>/2000</span></div>
							</div>
							<div class="clear_kry"></div>
							<span class="col-md-5 col-sm-5 col-xs-12">Stage of development</span>
							<div class="company_stage_select col-md-7 col-sm-7 col-xs-12">
								<select>
									<option disabled selected value="Already on market">Already on market</option>									
								</select>
							</div>
							<div class="clear_kry"></div>
							<div class="company_stage_of_development col-md-12 col-sm-12 col-xs-12">
								<span class="col-md-5 col-sm-5 col-xs-12">Comments Regarding Stage of Development</span>
								<textarea name="" class="company_textarea7 col-md-7 col-sm-7 col-xs-12" cols="30" rows="10"></textarea>
							</div>
							<div class="clear_kry"></div>
							<span class="col-md-5 col-sm-5 col-xs-12">Profile origin</span>
							<div class="company_stage_select col-md-7 col-sm-7 col-xs-12">
								<select>
									<option value="Consumer programme">Consumer programme</option>									
								</select>
							</div>
							<div class="clear_kry"></div>
							<div class="company_textarea_blocks col-md-12 col-sm-12 col-xs-12">
								<span class="col-md-5 col-sm-5 col-xs-12">Technical Specifications or Expertise Sought</span>
								<textarea name="" class="company_textarea7 col-md-7 col-sm-7 col-xs-12" cols="30" rows="10"></textarea>
							</div>
							<div class="clear_kry"></div>
							<span class="col-md-5 col-sm-5 col-xs-12">IPR status</span>
							<div class="col-md-7 col-sm-7 col-xs-12 nopadding">
								<select class="select_multiple" multiple="multiple">
									<option value="Copyright" selected>Copyright</option>
									<option value="Patents granted" selected>Patents granted</option>
								</select>
							</div>
							<div class="clear_kry"></div>
							<div class="company_textarea_blocks col-md-12 col-sm-12 col-xs-12">
								<span class="col-md-5 col-sm-5 col-xs-12">Technical Specifications or Expertise Sought</span>
								<textarea name="" class="company_textarea7 col-md-7 col-sm-7 col-xs-12" cols="30" rows="10"></textarea>
							</div>
							<div class="clear_kry"></div>
						</div>
					</div>					
					<div class="single_event_reg_step single_event_reg_step_company col-md-12 col-sm-12 col-xs-12">
						<div class="single_event_step_title">II. Dissemination</div>						
					</div>
					<div class="single_event_reg_step_info single_event_reg_step_info_company col-md-12 col-sm-12 col-xs-12">						
						<div class="single_event_participant_info participant_company_info een_page_details_list2 col-md-12 col-sm-12 col-xs-12">
							<span class="col-md-5 col-sm-5 col-xs-12">Technology keywords (max 5)</span>
							<div class="col-md-7 col-sm-7 col-xs-12 nopadding">
								<select class="select_multiple" multiple="multiple">
									<option value="Copyright" selected>Copyright</option>
									<option value="Patents granted" selected>Patents granted</option>
								</select>
							</div>
							<div class="clear_kry"></div>
							<span class="col-md-5 col-sm-5 col-xs-12">Market keywords (max 5)</span>
							<div class="col-md-7 col-sm-7 col-xs-12 nopadding">
								<select class="select_multiple" multiple="multiple">
									<option value="Copyright" selected>Copyright</option>
									<option value="Patents granted" selected>Patents granted</option>
								</select>
							</div>
							<div class="clear_kry"></div>
							<span class="col-md-5 col-sm-5 col-xs-12">Sector group</span>
							<input type="text" id="sector_grope" class="col-md-7 col-sm-7 col-xs-12">
							<div class="clear_kry"></div>
							<span class="col-md-5 col-sm-5 col-xs-12">Profile origin</span>
							<div class="company_stage_select col-md-7 col-sm-7 col-xs-12">
								<select>
									<option disabled selected value="Consumer programme">Consumer programme</option>									
								</select>
							</div>
						</div>
					</div>
					<div class="single_event_reg_step single_event_reg_step_company col-md-12 col-sm-12 col-xs-12">
						<div class="single_event_step_title">III. Client</div>						
					</div>
					<div class="single_event_reg_step_info single_event_reg_step_info_company col-md-12 col-sm-12 col-xs-12">						
						<div class="single_event_participant_info participant_company_info een_page_details_list2 col-md-12 col-sm-12 col-xs-12">
							<span class="col-md-5 col-sm-5 col-xs-12">Type and size of client</span>
							<div class="company_stage_select col-md-3 col-sm-3 col-xs-12" data-placeholder="Pick one">
								<select>
									<option value="Consumer programme">Consumer programme</option>									
								</select>
							</div>
							<div class="clear_kry"></div>
							<span class="col-md-5 col-sm-5 col-xs-12">Year established</span>									
							<div class="company_info_year_input col-md-2 col-sm-2 col-xs-2">
								<select class="col-md-12 col-sm-12 col-xs-12">
									<?php
									for ($i = 2019; $i > 1900; $i--)
									{
										if ($i == 2019)
										{
											echo '<option value="1993" disabled selected style="color:#bababa;">1993</oplion>';
										}
										else
										{
											echo '<option value="'.$i.'">'.$i.'</oplion>';
										}
									}
									?>
								</select>
							</div>
							<div class="clear_kry"></div>
							<span class="col-md-5 col-sm-5 col-xs-12">NACE kyewords (max 5)</span>
							<div class="col-md-7 col-sm-7 col-xs-12 nopadding">
								<select class="select_multiple" multiple="multiple">
									<option value="Copyright" selected>Copyright</option>
									<option value="Patents granted" selected>Patents granted</option>
								</select>
							</div>
							<div class="clear_kry"></div>
							<span class="col-md-5 col-sm-5 col-xs-12">Annual turnower</span>
							<div class="company_info_year_input col-md-3 col-sm-3 col-xs-12">
								<select>
									<option disabled selected value="5-10">5-10</option>									
								</select>
							</div>
							<div class="after_annual_turnover col-md-3 col-sm-3 col-xs-6">min EUR</div>
							<div class="clear_kry"></div>
							<span class="col-md-5 col-sm-5 col-xs-12">Already engaged in trans-national cooperation</span>
							<input type="text" id="already_engaged" class="col-md-7 col-sm-7 col-xs-12">
							<div class="clear_kry"></div>
							<div class="company_textarea_blocks col-md-12 col-sm-12 col-xs-12">
								<span class="col-md-5 col-sm-5 col-xs-12">Additional comments</span>
								<textarea name="" class="company_textarea7 col-md-7 col-sm-7 col-xs-12" cols="30" rows="10"></textarea>
							</div>
							<div class="clear_kry"></div>
							<span class="col-md-5 col-sm-5 col-xs-12">Certification Standarts</span>
							<div class="company_stage_select col-md-3 col-sm-3 col-xs-12">
								<select>
									<option disabled selected value="Pick one">Pick one</option>									
								</select>
							</div>
							<div class="clear_kry"></div>
							<span class="col-md-5 col-sm-5 col-xs-12">Language Spoken</span>
							<div class="company_select_block col-md-7 col-sm-7 col-xs-12 nopadding">
								<select class="select_multiple" multiple="multiple">
									<option value="English" selected>English</option>
									<option value="Russian" selected>Russian</option>
								</select>
							</div>
						</div>
					</div>
					<div class="single_event_reg_step single_event_reg_step_company col-md-12 col-sm-12 col-xs-12">
						<div class="single_event_step_title">IV. Partner sought</div>						
					</div>
					<div class="single_event_reg_step_info single_event_reg_step_info_company col-md-12 col-sm-12 col-xs-12">						
						<div class="single_event_participant_info participant_company_info een_page_details_list2 col-md-12 col-sm-12 col-xs-12">
							<span class="col-md-5 col-sm-5 col-xs-12">Type and role of partner sought</span>
							<input type="text" id="type_role_partner_sought" class="col-md-7 col-sm-7 col-xs-12">
							<span class="col-md-5 col-sm-5 col-xs-12">Type and size of partner sought</span>
							<div class="company_stage_select col-md-3 col-sm-3 col-xs-12">
								<select>
									<option disabled selected value=">500"> > 500</option>									
								</select>
							</div>
							<div class="clear_kry"></div>
							<span class="col-md-5 col-sm-5 col-xs-12">Type of partnership considered</span>
							<div class="company_stage_select col-md-3 col-sm-3 col-xs-12">
								<select>
									<option disabled selected value="Acquisition agreement">Acquisition agreement</option>									
								</select>
							</div>
						</div>
					</div>		
					<div class="single_event_reg_step single_event_reg_step_company col-md-12 col-sm-12 col-xs-12">
						<div class="single_event_step_title">V. Attachment</div>						
					</div>
					<div class="single_event_reg_step_info single_event_reg_step_info_company col-md-12 col-sm-12 col-xs-12">						
						<div class="single_event_participant_info participant_company_info een_page_details_list2 col-md-12 col-sm-12 col-xs-12">
							<div class="attechment_block_wrapp">
								<div class="upload_part_block upload_part_block_first col-md-6 col-sm-12 col-xs-12">
									<div class="upload_block first_upload_block">
										<img src="<?php print get_stylesheet_directory_uri(); ?>/images/upload_button_een.png" alt="">
									</div>
									<div class="upload_block second_upload_block" class="text_block">
										<span>Drag a file here in png, jpeg, bmp, doc, docx, xls or xlsx (max 4mb)...</span>
									</div>
								</div>
								<div class="upload_part_block upload_part_block_second col-md-6 col-sm-12 col-xs-12">
									<div class="upload_block first_upload_block">
										<span>...or choose a file from the file system</span>
									</div>
									<div class="upload_block second_upload_block">
										<input type="file" id="upload_file" class="upload_photo" style="display:none;">
										<label class="upload_part_block_label" for="upload_file" ><span>Select a file</span></label>
									</div>
								</div>
							</div>
						</div>
					</div>				
					<div class="een_save_icons col-md-12 col-sm-12 col-xs-12">
						<div class="een_save_blocks een_save_blocks_first">
							<a class="watch_test_results" href="<?php echo home_url();?>/epo_profile/thanks_order_service/">Замовити послугу</a>
						</div>
						<div class="een_save_blocks info_save_icons een_save_blocks_second">
							<a class="save_event_canges"><i class="fa fa-floppy-o" aria-hidden="true"></i>Зберегти зміни</a>
						</div>
					</div>
					<div class="clear_kry"></div>
					<div class="fill_block_white"></div>
				</div>
				<div class="user_company_info services_page_company_aside single_service_page_company_aside col-md-3  col-sm-12 col-xs-12">
					<div class="user_info_block_title">Мої компанії</div>
					<div class="overlay_companies"></div>
					<div class="clear_kry"></div>
					<div class="company_info_box">
						<div class="company_info_box_title">
							Вкажіть дані вашої компанії, щоб отримати доступ до послуг:
						</div>
						<div class="company_inputs_block">
							<label for="">Найменування юридичної особи</label>
							<input type="text" placeholder="напр. Ексім-меблі 3000">
							<label for="">Реєстраційний код (ЄРДПОУ або ІПН)</label>
							<input type="text">
							<label for="">Ваша посада</label>
							<input type="text">
							<label for="">Повна назва посади</label>
							<input type="text">
						</div>
						<input type="button" class="add_company_button" value="Додати компанію">
					</div>	
					<!--  Если пользователь залогиненый -->	
					<div class="logged_user">
						<div class="company_info_box active_company_info_box col-md-12 col-sm-12 col-xs-12">
							<span class="company_side_line"></span>
							<div class='remove_this_company'>
								<img src="<?php print get_stylesheet_directory_uri(); ?>/images/thrash.svg" alt="">
							</div>
							<img src="<?php print get_stylesheet_directory_uri(); ?>/images/credi_logo.png" alt="" class="user_logged_company_img">
							<div class="logged_company_names_wrapp">
								<span class="logged_company_name">Агріколь</span>
								<span class="logged_company_reg_num">12345678</span>									
							</div>
							<div class="clear_kry"></div>
							<a href="<?php echo home_url();?>/epo_profile/company/">Оновити інформацію</a>
							<div class="popup_delet_company">							
								<div class="change_pass_popup_head">
									<span>Ви впевнені що хочете прибрати цю компанію?</span>
									<img src="<?php print get_stylesheet_directory_uri(); ?>/images/close_button.svg" alt="">
								</div>	
								<span class="confirm_delete_company_aside col-md-5 col-sm-5 col-xs-5">Так</span><span class="dont_delete_company_aside col-md-5 col-sm-5 col-xs-5">Ні</span>
							</div>
						</div>	
						<div class="clear_kry"></div>
						<div class="company_info_box col-md-12 col-sm-12 col-xs-12">
							<span class="company_side_line"></span>
							<div class='remove_this_company'>
								<img src="<?php print get_stylesheet_directory_uri(); ?>/images/thrash.svg" alt="">
							</div>
							<img src="<?php print get_stylesheet_directory_uri(); ?>/images/nothumb.png" alt="" class="user_logged_company_img">
							<div class="logged_company_names_wrapp">
								<span class="logged_company_name">ФОП Носов Дмитро Андрійович</span>
								<span class="logged_company_reg_num">12345678</span>
							</div>	
							<div class="clear_kry"></div>
							<a href="">Оновити інформацію</a>	
							<div class="popup_delet_company">							
								<div class="change_pass_popup_head">
									<span>Ви впевнені що хочете прибрати цю компанію?</span>
									<img src="<?php print get_stylesheet_directory_uri(); ?>/images/close_button.svg" alt="">
								</div>	
								<span class="confirm_delete_company_aside col-md-5 col-sm-5 col-xs-5">Так</span><span class="dont_delete_company_aside col-md-5 col-sm-5 col-xs-5">Ні</span>
							</div>						
						</div>
						<div class="clear_kry"></div>
						<div class="logged_user_add_company_button">
							<img src="<?php print get_stylesheet_directory_uri(); ?>/images/add_company_button.png" alt="">Додати компанію
						</div>
					</div>							
				</div>
				<div class="fill_block"></div>
			</div>
		</form>
		<div class="fill_block_bottom"></div>
		<!-- Галерея услуг -->
		<div class="clear_kry"></div>
		<div class="carousel">
			<div class="overlay_companies"></div>
			<div class="services_carosel_title">Інші послуги</div>
			<div class="services_carousel owl-carousel">
				<a href="<?php echo home_url();?>/epo_profile/readliness_assessment_page/">
					<div class="corusel_item">
						<div class="service_carousel_item_title">Оцінка готовності</div>
						<div class="service_carousel_item_description">Оцініть свою готовність до зовнішньоекономічної діяльності. Визначте компетенції, яких вам бракує для досягення успіху.</div>
					</div>
				</a>
				<a href="<?php echo home_url();?>/epo_profile/checking_idea_page/">
					<div class="corusel_item">
						<div class="service_carousel_item_title">Перевірка ідеї</div>
						<div class="service_carousel_item_description">Отримайте допомогу у розробці плану інтернаціоналізації вашого бізнесу.</div>
					</div>
				</a>
				<a href="<?php echo home_url();?>/epo_profile/mentorstvo_page/">
					<div class="corusel_item">
						<div class="service_carousel_item_title">Менторство</div>
						<div class="service_carousel_item_description">Зв'язок з фахівцями, які зможуть надати професійну допомогу із пошуку партнерів та аналізу ринку.</div>
					</div>
				</a>
				<a href="<?php echo home_url();?>/epo_profile/internationalization_bussiness_page/">
					<div class="corusel_item">
						<div class="service_carousel_item_title">Інтернаціоналізація бізнесу</div>
						<div class="service_carousel_item_description">Зв'язок з фахівцями, які зможуть надати професійну допомогу із пошуку партнерів та аналізу ринку.</div>
					</div>
				</a>
				<a href="<?php echo home_url();?>/epo_profile/sourcing_service_page/">
					<div class="corusel_item">
						<div class="service_carousel_item_title">Soursing</div>
						<div class="service_carousel_item_description">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</div>
					</div>
				</a>
				<a href="<?php echo home_url();?>/epo_profile/een_page/">
					<div class="corusel_item">
						<div class="service_carousel_item_title">Enterprise Europe Network</div>
						<div class="service_carousel_item_description">EEN інформує про можливості розвитку в певній сфері, допомагає в залученні інвестицій для виробництва товарів і послуг, залученні нових технологій для підвищення конкурентноспроможності українських товаровиробників та підприємств, які надають послуги розвитку технологій, які були розроблені в Україні за допомогою закордонних партнерів.</div>
					</div>
				</a>			
			</div>
		</div>
		<div class="clear_kry"></div>
	</div>
	<div class="clear"></div>
</div>
<style>
.header{
	position: fixed;
}

.content-left-wrap{
	padding-top: 0;
}
</style>
<?php get_footer();  ?>