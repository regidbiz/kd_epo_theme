<?php 
if(!is_user_logged_in()){
	wp_redirect(get_home_url(), 302);
}
get_header();  
?>
<div class="clear"></div>
</header>
<div class="overflow_events_kry"></div>
<div id="content" class="site-content">
	<div class="container">
		<div class="content-left-wrap thanks_page col-md-12">
			<div class="thanks_block_wrapp">
				<img class="thanks_block_image_ok" src="<?php print get_stylesheet_directory_uri(); ?>/images/ok_shevron.svg" alt="">
				<div class="thanks_block_text_ok">Дякуємо за замовлення послуги!</div>
				<div class="thanks_block_sub_text_ok">Наші спеціалісти зв'яжутся з Вами у найближчий час</div>
				<div class="thanks_link_block col-md-6 col-sm-6 col-xs-6"><a href="<?php echo home_url();?>/epo_profile/dashboard/"><span>Повернутись до особистого кабінету</span><img src="<?php print get_stylesheet_directory_uri(); ?>/images/back_arrow_wth_cir.svg" alt=""></a></div>
				<div class="thanks_link_block col-md-6 col-sm-6 col-xs-6"><a href="<?php echo home_url();?>/"><i class="fa fa-home" aria-hidden="true"></i><span>На домашню сторінку</span></a></div>
			</div>
		</div>
		<div class="clear_kry"></div>
		
			<div class="carousel">
				<div class="services_carosel_title">Інші послуги</div>
				<div class="services_carousel owl-carousel">
					<a href="<?php echo home_url();?>/epo_profile/readliness_assessment_page/">
						<div class="corusel_item">
							<div class="service_carousel_item_title">Оцінка готовності</div>
							<div class="service_carousel_item_description">Консультації компаній, які мають ідеї щодо виходу на зовнішні ринки</div>
						</div>
					</a>
					<a href="<?php echo home_url();?>/epo_profile/checking_idea_page/">
						<div class="corusel_item">
							<div class="service_carousel_item_title">Перевірка ідеї</div>
							<div class="service_carousel_item_description">Отримайте допомогу у розробці плану інтернаціоналізації вашого бізнесу.</div>
						</div>
					</a>
					<a href="<?php echo home_url();?>/epo_profile/mentorstvo_page/">
						<div class="corusel_item">
							<div class="service_carousel_item_title">Менторство</div>
							<div class="service_carousel_item_description">Зв'язок з фахівцями, які зможуть надати професійну допомогу із пошуку партнерів та аналізу ринку.</div>
						</div>
					</a>
					<a href="<?php echo home_url();?>/epo_profile/internationalization_bussiness_page/">
						<div class="corusel_item">
							<div class="service_carousel_item_title">Інтернаціоналізація бізнесу</div>
							<div class="service_carousel_item_description">Зв'язок з фахівцями, які зможуть надати професійну допомогу із пошуку партнерів та аналізу ринку.</div>
						</div>
					</a>
					<a href="<?php echo home_url();?>/epo_profile/sourcing_service_page/">
						<div class="corusel_item">
							<div class="service_carousel_item_title">Soursing</div>
							<div class="service_carousel_item_description">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</div>
						</div>
					</a>
					<a href="<?php echo home_url();?>/epo_profile/een_page/">
						<div class="corusel_item">
							<div class="service_carousel_item_title">Enterprise Europe Network</div>
							<div class="service_carousel_item_description">EEN інформує про можливості розвитку в певній сфері, допомагає в залученні інвестицій для виробництва товарів і послуг, залученні нових технологій для підвищення конкурентноспроможності українських товаровиробників та підприємств, які надають послуги розвитку технологій, які були розроблені в Україні за допомогою закордонних партнерів.</div>
						</div>
					</a>			
				</div>
			</div>
		<div class="clear_kry"></div>
	</div>
	<div class="clear"></div>
</div>
<style>
.header{
	position: fixed;
}

.content-left-wrap{
	padding-top: 0;
}
</style>
<?php get_footer();  ?>