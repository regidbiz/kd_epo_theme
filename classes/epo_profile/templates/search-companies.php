<?php 
if(!is_user_logged_in()){
	wp_redirect(get_home_url(), 302);
}
get_header();  
?>
<div class="clear"></div>
</header>
	<div class="content">
		<header class="search-header">
			<div class="container">
				<div class="search-header__results">
					<div class="result-found">
						<h1>
							<span class="result-found__quantity">51</span><span> partners found</span>
							<a class="add_industry" href="#">Add or change industry code</a>
						</h1>
						<div class="add-tosearch"></div>
					</div>
				</div>
				<div class="search-header__filters">
					<div class="filter_items_wrapp">
						<div class="filter_item regions">
							<span>All regions</span> 
							<img src="<?php print get_stylesheet_directory_uri(); ?>/images/close.svg" alt="">
							<div class="filter_item_chosen_block" style="display: none;">
								<div class="filter_chosen_item">
									<span>
										<input type="checkbox">		
									</span>
									<label>Лютий 2018</label>							
								</div>
								<div class="filter_chosen_item">
									<span>
										<input type="checkbox">		
									</span>
									<label>Березень 2018</label>							
								</div>
								<div class="filter_chosen_item">
									<span>
										<input type="checkbox">		
									</span>
									<label>Квітень 2018</label>							
								</div>
								<div class="filter_chosen_item">
									<span>
										<input type="checkbox">		
									</span>
									<label>Травень 2018</label>
								</div>
								<div class="filter_chosen_item">
									<span>
										<input type="checkbox">		
									</span>
									<label>Вересень 2018</label>
								</div>
								<div class="confirm_chose">
									<span>Дивитись події</span>
								</div>
							</div>
						</div>
						<div class="filter_item date">
							<span>All exports markets</span> 
							<img src="<?php print get_stylesheet_directory_uri(); ?>/images/close.svg" alt="">
							<div class="filter_item_chosen_block" style="display: none;">
								<div class="filter_chosen_item">
									<span>
										<input type="checkbox">		
									</span>
									<label>Лютий 2018</label>							
								</div>
								<div class="filter_chosen_item">
									<span>
										<input type="checkbox">		
									</span>
									<label>Березень 2018</label>							
								</div>
								<div class="filter_chosen_item">
									<span>
										<input type="checkbox">		
									</span>
									<label>Квітень 2018</label>							
								</div>
								<div class="filter_chosen_item">
									<span>
										<input type="checkbox">		
									</span>
									<label>Травень 2018</label>
								</div>
								<div class="filter_chosen_item">
									<span>
										<input type="checkbox">		
									</span>
									<label>Вересень 2018</label>
								</div>
								<div class="confirm_chose">
									<span>Дивитись події</span>
								</div>
							</div>
						</div>
						<div class="filter_item types">
							<span>Any turnover</span> 
							<img src="<?php print get_stylesheet_directory_uri(); ?>/images/close.svg" alt="">					
							<div class="filter_item_chosen_block" style="display: none;">
								<div class="filter_chosen_item">
									<span>
										<input type="checkbox">		
									</span>
									<label>Усі типи</label>							
								</div>
								<div class="filter_chosen_item">
									<span>
										<input type="checkbox">		
									</span>
									<label>Лише виставки</label>							
								</div>
								<div class="filter_chosen_item">
									<span>
										<input type="checkbox">		
									</span>
									<label>Лише торгові місії</label>							
								</div>
								<div class="confirm_chose">
									<span>Дивитись події</span>
								</div>
							</div>					
						</div>
						<div class="filter_item reg_status">
							<span>Any numbers of employers</span> 
							<img src="<?php print get_stylesheet_directory_uri(); ?>/images/close.svg" alt="">
							<div class="filter_item_chosen_block" style="display: none;">
								<div class="filter_chosen_item">
									<span>
										<input type="checkbox">		
									</span>
									<label>Усі статуси</label>							
								</div>
								<div class="filter_chosen_item">
									<span>
										<input type="checkbox">		
									</span>
									<label>Лише з відкритою реєстрацією</label>							
								</div>
								
								<div class="confirm_chose">
									<span>Дивитись події</span>
								</div>
							</div>
						</div>
						<hr>
					</div>
				</div>
			</div>
		</header>
		<div class="search-content">
			<div class="container">
				<div class="search-blocks">
					<div class="search-block">
						<div class="search-block__cell search-block__cell_logo">
							<img src="<?php print get_stylesheet_directory_uri(); ?>/images/logo-enzim.jpg" alt="Company Logo">
						</div>
						<div class="search-block__cell search-block__cell_info">
							<span class="info-name">PrJSC Enzym Company</span>
							<span class="info-phone">123456789123</span>
							<span class="info-location">Brody, Lviv oblast</span>
							<div class="info-website">
								<a href="#">http://enzym.lviv.ua</a>
							</div>
						</div>
						<div class="search-block__cell search-block__cell_details">
							<ul class="details-list">
								<li>
									<span>10-50 mln UAH</span>
									<span>annual turnover</span>
								</li>
								<li>
									<span>5-20</span>
									<span>employers</span>
								</li>
								<li>
									<span>China, Canada, USA</span>
									<span>export markets</span>
								</li>
							</ul>
							<div class="industry-list">
								<span>87225001 - Association of the Promotion of Industrial Development</span>
								<a href="#">and 4 more</a>
							</div>
							<div class="goods-list">
								<span>4015 - Articles of apparel and clothing accessories (including gloves and mitts) for all...</span>
								<a href="#">and 1 more</a>
							</div>
						</div>
						<div class="search-block__cell search-block__cell_contacts">
							<div class="contacts-name"><span>Vitaliy Koroblenko</span></div>
							<div class="contacts-position"><span>CEO</span></div>
							<div class="contacts-phone"><span>+38 098 765-43-21</span></div>
							<div class="contacts-email"><a href="mailto:koroblenko@enzym.lviv.ua">koroblenko@enzym.lviv.ua</a></div>
						</div>
					</div>
					<div class="search-block">
						<div class="search-block__cell search-block__cell_logo">
							<img src="<?php print get_stylesheet_directory_uri(); ?>/images/logo-enzim.jpg" alt="Company Logo">
						</div>
						<div class="search-block__cell search-block__cell_info">
							<span class="info-name">Apriorit</span>
							<span class="info-phone">123456789123</span>
							<span class="info-location">Lutsk, Volyn oblast</span>
							<div class="info-website">
								<a href="#">http://www.apriorit.com</a>
							</div>
						</div>
						<div class="search-block__cell search-block__cell_details">
							<ul class="details-list">
								<li>
									<span>5-10 mln UAH</span>
									<span>annual turnover</span>
								</li>
								<li>
									<span>20-50</span>
									<span>employers</span>
								</li>
								<li>
									<span>Canada, USA</span>
									<span>export markets</span>
								</li>
							</ul>
							<div class="industry-list">
								<span>87225001 - Association of the Promotion of Industrial Development</span>
								<a href="#">and 4 more</a>
							</div>
						</div>
						<div class="search-block__cell search-block__cell_contacts">
							<div class="contacts-name"><span>Eduard Antonov</span></div>
							<div class="contacts-position"><span>New Business Manager</span></div>
							<div class="contacts-phone"><span>+38 056 736-31-05</span></div>
							<div class="contacts-email"><a href="mailto:info@apriorit.com">info@apriorit.com</a></div>
						</div>
					</div>
					<div class="search-block">
						<div class="search-block__cell search-block__cell_logo">
							<img src="<?php print get_stylesheet_directory_uri(); ?>/images/logo-enzim.jpg" alt="Company Logo">
						</div>
						<div class="search-block__cell search-block__cell_info">
							<span class="info-name">Agricole</span>
							<span class="info-phone">123456789123</span>
							<span class="info-location">Brody, Lviv oblast</span>
							<div class="info-website">
								<a href="#">http://enzym.lviv.ua</a>
							</div>
						</div>
						<div class="search-block__cell search-block__cell_details">
							<ul class="details-list">
								<li>
									<span>10-50 mln UAH</span>
									<span>annual turnover</span>
								</li>
								<li>
									<span>5-20</span>
									<span>employers</span>
								</li>
								<li>
									<span>China, Canada, USA</span>
									<span>export markets</span>
								</li>
							</ul>
							<div class="goods-list">
								<span>4015 - Articles of apparel and clothing accessories (including gloves and mitts) for all...</span>
								<a href="#">and 1 more</a>
							</div>
						</div>
						<div class="search-block__cell search-block__cell_contacts">
							<div class="contacts-name"><span>Vitaliy Koroblenko</span></div>
							<div class="contacts-position"><span>CEO</span></div>
							<div class="contacts-phone"><span>+38 098 765-43-21</span></div>
							<div class="contacts-email"><a href="mailto:koroblenko@enzym.lviv.ua">koroblenko@enzym.lviv.ua</a></div>
						</div>
					</div>
					<div class="search-block">
						<div class="search-block__cell search-block__cell_logo">
							<img src="<?php print get_stylesheet_directory_uri(); ?>/images/logo-enzim.jpg" alt="Company Logo">
						</div>
						<div class="search-block__cell search-block__cell_info">
							<span class="info-name">Point Agency</span>
							<span class="info-phone">123456789123</span>
							<span class="info-location">Kyiv, Kyiv oblast</span>
							<div class="info-website">
								<a href="#">http://enzym.lviv.ua</a>
							</div>
						</div>
						<div class="search-block__cell search-block__cell_details">
							<ul class="details-list">
								<li>
									<span>5-10 mln UAH</span>
									<span>annual turnover</span>
								</li>
								<li>
									<span>5-20</span>
									<span>employers</span>
								</li>
								<li>
									<span>USA</span>
									<span>export markets</span>
								</li>
							</ul>
							<div class="industry-list">
								<span>87225001 - Association of the Promotion of Industrial Development</span>
								<a href="#">and 4 more</a>
							</div>
						</div>
						<div class="search-block__cell search-block__cell_contacts">
							<div class="contacts-name"><span>Taras Khvyl</span></div>
							<div class="contacts-position"><span>Chairman</span></div>
							<div class="contacts-phone"><span>+38 098 765-43-21</span></div>
							<div class="contacts-email"><a href="mailto:tk@point-agency.ua">tk@point-agency.ua</a></div>
						</div>
					</div>
					<div class="search-block">
						<div class="search-block__cell search-block__cell_logo">
							<img src="<?php print get_stylesheet_directory_uri(); ?>/images/logo-enzim.jpg" alt="Company Logo">
						</div>
						<div class="search-block__cell search-block__cell_info">
							<span class="info-name">Sponge Digital & Design</span>
							<span class="info-phone">123456789123</span>
							<span class="info-location">Odessa, Odessa oblast</span>
							<div class="info-website">
								<a href="#">http://sponge.com.ua</a>
							</div>
						</div>
						<div class="search-block__cell search-block__cell_details">
							<ul class="details-list">
								<li>
									<span>5-10 mln UAH</span>
									<span>annual turnover</span>
								</li>
								<li>
									<span>20-50</span>
									<span>employers</span>
								</li>
								<li>
									<span>Canada, Israel, Germany, France, Russia</span>
									<span>export markets</span>
								</li>
							</ul>
							<div class="industry-list">
								<span>87225001 - Association of the Promotion of Industrial Development</span>
								<a href="#">and 4 more</a>
							</div>
						</div>
						<div class="search-block__cell search-block__cell_contacts">
							<div class="contacts-name"><span>Alexey Cherednichenko</span></div>
							<div class="contacts-position"><span>CEO</span></div>
							<div class="contacts-phone"><span>+38 098 765-43-21</span></div>
							<div class="contacts-email"><a href="mailto:ac@sponge.com.ua">ac@sponge.com.ua</a></div>
						</div>
					</div>
					<div class="search-block">
						<div class="search-block__cell search-block__cell_logo">
							<img src="<?php print get_stylesheet_directory_uri(); ?>/images/logo-enzim.jpg" alt="Company Logo">
						</div>
						<div class="search-block__cell search-block__cell_info">
							<span class="info-name">Vintage</span>
							<span class="info-phone">123456789123</span>
							<span class="info-location">Kyiv, Kyiv oblast</span>
							<div class="info-website">
								<a href="#">http://vintage.com.ua</a>
							</div>
						</div>
						<div class="search-block__cell search-block__cell_details">
							<ul class="details-list">
								<li>
									<span>5-10 mln UAH</span>
									<span>annual turnover</span>
								</li>
								<li>
									<span>20-50</span>
									<span>employers</span>
								</li>
								<li>
									<span>Israel, Germany, France, Russia, USA</span>
									<span>export markets</span>
								</li>
							</ul>
							<div class="industry-list">
								<span>87225001 - Association of the Promotion of Industrial Development</span>
								<a href="#">and 2 more</a>
							</div>
						</div>
						<div class="search-block__cell search-block__cell_contacts">
							<div class="contacts-name"><span>V. Shevchenko</span></div>
							<div class="contacts-position"><span>CEO</span></div>
							<div class="contacts-phone"><span>+38 044 337 67 01</span></div>
							<div class="contacts-email"><a href="mailto:sales@vintage.com.ua">sales@vintage.com.ua</a></div>
						</div>
					</div>
					<div class="load_more col-md-12 col-sm-12 col-xs-12">
						<i class="fa fa-repeat" aria-hidden="true"></i>
						SEE MORE 10 COMPANIES
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="clear"></div>
<style>
	.header{
		position: fixed;
	}
	.content-left-wrap{
		padding-top: 0;
	}
	body{
		background-color: #f4f4f4;
	}
	.container{
		padding: 0!important;
	}
</style>
<?php get_footer();  ?>