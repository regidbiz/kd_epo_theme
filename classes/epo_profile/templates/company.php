<?php 
if(!is_user_logged_in()){
	wp_redirect(get_home_url(), 302);
}
get_header();  
?>
<div class="clear"></div>
</header>
<div id="content" class="site-content">

	<div class="container">

		<div class="content-left-wrap col-md-12">
			<div class="popup_for_search_KVED">
				<img class="close_search_popup" src="<?php print get_stylesheet_directory_uri(); ?>/images/close_button.svg" alt="">
				<div class="serch_title">Додати КВЕД</div>
				<div class="clear_kry"></div>
				<div class="main_search_wrapp">
					<div class="search_button_wrapp">
						<input type="submit" value=""><i class="fa fa-search" aria-hidden="true"></i>
					</div>
					<input name="s" type="search" placeholder="Введіть назву товару/послуги або КВЕД" class="search_input">
					
					<div class="search_results">	
						<ul class="list-kved">
							<li class="list-kved__item">
								<span class="list-kved__point close"><svg></svg></span>
								<span class="list-kved__title">80 - Услуги бизенса</span>
								<ul>
									<li class="list-kved__item">
										<span class="list-kved__point close"><svg></svg></span>
										<span class="list-kved__title">80 - Услуги бизенсу</span>
									</li>
									<li class="list-kved__item">
										<span class="list-kved__point close"><svg></svg></span>
										<span class="list-kved__title">Финансовые и страховые услуги</span>
										<ul>
											<li class="list-kved__item">
												<span class="list-kved__point close"><svg></svg></span>
												<span class="list-kved__title">Бумага, печатные и издательские услуги</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point close"><svg></svg></span>
												<span class="list-kved__title">Досуг развлечения  и туризм</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point close"><svg></svg></span>
												<span class="list-kved__title">Информационные технологии, компьютеры, программное обеспечение, Интернет, исследовани и разработки</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point close"><svg></svg></span>
												<span class="list-kved__title">Металлы, станки, машины, металлообработка</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point close"><svg></svg></span>
												<span class="list-kved__title">Минералы, руды, полезные ископаемые</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point close"><svg></svg></span>
												<span class="list-kved__title">Образование, обучение, общественные институты</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point close"><svg></svg></span>
												<span class="list-kved__title">Розничаная торговля и дистрибуци</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point close"><svg></svg></span>
												<span class="list-kved__title">Сельское хозяйство, продукты питания</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point close"><svg></svg></span>
												<span class="list-kved__title">Строительство</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point close"><svg></svg></span>
												<span class="list-kved__title">Текстиль, одежда, кожа, часы, ювелизрные изделия</span>
											</li>
											<li class="list-kved__item">
												<span class="list-kved__point close"><svg></svg></span>
												<span class="list-kved__title">Транспорт и логистика</span>
											</li>
										</ul>
									</li>		
								</ul>
							</li>
						</ul>
					</div>
					<div class="clear_kry"></div>
					<div class="save_button_wrapp">	
						<input type="submit" value="Зберегти зміни">
					</div>
				</div>
			</div>
			<div class="popup_for_search_HS">
				<img class="close_search_popup" src="<?php print get_stylesheet_directory_uri(); ?>/images/close_button.svg" alt="">
				<div class="serch_title">Додати HS</div>
				<div class="clear_kry"></div>
				<div class="main_search_wrapp">
					<div class="search_button_wrapp">
						<input type="submit" value=""><i class="fa fa-search" aria-hidden="true"></i>
					</div>
					<input name="s" type="search" placeholder="Введіть назву товару/послуги або HS" class="search_input">									
					<div class="search_results">	
						
					</div>
					<div class="clear_kry"></div>
					<div class="save_button_wrapp">	
						<input type="submit" value="Зберегти зміни">
					</div>
				</div>								
			</div>
			<div class="user_events_blocks col-md-9 col-sm-12 col-xs-12">
				<div class="event_block active_event_block">
					<a href="#">
						<div class="event_block_title">
							<p>Особиста інформація</p><img src="<?php print get_stylesheet_directory_uri(); ?>/images/arr_right.svg" alt="">
						</div>
						<div class="clear_kry"></div>
						<div class="event_block_desc">
							Редагуйте інформацію про себе та свою компанію
						</div>
					</a>
				</div>
				<div class="event_block">
					<a href="<?php echo home_url();?>/epo_profile/services_list/">
						<div class="event_block_title">
							<p>Послуги</p><img src="<?php print get_stylesheet_directory_uri(); ?>/images/arr_right.svg" alt="">
						</div>
						<div class="clear_kry"></div>
						<div class="event_block_desc">
							Отримайте оцінку свого бізнесу та коментарі від експертів
						</div>
					</a>
				</div>
				<div class="event_block">
					<a href="<?php echo home_url();?>/epo_profile/events_for_user/">
						<div class="event_block_title">
							<p>Події</p><img src="<?php print get_stylesheet_directory_uri(); ?>/images/arr_right.svg" alt="">
						</div>
						<div class="clear_kry"></div>
						<div class="event_block_desc">
							Найактуальніші події та нетворкінг для експортерів
						</div>
					</a>
				</div>
				<hr>
			</div>
			<div class="clear_kry"></div>
			<form action="" class="company_dash_block col-md-12">
				<div class="company_greeting_wrap col-md-9 col-sm-12 col-xs-12">	
					<div class="user_greeting col-md-8 col-sm-8 col-xs-8">
						<a href="<?php echo home_url();?>/epo_profile/dashboard/"><img src="<?php print get_stylesheet_directory_uri(); ?>/images/back_arrow.png" alt=""></a><span class="company_greeting_name_block"> <span class="company_name">Креді Агріколь Банк Україна</span>!</span>
					</div>
					<!--<input class="delet_this_company_button col-md-4 col-sm-4 col-xs-4" type="button" value="Видалити компанію">-->	
					<div class="delet_this_company_button col-md-4 col-sm-4 col-xs-4">Видалити компанію</div>		
					<div class="overflow_kry"></div>
					<div class="delet_this_company_confirm">
						<span>Ви дійсно хочете видалити цю компанію?</span>
						<input type="button" value="Так, видалити"><input type="button" value="Ні">
					</div>
					<!--<div class="delet_this_company_popup">
						<div class="change_pass_popup_head">
							<span>Ви впевнені що хочете прибрати цю компанію?</span>
							<img src="<?php print get_stylesheet_directory_uri(); ?>/images/close_button.png" alt="">
						</div>
						<input type="submit" class="col-md-5 col-sm-5 col-xs-5" value="Так"><span class=" dont_delete col-md-5 col-sm-5 col-xs-5">Ні</span>
					</div>-->
					<div class="fill_block_top"></div>
				</div>
				<div class="clear_kry"></div>
				<div class="main_info_about_user col-md-12 col-sm-12 col-xs-12">
					<div class="user_self_info_wrapp col-md-9 col-sm-12 col-xs-12">
						<div class="user_self_info col-md-12 col-sm-12 col-xs-12">	
							<div class="user_info_block_title">Загальна інформація</div>
							<div class="about_user_blocks">
								<div class="user_photo_wrapp">
									<div class="user_photo">
										<div class="user_photo_overlay"></div>
										<img src="<?php print get_stylesheet_directory_uri(); ?>/images/default_company_image.png" alt="">
									</div>
									<div class="clear_kry"></div>
									<input type="file" id="upload_avatar" class="upload_photo" style="display:none;">
									<label class="label_upload_avatar" for="upload_avatar" ><img class="upload_image" src="<?php print get_stylesheet_directory_uri(); ?>/images/upload_button.png" alt=""><span>Завантажити фото</span></label>
								</div>
								<div class="avatar-block">
									<div class="avatar-block__choose choose-picture">
										<div class="close-button"><i class="icon-bar-close"></i><i class="icon-bar-close"></i></div>
										<div class="choose-button">
											<input type="file" id="upload_avatar_mobile" class="upload_photo" style="display:none;">
											<label for="upload_avatar_mobile">
												<span>Завантажити фото</span>
											</label>
										</div>
									</div>
									<div class="avatar-block__choose">
										<div class="close-button"><i class="icon-bar-close"></i><i class="icon-bar-close"></i></div>
										<div class="download-block">
											<div class="download-block__image"><img src="<?php print get_stylesheet_directory_uri(); ?>/images/simple-line-icons.png" alt=""></div>
											<div class="download-block__info">
												<div class="title">
 													<span class="title__name">my_visa_photo.jpg <span class="title__name_size">(353 kb)</span>
													<img class="title__delete"src="<?php print get_stylesheet_directory_uri(); ?>/images/thrash.svg" alt="delete avatar">
												</div>
												<div class="progress-bar"></div>
											</div>
										</div>
									</div>
									</div>
								<div class="user_info_wrapp">
									<div class="user-block">
										<div class="user-block__title">Назва офіційна</div>
										<div class="user-block__content">
											<input type="text" id="user_name">
										</div>
									</div>
									<div class="user-block">
										<div class="user-block__title">Альтернативна<br>назва</div>
										<div class="user-block__content">
											<input type="text" id="user_name">
										</div>
									</div>
									<div class="user-block user-block__content_code">
										<div class="user-block__title">Реєстраційний код (ЕРДПУ або ІПН)</div>
										<div class="user-block__content">
											<input type="text" id="user_name">
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="clear_kry"></div>
						<div class="company_date_info col-md-12 col-sm-12 col-xs-12">
							<div class="user_info_wrapp">
								<div class="user-block">
									<div class="user-block__title">Рік заснування</div>									
									<div class="user-block__content user-block__content_year">
										<select name="" class="user_info_company_workplace_select" data-placeholder="Оберіть рік заснування">
											<option></option>
											<?php
											for ($i = 2019; $i > 1900; $i--)
											{
												if ($i == 2019)
												{
													echo '<option value="1993" disabled selected style="color:#bababa;">1993</oplion>';
												}
												else
												{
													echo '<option value="'.$i.'">'.$i.'</oplion>';
												}
											}
											?>
										</select>
									</div>
								</div>
								<div class="user-block company_info_number_employers">
									<div class="user-block__title">Кількість працівників</div>									
									<div class="user-block__content user-block__number_employers">
										<select name="" class="user_info_company_workplace_select" data-placeholder="">
											<option></option>
											<option value="100-500">100-500</option>
											<option value="1-5 тис">1-5 тис</option>
											<option value="5-10 тис">5-10 тис</option>
										</select>
										<span class="after_title">осіб</span>
									</div>
								</div>	
								<div class="user-block">
									<div class="user-block__title">Форма власності</div>									
									<div class="user-block__content user-block__content_forma">
										<select name="" class="user_info_company_workplace_select" data-placeholder="Оберіть форму власності">
											<option></option>
											<option value="Закрите акціонерне товариство">Закрите акціонерне товариство</option>
											<option value="Фізична особа підприємець">Фізична особа підприємець</option>
											<option value="Приватне підприємство">Приватне підприємство</option>
											<option value="Відкрите акціонерне товариство">Відкрите акціонерне товариство</option>
											<option value="Державне підприємство">Державне підприємство</option>
										</select>
									</div>
								</div>
								<div class="user-block">
									<div class="user-block__title">Річний оборот</div>									
									<div class="user-block__content company_info_full_turnover">
										<select name="" data-placeholder="1-2">
											<option></option>
											<option value="1-2">1-2</option>
											<option value="3-4">5-10</option>
											<option value="5-10">5-10</option>
										</select>
										<span class="after_title">млн. грн</span>
									</div>
								</div>	
								<div class="user-block">
									<div class="user-block__title">Доля експорту у продажах</div>									
									<div class="user-block__content company_info_full_turnover turnover_percent">
										<input type="text">
										<span class="after_title">%</span>
									</div>
								</div>						
							</div>
						</div>
						<div class="clear_kry"></div>
						<div class="company_services_block col-md-12 col-sm-12 col-xs-12">	
							<div class="user_info_block_title">Послуги</div>
							<div class="company_services_block_subtitle">вкажіть спеціалізацію компанії, обравши<br/> відповідні класи КВЕД</div>
							<!-- Если страница компании заполнена -->
							<div class="clear_kry"></div>
							<img class="pluss_icon" src="<?php print get_stylesheet_directory_uri(); ?>/images/add.svg" alt=""><span class="add_info_company_services">Додати КВЕД</span>							
						</div>
						<div class="clear_kry"></div>
						<div class="company_stuffs_block col-md-12 col-sm-12 col-xs-12">	
							<div class="user_info_block_title">Товари</div>
							<div class="company_stuffs_block_subtitle">Вкажить HS класи товарів, які виробляє компанія</div>
							<!-- Если страница компании заполнена -->
							<div class="clear_kry"></div>
							<img class="pluss_icon" src="<?php print get_stylesheet_directory_uri(); ?>/images/add.svg" alt=""><span class="add_info_company_stuffs">Додати товари</span>							
						</div>	
						<div class="overflov_for_search"></div>
						<div class="clear_kry"></div>					
						<div class="user_feedback_block col-md-12 col-sm-12 col-xs-12">	
							<div class="user_info_block_title">Засоби зв'язку</div>
							<div class="about_user_blocks">
								<div class="user_info_wrapp">
									<div class="user_info_email_blocks">
										<div class="user-block__title">Email</div>									
										<div class="user-block__content user_info_feedback_input">
											<div class="inputs_blocks"></div>
											<img class="pluss_icon" src="<?php print get_stylesheet_directory_uri(); ?>/images/add.svg" alt="">
											<span class="text_after_plus_icon">Додати e-mail</span>
										</div>
									</div>
									<div class="user_info_phone_blocks col-md-12 col-sm-12 col-xs-12">
										<div class="user-block__title">Телефон</div>
										<div class="user-block__content user_info_feedback_input">
											<div class="inputs_blocks"></div>
											<img class="pluss_icon" src="<?php print get_stylesheet_directory_uri(); ?>/images/add.svg" alt="">
											<span class="text_after_plus_icon">Додати телефон</span>
										</div>
									</div>
									<div class="user_info_work_phone_blocks col-md-12 col-sm-12 col-xs-12">
										<div class="user-block__title">Сайт</div>
										<div class="user-block__content user_info_feedback_input">
											<div class="inputs_blocks"></div>
											<img class="pluss_icon" src="<?php print get_stylesheet_directory_uri(); ?>/images/add.svg" alt="">
											<span class="text_after_plus_icon">Додати сайт</span>
										</div>
									</div>
								</div>
								<div class="user_info_submit_block col-md-12 col-sm-12 col-xs-12">
									<div class="col-md-9 col-sm-9 col-xs-8"></div>
									<div class="col-md-3 col-sm-3 col-xs-4">
										<input type="submit" value="Зберегти зміни">
									</div>
								</div>
							</div>
						</div>					
						<div class="fill_block_white"></div>
						<!-- Убрать на продакшене -->
						<div class="clear_kry"></div>
						<div class="company_page_full">Если страница компании заполнена</div>
						<div class="empty_user_dashboard">Если страница пользователя пустая</div>
						<div class="clear_kry"></div>
					</div>
					<div class="user_company_info col-md-3  col-sm-12 col-xs-12">
						<div class="overlay_companies"></div>
						<div class="clear_kry"></div>
						<div class="company_info_box">
							<div class="company_info_box_title">
								Запросіть своїх колег, щоб мати змогу запрошувати їх на події та дозволяти їм оновлювати інформацію
							</div>
							<div class="company_inputs_block">
								<label for="">П.І.Б.</label>
								<input type="text" placeholder="напр. Сергій Петренко">
								<label for="">E-mail</label>
								<input type="text">
							</div>
							<input type="button" class="add_colleague_button" value="Надіслати запрошення">
						</div>	
						<!--  Если пользователь залогиненый -->	
						<div class="logged_company">
							<div class="company_info_box col-md-12 col-sm-12 col-xs-12">								
								<a href="<?php echo home_url();?>/epo_profile/colleague/" class="edit_this_worker">
									<img src="<?php print get_stylesheet_directory_uri(); ?>/images/pen.png" alt="">
								</a>
								<div class='remove_this_worker'>
									<img src="<?php print get_stylesheet_directory_uri(); ?>/images/thrash.svg" alt="">
								</div>								
								<div class="logged_worker_name_wrapp">
									<span class="logged_worker_name">Валентина Іванівна</span>
									<span class="logged_worker_position">Експортний менеджер</span>
									<span class="logged_worker_phone">+38 (123) 456-78-90</span>
									<span class="logged_worker_email">val@credi-agricole.ua</span>									
								</div>
								<div class="clear_kry"></div>								
								<!--<div class="popup_delet_worker">							
									<div class="change_pass_popup_head">
										<span>Ви впевнені що хочете видалити цього працівника?</span>
										<img src="<?php print get_stylesheet_directory_uri(); ?>/images/close_button.svg" alt="">
									</div>	
									<span class="confirm_delete_worker_aside col-md-5 col-sm-5 col-xs-5">Так</span><span class="dont_delete_worker_aside col-md-5 col-sm-5 col-xs-5">Ні</span>
								</div>
								<div class="overflow_for_delet_aside_elem"></div>-->
								<div class="delete_company_confirm">
									<span>Ви впевнені що хочете видалити цього працівника?</span>
									<input class="confirm_delet_company" type="button" value="Так, видалити"><input class="dont_delete_company" type="button" value="Ні">
								</div>
							</div>	
							<div class="clear_kry"></div>
							<div class="company_info_box col-md-12 col-sm-12 col-xs-12">								
								<a href="<?php echo home_url();?>/epo_profile/colleague/" class="edit_this_worker">
									<img src="<?php print get_stylesheet_directory_uri(); ?>/images/pen.png" alt="">
								</a>
								<div class='remove_this_worker'>
									<img src="<?php print get_stylesheet_directory_uri(); ?>/images/thrash.svg" alt="">
								</div>								
								<div class="logged_worker_name_wrapp">
									<span class="logged_worker_name">Катерина Петрівна</span>
									<span class="logged_worker_position"></span>
									<span class="logged_worker_phone">+38 (123) 456-78-90</span>
									<span class="logged_worker_email">cat@credi-agricole.ua</span>									
								</div>
								<div class="clear_kry"></div>								
								<!--<div class="popup_delet_worker">							
									<div class="change_pass_popup_head">
										<span>Ви впевнені що хочете видалити цього працівника?</span>
										<img src="<?php print get_stylesheet_directory_uri(); ?>/images/close_button.svg" alt="">
									</div>	
									<span class="confirm_delete_worker_aside col-md-5 col-sm-5 col-xs-5">Так</span><span class="dont_delete_worker_aside col-md-5 col-sm-5 col-xs-5">Ні</span>
								</div>	
								<div class="overflow_for_delet_aside_elem"></div>	-->	
								<div class="delete_company_confirm">
									<span>Ви впевнені що хочете видалити цього працівника?</span>
									<input class="confirm_delet_company" type="button" value="Так, видалити"><input class="dont_delete_company" type="button" value="Ні">
								</div>		
							</div>
							<div class="clear_kry"></div>
							<div class="company_info_box col-md-12 col-sm-12 col-xs-12">								
								<a href="<?php echo home_url();?>/epo_profile/colleague/" class="edit_this_worker">
									<img src="<?php print get_stylesheet_directory_uri(); ?>/images/pen.png" alt="">
								</a>
								<div class='remove_this_worker'>
									<img src="<?php print get_stylesheet_directory_uri(); ?>/images/thrash.svg" alt="">
								</div>								
								<div class="logged_worker_name_wrapp">
									<span class="logged_worker_name">Катя</span>
									<span class="logged_worker_position">Асистент начальника експортного відділу</span>
									<span class="logged_worker_phone"></span>
									<span class="logged_worker_email">cat@credi-agricole.ua</span>									
								</div>
								<div class="clear_kry"></div>								
								<!--<div class="popup_delet_company">							
									<div class="change_pass_popup_head">
										<span>Ви впевнені що хочете видалити цього працівника?</span>
										<img src="<?php print get_stylesheet_directory_uri(); ?>/images/close_button.svg" alt="">
									</div>	
									<span class="confirm_delete_worker_aside col-md-5 col-sm-5 col-xs-5">Так</span><span class="dont_delete_worker_aside col-md-5 col-sm-5 col-xs-5">Ні</span>
								</div>
								<div class="overflow_for_delet_aside_elem"></div>	-->		
								<div class="delete_company_confirm">
									<span>Ви впевнені що хочете видалити цього працівника?</span>
									<input class="confirm_delet_company" type="button" value="Так, видалити"><input class="dont_delete_company" type="button" value="Ні">
								</div>		
							</div>
							<div class="clear_kry"></div>						
						</div>						
					</div>
					<div class="fill_block"></div>
				</div>
			</form>
			<div class="fill_block_bottom"></div>
		</div>
		<div class="clear"></div>
	</div>
	<style>
		.header{
			position: fixed;
		}

		.content-left-wrap{
			padding-top: 0;
		}

		footer{
			margin-top: 0;
		}

		@media only screen and (max-width: 767px){
			#footer {
				padding-top: 0;
				margin-top: 0px;
			}
		}
	</style>
<?php get_footer();  ?>