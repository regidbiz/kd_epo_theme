<?php 
if(!is_user_logged_in()){
	wp_redirect(get_home_url(), 302);
}
get_header();  
?>
<div class="clear"></div>
</header>
<div id="content" class="site-content">

	<div class="container">

		<div class="content-left-wrap services_page_main_wrapp col-md-12">			
			<div class="user_events_blocks col-md-9 col-sm-12 col-xs-12">
				<div class="event_block">
					<a href="<?php echo home_url();?>/epo_profile/dashboard/">
						<div class="event_block_title">
							<p>Особиста інформація</p><img src="<?php print get_stylesheet_directory_uri(); ?>/images/arr_right.svg" alt="">
						</div>
						<div class="clear_kry"></div>
						<div class="event_block_desc">
							Редагуйте інформацію про себе та свою компанію
						</div>
					</a>
				</div>
				<div class="event_block active_event_block">
					<a href="#">
						<div class="event_block_title">
							<p>Послуги</p><img src="<?php print get_stylesheet_directory_uri(); ?>/images/arr_right.svg" alt="">
						</div>
						<div class="clear_kry"></div>
						<div class="event_block_desc">
							Отримайте оцінку свого бізнесу та коментарі від експертів
						</div>
					</a>
				</div>
				<div class="event_block">
					<a href="<?php echo home_url();?>/epo_profile/events_for_user/">
						<div class="event_block_title">
							<p>Події</p><img src="<?php print get_stylesheet_directory_uri(); ?>/images/arr_right.svg" alt="">
						</div>
						<div class="clear_kry"></div>
						<div class="event_block_desc">
							Найактуальніші події та нетворкінг для експортерів
						</div>
					</a>
				</div>
				<hr>
			</div>
			<div class="clear_kry"></div>
			<div class="services_page_title col-md-9 col-sm-12 col-xs-12">
				Послуги
				<div class="fill_block_top"></div>
			</div>
			<div class="clear_kry"></div>
			<div class="services_list_wrapp col-md-9 col-sm-12 col-xs-12">
				<div class="service_list_item">
					<div class="service_list_item_title">Оцінка готовності</div>
					<div class="service_list_item_desc col-md-12 col-sm-12 col-xs-12">
						<p>Оцініть свою готовність до зовнішньоекономічної діяльності.</p>
						<p>Визначте компетенції, яких вам бракує для досягнення успіху.</p>
					</div>
					<div class="clear_kry"></div>
					<a href="<?php echo home_url();?>/epo_profile/readliness_assessment_page/" class="link_on_single_service_page">Перейти</a>
				</div>
				<div class="service_list_item">
					<div class="service_list_item_title">Перевірка ідеї</div>
					<div class="service_list_item_desc col-md-12 col-sm-12 col-xs-12"><p>Консультації компаній, які мають ідеї щодо виходу на зовнішні ринки.</p></div>
					<div class="clear_kry"></div>
					<a href="<?php echo home_url();?>/epo_profile/checking_idea_page/" class="link_on_single_service_page">Перейти</a>
				</div>
				<div class="service_list_item">
					<div class="service_list_item_title">Менторство</div>
					<div class="service_list_item_desc col-md-12 col-sm-12 col-xs-12"><p>Отримайте допомогу у розробці плану інтернаціоналізації вашого бізнесу.</p></div>
					<div class="clear_kry"></div>
					<a href="<?php echo home_url();?>/epo_profile/mentorstvo_page/" class="link_on_single_service_page">Перейти</a>
				</div>
				<div class="service_list_item">
					<div class="service_list_item_title">Інтернаціоналізація бізнесу</div>
					<div class="service_list_item_desc col-md-12 col-sm-12 col-xs-12"><p>Зв'язок з фахівцями, які зможуть надати професійну допомогу із пошуку партнерів та аналізу ринку.</p></div>
					<div class="clear_kry"></div>
					<a href="<?php echo home_url();?>/epo_profile/internationalization_bussiness_page/" class="link_on_single_service_page">Перейти</a>
				</div>
				<div class="service_list_item">
					<div class="service_list_item_title">Soursing</div>
					<div class="service_list_item_desc col-md-12 col-sm-12 col-xs-12"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p></div>
					<div class="clear_kry"></div>
					<a href="<?php echo home_url();?>/epo_profile/sourcing_service_page/" class="link_on_single_service_page">Перейти</a>
				</div>
				<div class="service_list_item">
					<div class="service_list_item_title">Enterprise Europe Network</div>
					<div class="service_list_item_desc col-md-12 col-sm-12 col-xs-12"><p>EEN інформує про можливості розвитку в певній сфері, допомагає в залученні інвестицій для виробництва товарів і послуг, залученні нових технологій для підвищення конкурентноспроможності українських товаровиробників та підприємств, які надають послуги розвитку технологій, які були розроблені в Україні за допомогою закордонних партнерів.</p></div>
					<div class="clear_kry"></div>
					<a href="<?php echo home_url();?>/epo_profile/een_page/" class="link_on_single_service_page">Перейти</a>
				</div>
				<div class="fill_block"></div>
				<div class="fill_block_white"></div>
			</div>
			<div class="user_company_info services_page_company_aside col-md-3  col-sm-12 col-xs-12">
				<div class="user_info_block_title">Мої компанії</div>
    				<div class="overlay_companies"></div>
				<div class="clear_kry"></div>
				<div class="company_info_box">
					<div class="company_info_box_title">
						Вкажіть дані вашої компанії, щоб отримати доступ до послуг:
					</div>
					<div class="company_inputs_block">
						<label for="">Найменування юридичної особи</label>
						<input type="text" placeholder="напр. Ексім-меблі 3000">
						<label for="">Реєстраційний код (ЄРДПОУ або ІПН)</label>
						<input type="text">
						<label for="">Ваша посада</label>
						<input type="text">
						<label for="">Повна назва посади</label>
						<input type="text">
					</div>
					<input type="button" class="add_company_button" value="Додати компанію">
				</div>	
				<!--  Если пользователь залогиненый -->	
				<div class="logged_user">
					<div class="company_info_box active_company_info_box col-md-12 col-sm-12 col-xs-12">
						<span class="company_side_line"></span>
						<div class='remove_this_company'>
							<img src="<?php print get_stylesheet_directory_uri(); ?>/images/thrash.svg" alt="">
						</div>
						<img src="<?php print get_stylesheet_directory_uri(); ?>/images/credi_logo.png" alt="" class="user_logged_company_img">
						<div class="logged_company_names_wrapp">
							<span class="logged_company_name">Агріколь</span>
							<span class="logged_company_reg_num">12345678</span>									
						</div>
						<div class="clear_kry"></div>
						<a href="<?php echo home_url();?>/epo_profile/company/">Оновити інформацію</a>
						<!--<div class="popup_delet_company">							
							<div class="change_pass_popup_head">
								<span>Ви впевнені що хочете прибрати цю компанію?</span>
								<img src="<?php print get_stylesheet_directory_uri(); ?>/images/close_button.svg" alt="">
							</div>	
							<span class="col-md-5 col-sm-5 col-xs-5">Так</span><span class="col-md-5 col-sm-5 col-xs-5">Ні</span>
						</div>-->
						<div class="delete_company_confirm">
							<span>Ви дійсно хочете видалити цю компанію?</span>
							<input class="confirm_delet_company" type="button" value="Так, видалити"><input class="dont_delete_company" type="button" value="Ні">
						</div>
					</div>	
					<div class="clear_kry"></div>
					<div class="company_info_box col-md-12 col-sm-12 col-xs-12">
						<span class="company_side_line"></span>
						<div class='remove_this_company'>
							<img src="<?php print get_stylesheet_directory_uri(); ?>/images/thrash.svg" alt="">
						</div>
						<img src="<?php print get_stylesheet_directory_uri(); ?>/images/nothumb.png" alt="" class="user_logged_company_img">
						<div class="logged_company_names_wrapp">
							<span class="logged_company_name">ФОП Носов Дмитро Андрійович</span>
							<span class="logged_company_reg_num">12345678</span>
						</div>	
						<div class="clear_kry"></div>
						<a href="">Оновити інформацію</a>	
						<!--<div class="popup_delet_company">							
							<div class="change_pass_popup_head">
								<span>Ви впевнені що хочете прибрати цю компанію?</span>
								<img src="<?php print get_stylesheet_directory_uri(); ?>/images/close_button.svg" alt="">
							</div>	
							<span class="col-md-5 col-sm-5 col-xs-5">Так</span><span class="col-md-5 col-sm-5 col-xs-5">Ні</span>
						</div>	-->		
						<div class="delete_company_confirm">
							<span>Ви дійсно хочете видалити цю компанію?</span>
							<input class="confirm_delet_company" type="button" value="Так, видалити"><input class="dont_delete_company" type="button" value="Ні">
						</div>			
					</div>
					<div class="clear_kry"></div>
					<div class="logged_user_add_company_button">
						<img src="<?php print get_stylesheet_directory_uri(); ?>/images/add_company_button.png" alt="">Додати компанію
					</div>
				</div>							
			</div>			
		</div>
		<div class="fill_block_bottom"></div>
		<div class="clear"></div>
	</div>
	<style>
		.header{
			position: fixed;
		}

		.content-left-wrap{
			padding-top: 0;
		}

		footer{
			margin-top: 0;
		}

		@media only screen and (max-width: 767px){
			#footer {
				padding-top: 0;
				margin-top: 0px;
			}
		}
	</style>
<?php get_footer();  ?>