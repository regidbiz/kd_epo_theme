<?php 
if(is_user_logged_in()){
	wp_redirect(get_home_url() . '/epo_profile/dashboard/', 302);
}
if(isset($_POST['key']) && $_POST['key'] != ''){
	$key = $_POST['key'];
}else{
	$key = false;
}
if(isset($_POST['uid']) && $_POST['uid'] != ''){
	$uid = $_POST['uid'];
}else{
	$uid = false;
}
if(isset($_POST['pass_1']) && $_POST['pass_1'] != ''){
	$pass_1 = $_POST['pass_1'];
}else{
	$pass_1 = false;
}
if(isset($_POST['pass_2']) && $_POST['pass_2'] != ''){
	$pass_2 = $_POST['pass_2'];
}else{
	$pass_2 = false;
}
$error = false;
if($key && $uid && $pass_1 && $pass_2){
	if($pass_1 == $pass_2){
		$key_u = get_user_meta( $uid, 'has_to_be_reset', true );
		if($key_u == $key){
			$user = get_user_by( 'ID', $uid ); 
			if($user){
				delete_user_meta( $uid, 'has_to_be_reset');
				wp_set_password( $pass_1, $uid );
				wp_set_current_user( $uid, $user->user_login );
				wp_set_auth_cookie( $uid );
				do_action( 'wp_login', $user->user_login );	
				wp_redirect(get_home_url() . '/epo_profile/dashboard/', 302);
			}else{
				$error = 'Такий користувач вже не існує ...';
			}
		}else{
			$error = 'Ви намагаєтесь відновити доступ до чужого аккаунта ...';
		}
	}else{
		$error = 'Паролі не співпадають. Спробуйте ввести ще раз ...';
	}
}
get_header();  
?>
<div class="clear"></div>
</header>
<div class="overflow_events_kry"></div>
<div id="content" class="site-content">
	<div class="container">
		<div class="content-left-wrap thanks_page col-md-12">
			<div class="thanks_block_wrapp">
				<?php
				$key = isset($_GET['key']) ? $_GET['key'] : false; 
				$user_id = isset($_GET['user']) ? intval($_GET['user']) : false; 
				if($key && $user_id){
					$user = get_user_by("ID",$user_id);
					if($user){
						$key_u = get_user_meta( $user_id, 'has_to_be_reset', true );
						if($key_u == $key){
							?>
							<img class="thanks_block_image_ok" src="<?php print get_stylesheet_directory_uri(); ?>/images/ok_shevron.svg" alt="">
							<div class="thanks_block_text_ok">Новий пароль для доступу до кабінету!</div>
							<?php if($error){ ?>
								<div class="thanks_block_text_ok"><?php print $error; ?></div>
							<?php } ?>
							<div class="thanks_block_text_ok" style="text-align: center;">
								<form action="" class="thanks_block_password_creator" method="post">
									<input type="hidden" name="key" value="<?php print $key; ?>">
									<input type="hidden" name="uid" value="<?php print $user_id; ?>">
									<table border="0" style="max-width: 400px; text-align: left; margin: 0 auto; border: 0; background: #e8ecf3;">
										<tr><td colspan="2" style="border: 0;"></tr>
										<tr><td colspan="2" style="border: 0;"></tr>
										<tr>
											<td style="border: 0; padding: 5px 15px;">Пароль:</td>
											<td style="border: 0; padding: 5px 15px;"><input type="password" name="pass_1" class="thanks_block_password_creator_pass_1" required></td>
										</tr>
										<tr><td colspan="2" style="border: 0;"></tr>
										<tr>
											<td style="border: 0; padding: 5px 15px;">Повторіть пароль:</td>
											<td style="border: 0; padding: 5px 15px;"><input type="password" name="pass_2" class="thanks_block_password_creator_pass_2" required></td>
										</tr>
										<tr><td colspan="2" style="border: 0;"></tr>
										<tr>
											<td colspan="2" style="text-align: center; border: 0; padding: 5px 15px;">
												<button>Відновити доступ</button>
											</td>
										</tr>
										<tr><td colspan="2" style="border: 0;"></tr>
									</table>
								</form>
							</div>
							<?php						
						}else{
							?>
							<img class="thanks_block_image_ok" src="<?php print get_stylesheet_directory_uri(); ?>/images/error_pic.png" alt="">
							<div class="thanks_block_text_ok">Помилка при відновленні доступу до аккаунта!</div>
							<div class="thanks_block_text_ok">Ключ відновлення доступу не співпадає ...</div>			
							<?php								
						}
					}else{
						?>
						<img class="thanks_block_image_ok" src="<?php print get_stylesheet_directory_uri(); ?>/images/error_pic.png" alt="">
						<div class="thanks_block_text_ok">Помилка при відновленні доступу до аккаунта!</div>
						<div class="thanks_block_text_ok">Ви намагаєтесь відновити доступ для неіснуючого користувача ...</div>			
						<?php						
					}
				}else{
					?>
					<img class="thanks_block_image_ok" src="<?php print get_stylesheet_directory_uri(); ?>/images/error_pic.png" alt="">
					<div class="thanks_block_text_ok">Помилка при відновленні доступу до аккаунта!</div>
					<div class="thanks_block_text_ok">Ви намагаєтесь відновити доступ під неіснуючим ключем або для неіснуючого користувача ...</div>			
					<?php					
				}
				?>
			</div>
		</div>
		<div class="clear_kry"></div>
	</div>
	<div class="clear"></div>
</div>
<style>
.header{
	position: fixed;
}

.content-left-wrap{
	padding-top: 0;
}
</style>
<?php get_footer();  ?>