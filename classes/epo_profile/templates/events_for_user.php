<?php 
if(!is_user_logged_in()){
	wp_redirect(get_home_url(), 302);
}
get_header();  
?>
<div class="clear"></div>
</header>
<div class="overflow_events_kry"></div>
<div id="content" class="site-content">
	<div class="container">
		<div class="content-left-wrap col-md-12">
			<div class="user_events_blocks col-md-9 col-sm-12 col-xs-12">
				<div class="event_block">
					<a href="<?php echo home_url();?>/epo_profile/dashboard/">
						<div class="event_block_title">
							<p>Особиста інформація</p><img src="<?php print get_stylesheet_directory_uri(); ?>/images/arr_right.svg" alt="">
						</div>
						<div class="clear_kry"></div>
						<div class="event_block_desc">
							Редагуйте інформацію про себе та свою компанію
						</div>
					</a>
				</div>
				<div class="event_block">
					<a href="<?php echo home_url();?>/epo_profile/services_list/">
						<div class="event_block_title">
							<p>Послуги</p><img src="<?php print get_stylesheet_directory_uri(); ?>/images/arr_right.svg" alt="">
						</div>
						<div class="clear_kry"></div>
						<div class="event_block_desc">
							Отримайте оцінку свого бізнесу та коментарі від експертів
						</div>
					</a>
				</div>
				<div class="event_block active_event_block">
					<a href="#">
						<div class="event_block_title">
							<p>Події</p><img src="<?php print get_stylesheet_directory_uri(); ?>/images/arr_right.svg" alt="">
						</div>
						<div class="clear_kry"></div>
						<div class="event_block_desc">
							Найактуальніші події та нетворкінг для експортерів
						</div>
					</a>
				</div>
				<hr>
			</div>		
			<div class="clear_kry"></div>
			<div class="filter_items_wrapp col-md-offset-1">
				<div class="filter_item date">
					<span>Дата</span> 
					<img src="<?php print get_stylesheet_directory_uri(); ?>/images/close.svg" alt="">
					<div class="filter_item_chosen_block">
						<div class="filter_chosen_item">
							<span>
								<input type="checkbox" />		
							</span>
							<label>Лютий 2018</label>							
						</div>
						<div class="filter_chosen_item">
							<span>
								<input type="checkbox" />		
							</span>
							<label>Березень 2018</label>							
						</div>
						<div class="filter_chosen_item">
							<span>
								<input type="checkbox" />		
							</span>
							<label>Квітень 2018</label>							
						</div>
						<div class="filter_chosen_item">
							<span>
								<input type="checkbox" />		
							</span>
							<label>Травень 2018</label>
						</div>
						<div class="filter_chosen_item">
							<span>
								<input type="checkbox" />		
							</span>
							<label>Вересень 2018</label>
						</div>
						<div class="confirm_chose">
							<span>Дивитись події</span>
						</div>
					</div>
				</div>
				<div class="filter_item types">
					<span>Усі типи</span> 
					<img src="<?php print get_stylesheet_directory_uri(); ?>/images/close.svg" alt="">					
					<div class="filter_item_chosen_block">
						<div class="filter_chosen_item">
							<span>
								<input type="checkbox" />		
							</span>
							<label>Усі типи</label>							
						</div>
						<div class="filter_chosen_item">
							<span>
								<input type="checkbox" />		
							</span>
							<label>Лише виставки</label>							
						</div>
						<div class="filter_chosen_item">
							<span>
								<input type="checkbox" />		
							</span>
							<label>Лише торгові місії</label>							
						</div>
						<div class="confirm_chose">
							<span>Дивитись події</span>
						</div>
					</div>					
				</div>
				<div class="filter_item reg_status">
					<span>Статус реєстрації</span> 
					<img src="<?php print get_stylesheet_directory_uri(); ?>/images/close.svg" alt="">
					<div class="filter_item_chosen_block">
						<div class="filter_chosen_item">
							<span>
								<input type="checkbox" />		
							</span>
							<label>Усі статуси</label>							
						</div>
						<div class="filter_chosen_item">
							<span>
								<input type="checkbox" />		
							</span>
							<label>Лише з відкритою реєстрацією</label>							
						</div>
						
						<div class="confirm_chose">
							<span>Дивитись події</span>
						</div>
					</div>
				</div>
				<hr>
			</div>
			<div class="clear_kry"></div>
			<div class="events_list_wrapp col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
				<div class="event_list_item col-md-12 col-sm-12 col-xs-12">
					<div class="event_title_tag_wrapp col-md-8 col-sm-8 col-xs-12">
						<div class="event_tag col-md-12 col-sm-12 col-xs-12"><span>Виставки</span></div>
						<div class="clear_kry"></div>
						<div class="event_title col-md-12 col-sm-12 col-xs-12">						
							<a href="#">Виставка Fruit Logistica 2018</a>
						</div>
					</div>
					<div class="date_reg_event_info_wrapp col-md-4 col-sm-4 col-xs-12">
						<div class="event_begin col-md-6 col-sm-6 col-xs-6">Початок</div><div class="event_end col-md-6 col-sm-6 col-xs-6">Кінець</div>
						<div class="time_block_part time_block_part_one col-md-6 col-sm-6 col-xs-6"><span>07</span>лютого 2018</div><div class="time_block_part col-md-6 col-sm-6 col-xs-6"><span>09</span>лютого 2018</div>
						<div class="events_reg_status event_reg_closed col-md-12 col-sm-12 col-xs-12">Реєстрація закрита</div>
					</div>
				</div>
				<div class="event_list_item col-md-12 col-sm-12 col-xs-12">
					<div class="event_title_tag_wrapp col-md-8 col-sm-8 col-xs-12">
						<div class="event_tag col-md-12 col-sm-12 col-xs-12"><span>Виставки</span></div>
						<div class="clear_kry"></div>
						<div class="event_title col-md-12 col-sm-12 col-xs-12">						
							<a href="#">Виставка Fruit Logistica 2018</a>
						</div>
					</div>
					<div class="date_reg_event_info_wrapp col-md-4 col-sm-4 col-xs-12">
						<div class="event_begin col-md-6 col-sm-6 col-xs-6">Початок</div><div class="event_end col-md-6 col-sm-6 col-xs-6">Кінець</div>
						<div class="time_block_part time_block_part_one col-md-6 col-sm-6 col-xs-6"><span>07</span>лютого 2018</div><div class="time_block_part col-md-6 col-sm-6 col-xs-6"><span>09</span>лютого 2018</div>
						<div class="events_reg_status event_reg_closed col-md-12 col-sm-12 col-xs-12">Реєстрація закрита</div>
					</div>
				</div>
				<div class="event_list_item col-md-12 col-sm-12 col-xs-12">
					<div class="event_title_tag_wrapp col-md-8 col-sm-8 col-xs-12">
						<div class="event_tag col-md-12 col-sm-12 col-xs-12"><span>Виставки</span></div>
						<div class="clear_kry"></div>
						<div class="event_title col-md-12 col-sm-12 col-xs-12">						
							<a href="#">Виставка Fruit Logistica 2018</a>
						</div>
					</div>
					<div class="date_reg_event_info_wrapp col-md-4 col-sm-4 col-xs-12">
						<div class="event_begin col-md-6 col-sm-6 col-xs-6">Початок</div><div class="event_end col-md-6 col-sm-6 col-xs-6">Кінець</div>
						<div class="time_block_part time_block_part_one col-md-6 col-sm-6 col-xs-6"><span>07</span>лютого 2018</div><div class="time_block_part col-md-6 col-sm-6 col-xs-6"><span>09</span>лютого 2018</div>
						<div class="events_reg_status event_reg_closed col-md-12 col-sm-12 col-xs-12">Реєстрація закрита</div>
					</div>
				</div>
				<div class="event_list_item col-md-12 col-sm-12 col-xs-12">
					<div class="event_title_tag_wrapp col-md-8 col-sm-8 col-xs-12">
						<div class="event_tag col-md-12 col-sm-12 col-xs-12"><span>Виставки</span></div>
						<div class="clear_kry"></div>
						<div class="event_title col-md-12 col-sm-12 col-xs-12">						
							<a href="#">Виставка Fruit Logistica 2018</a>
						</div>
					</div>
					<div class="date_reg_event_info_wrapp col-md-4 col-sm-4 col-xs-12">
						<div class="event_begin col-md-6 col-sm-6 col-xs-6">Початок</div><div class="event_end col-md-6 col-sm-6 col-xs-6">Кінець</div>
						<div class="time_block_part time_block_part_one col-md-6 col-sm-6 col-xs-6"><span>07</span>лютого 2018</div><div class="time_block_part col-md-6 col-sm-6 col-xs-6"><span>09</span>лютого 2018</div>
						<div class="events_reg_status event_reg_open col-md-12 col-sm-12 col-xs-12">Реєстрація відкрита</div>
					</div>
				</div>
				<div class="event_list_item col-md-12 col-sm-12 col-xs-12">
					<div class="event_title_tag_wrapp col-md-8 col-sm-8 col-xs-12">
						<div class="event_tag col-md-12 col-sm-12 col-xs-12"><span>Виставки</span></div>
						<div class="clear_kry"></div>
						<div class="event_title col-md-12 col-sm-12 col-xs-12">						
							<a href="#">Виставка Fruit Logistica 2018</a>
						</div>
					</div>
					<div class="date_reg_event_info_wrapp col-md-4 col-sm-4 col-xs-12">
						<div class="event_begin col-md-6 col-sm-6 col-xs-6">Початок</div><div class="event_end col-md-6 col-sm-6 col-xs-6">Кінець</div>
						<div class="time_block_part time_block_part_one col-md-6 col-sm-6 col-xs-6"><span>07</span>лютого 2018</div><div class="time_block_part col-md-6 col-sm-6 col-xs-6"><span>09</span>лютого 2018</div>
						<div class="events_reg_status event_reg_open col-md-12 col-sm-12 col-xs-12">Реєстрація відкрита</div>
					</div>
				</div>
				<div class="event_list_item col-md-12 col-sm-12 col-xs-12">
					<div class="event_title_tag_wrapp col-md-8 col-sm-8 col-xs-12">
						<div class="event_tag col-md-12 col-sm-12 col-xs-12"><span>Виставки</span></div>
						<div class="clear_kry"></div>
						<div class="event_title col-md-12 col-sm-12 col-xs-12">						
							<a href="#">Виставка Fruit Logistica 2018</a>
						</div>
					</div>
					<div class="date_reg_event_info_wrapp col-md-4 col-sm-4 col-xs-12">
						<div class="event_begin col-md-6 col-sm-6 col-xs-6">Початок</div><div class="event_end col-md-6 col-sm-6 col-xs-6">Кінець</div>
						<div class="time_block_part time_block_part_one col-md-6 col-sm-6 col-xs-6"><span>07</span>лютого 2018</div><div class="time_block_part col-md-6 col-sm-6 col-xs-6"><span>09</span>лютого 2018</div>
						<div class="events_reg_status event_reg_open col-md-12 col-sm-12 col-xs-12">Реєстрація відкрита</div>
					</div>
				</div>
				<div class="event_list_item col-md-12 col-sm-12 col-xs-12">
					<div class="event_title_tag_wrapp col-md-8 col-sm-8 col-xs-12">
						<div class="event_tag col-md-12 col-sm-12 col-xs-12"><span>Виставки</span><span>Торгові місії</span></div>
						<div class="clear_kry"></div>
						<div class="event_title col-md-12 col-sm-12 col-xs-12">						
							<a href="#">Виставка Fruit Logistica 2018</a>
						</div>
					</div>
					<div class="date_reg_event_info_wrapp col-md-4 col-sm-4 col-xs-12">
						<div class="event_begin col-md-6 col-sm-6 col-xs-6">Початок</div><div class="event_end col-md-6 col-sm-6 col-xs-6">Кінець</div>
						<div class="time_block_part time_block_part_one col-md-6 col-sm-6 col-xs-6"><span>07</span>лютого 2018</div><div class="time_block_part col-md-6 col-sm-6 col-xs-6"><span>09</span>лютого 2018</div>
						<div class="events_reg_status event_reg_open col-md-12 col-sm-12 col-xs-12">Реєстрація відкрита</div>
					</div>
				</div>
				<div class="event_list_item col-md-12 col-sm-12 col-xs-12">
					<div class="event_title_tag_wrapp col-md-8 col-sm-8 col-xs-12">
						<div class="event_tag col-md-12 col-sm-12 col-xs-12"><span>Виставки</span></div>
						<div class="clear_kry"></div>
						<div class="event_title col-md-12 col-sm-12 col-xs-12">						
							<a href="#">Виставка Fruit Logistica 2018</a>
						</div>
					</div>
					<div class="date_reg_event_info_wrapp col-md-4 col-sm-4 col-xs-12">
						<div class="event_begin col-md-6 col-sm-6 col-xs-6">Початок</div><div class="event_end col-md-6 col-sm-6 col-xs-6">Кінець</div>
						<div class="time_block_part time_block_part_one col-md-6 col-sm-6 col-xs-6"><span>07</span>лютого 2018</div><div class="time_block_part col-md-6 col-sm-6 col-xs-6"><span>09</span>лютого 2018</div>
						<div class="events_reg_status event_reg_open col-md-12 col-sm-12 col-xs-12">Реєстрація відкрита</div>
					</div>
				</div>
				<div class="load_more col-md-12 col-sm-12 col-xs-12">
					<i class="fa fa-repeat" aria-hidden="true"></i>
					ПОКАЗАТИ БІЛЬШЕ
				</div>	
				<div class="events_list_fill"></div>				
			</div>							
		</div>	
	</div>
	<div class="clear"></div>
</div>
<style>
		.header{
			position: fixed;
		}

		.content-left-wrap{
			padding-top: 0;
		}

		footer{
			margin-top: 0;
		}

		@media only screen and (max-width: 767px){
			#footer {
				padding-top: 0;
				margin-top: 0px;
			}
		}
</style>
<?php get_footer();  ?>