<?php 
if(!is_user_logged_in()){
	wp_redirect(get_home_url(), 302);
}
get_header();  
?>
<div class="clear"></div>
</header>
	<div class="content">
		<header class="company-header">
			<div class="container">
				<div class="header-container">
					<div class="company-header__logo"><img src="<?php print get_stylesheet_directory_uri(); ?>/images/logo-enzim.jpg" alt="Company Logo"></div>
					<div class="company-header__info">
						<div class="company-about">
							<span class="company-about__name">PrJSC Enzym Company</span>
							<span class="company-about__phone">123456789123</span>
						</div>
						<div class="company-location">Brody, Lviv oblast</div>
						<div class="company-website">
							<a href="#">http://enzym.lviv.ua</a>
						</div>
					</div>
					<div class="company-header__details">
						<ul class="company-list">
							<li>
								<span>10-50 mln UAH</span>
								<span>annual turnover</span>
							</li>
							<li>
								<span>5-20</span>
								<span>employers</span>
							</li>
							<li>
								<span>China, Canada, USA</span>
								<span>export markets</span>
							</li>
						</ul>	
					</div>
					<div class="company-header__print">
						<a href="#" class="print-button">Print page</a>
					</div>
				</div>
			</div>
		</header>
		<div class="company-content">
			<div class="container">
				<div class="company-wrapper">
					<section class="company-section">
						<h2 class="company-section__title">About the company</h2>
						<div class="company-section__body company-section__body_about">
							<p>PrJSC Enzym Company is a leading Ukranian manufacturer of yeast and yeast-based products wita a hundred years history and traditions, as well as widely diversified business.</p>

							<p>Our company pffers wide range of ueast-based products with the focus on biotechnological products - active dry yeasr (probiotic effect and possibility of antibiotic substitution) and inactive fodder yeast for animal feed.</p>

							<p>Following the tendercy of antibiotic reduction in animal feeding, we offer 100% natural, clean label solution for animals, mainly dairy cows and poultry.</p>
						</div>
						<div class="company-section__body company-section__body_card">
							<div class="company-card">
								<div class="company-card__header">
									<div class="company-card__avatar">
										<img src="<?php print get_stylesheet_directory_uri(); ?>/images/user_photo.jpg" alt="">
									</div>
									<div class="company-card__title">
										<div class="title-name">Dmytro Nosov</div>
										<div class="title-position">Finance director</div>
									</div>
								</div>
								<div class="company-card__body">
									<div class="card-title">Mobile phone</div>
									<div class="card-blocks">
										<div class="card-block"><span>+38 098 765-43-21</span></div>
										<div class="card-block"><span>+38 098 765-43-21</span></div>
									</div>
									<div class="card-title">Work phone</div>
									<div class="card-blocks">
										<div class="card-block"><span>+38 098 765-43-21</span></div>
									</div>
									<div class="card-title">Email</div>
									<div class="card-blocks">
										<div class="card-block"><a href="mailto:d.nosov@gmail.com">d.nosov@gmail.com</a></div>
									</div>
								</div>
								<div class="company-card__footer">
									<a href="javascript:void(0);" class="add-to-contacts">Add to contacts</a>
								</div>
							</div>
						</div>
					</section>
					<section class="company-section">
						<h2 class="company-section__title">Industries</h2>
						<div class="company-section__body company-section__body_products">
							<article class="product">
								<span class="product__info">8214029 - Banking services on long-term loans</span>
							</article>
							<article class="product">
								<span class="product__info">8214029 - Banking services on long-term loans</span>
							</article>
							<article class="product">
								<span class="product__info">8214029 - Banking services on long-term loans</span>
							</article>
							<article class="product">
								<span class="product__info">8214029 - Banking services on long-term loans</span>
							</article>
							<article class="product">
								<span class="product__info">8214029 - Banking services on long-term loans</span>
							</article>
						</div>
					</section>
					<section class="company-section">
						<h2 class="company-section__title">Products</h2>
						<div class="company-section__body company-section__body_products">
							<article class="product">
								<span class="product__info">8214029 - Banking services on long-term loans</span>
							</article>
							<article class="product">
								<span class="product__info">8214029 - Banking services on long-term loans</span>
							</article>
						</div>
					</section>
				</div>
			</div>
		</div>
	</div>
	<div class="clear"></div>
<style>
	.header{
		position: fixed;
	}
	.content-left-wrap{
		padding-top: 0;
	}
	body{
		background-color: #f4f4f4;
	}
	.container{
		padding: 0!important;
	}
</style>
<?php get_footer();  ?>