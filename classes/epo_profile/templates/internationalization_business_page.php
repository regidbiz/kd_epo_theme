<?php 
if(!is_user_logged_in()){
	wp_redirect(get_home_url(), 302);
}
get_header();  
?>
<div class="clear"></div>
</header>
<div class="overflow_events_kry"></div>
<div id="content" class="site-content">
	<div class="container">
		<div class="content-left-wrap col-md-12">
			<div class="overflow_kry"></div>		
			<div class="service_learn_more_popup">
				<img class="close_service_learn_more_popup" src="<?php print get_stylesheet_directory_uri(); ?>/images/close_button.svg" alt="">
				<div class="service_learn_more_popup_title">Інтернаціоналізація бізнесу</div>
				<div class="service_learn_more_popup_content">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus optio a nostrum ratione atque recusandae natus rerum necessitatibus dicta, quos. Minima aperiam quo quod fugiat dolor vero officiis et quasi!</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus optio a nostrum ratione atque recusandae natus rerum necessitatibus dicta, quos. Minima aperiam quo quod fugiat dolor vero officiis et quasi!</p>
					<ul>
						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
						<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
					</ul>
				</div>
			</div>	
			<div class="user_events_blocks col-md-9 col-sm-12 col-xs-12">
				<div class="event_block">
					<a href="<?php echo home_url();?>/epo_profile/dashboard/">
						<div class="event_block_title">
							<p>Особиста інформація</p><img src="<?php print get_stylesheet_directory_uri(); ?>/images/arr_right.svg" alt="">
						</div>
						<div class="clear_kry"></div>
						<div class="event_block_desc">
							Редагуйте інформацію про себе та свою компанію
						</div>
					</a>
				</div>
				<div class="event_block active_event_block">
					<a href="<?php echo home_url();?>/epo_profile/services_list/">
						<div class="event_block_title">
							<p>Послуги</p><img src="<?php print get_stylesheet_directory_uri(); ?>/images/arr_right.svg" alt="">
						</div>
						<div class="clear_kry"></div>
						<div class="event_block_desc">
							Отримайте оцінку свого бізнесу та коментарі від експертів
						</div>
					</a>
				</div>
				<div class="event_block">
					<a href="<?php echo home_url();?>/epo_profile/events_for_user/">
						<div class="event_block_title">
							<p>Події</p><img src="<?php print get_stylesheet_directory_uri(); ?>/images/arr_right.svg" alt="">
						</div>
						<div class="clear_kry"></div>
						<div class="event_block_desc">
							Найактуальніші події та нетворкінг для експортерів
						</div>
					</a>
				</div>
				<hr>
			</div>
			<div class="clear_kry"></div>
			<form action="" class="company_dash_block readiness_assessment_page">
				<div class="company_greeting_wrap col-md-9 col-sm-12 col-xs-12">	
					<div class="single_service_head col-md-8 col-sm-8 col-xs-12 nopadding">
						<div class="service_single_page_title">
							<a href="<?php echo home_url();?>/epo_profile/services_list/">
								<img src="<?php print get_stylesheet_directory_uri(); ?>/images/back_arrow.png" alt="">
							</a>
							<span>Інтернаціоналізація бізнесу</span>
						</div>
						<p class="service_single_page_description">Зв'язок з фахівцями, які зможуть надати професійну допомогу із пошуку партнерів та аналізу ринку.</p>
					</div>
					<div class="service_learn_more col-md-4 col-sm-4 col-xs-12">Детальніше про послугу</div>
					<div class="fill_block_top"></div>		
				</div>
				<div class="clear_kry"></div>
				<div class="main_info_about_user col-md-12 col-sm-12 col-xs-12">
					<div class="user_self_info_wrapp col-md-9 col-sm-12 col-xs-12">
						<div class="single_event_reg_step col-md-12 col-sm-12 col-xs-12">
							<div class="single_event_step_title">I. Оберіть контактну особу</div>
							<div class="single_event_participant open_selection_user_bloks_wrpp col-md-6 col-sm-8 col-xs-12">
								<div class="participant_photo_block" style='background-image: url("<?php print get_stylesheet_directory_uri(); ?>/images/user_photo.jpg ?>");background-size:cover; background-repeat:no-repeat; background-position: left center'></div>
								<div class="participant_name_block">Дмитро Носов</div>
								<div class="participant_position_block">Фінансовий директор</div>
								<div class="chevron_wrapp">
									<i class="fa fa-chevron-down" aria-hidden="true"></i>
								</div>
								<div class="selection_user_bloks_wrapp">
									<div class="user_block_in_selection">
										<div class="single_event_participant col-md-12 col-sm-12 col-xs-12">
											<div class="participant_photo_block" style='background-image: url("<?php print get_stylesheet_directory_uri(); ?>/images/default_user_image.svg ?>");background-size:cover; background-repeat:no-repeat; background-position: left center'></div>
											<div class="participant_name_block">Валентина Іванівна</div>
											<div class="participant_position_block">Експортний менеджер</div>
										</div>
									</div>
									<div class="user_block_in_selection">
										<div class="single_event_participant col-md-12 col-sm-12 col-xs-12">
											<div class="participant_photo_block" style='background-image: url("<?php print get_stylesheet_directory_uri(); ?>/images/default_user_image.svg ?>");background-size:cover; background-repeat:no-repeat; background-position: left center'></div>
											<div class="participant_name_block">Валентина Іванівна</div>
											<div class="participant_position_block">Експортний менеджер</div>
										</div>
									</div>
									<div class="user_block_in_selection">
										<div class="single_event_participant col-md-12 col-sm-12 col-xs-12">
											<div class="participant_photo_block" style='background-image: url("<?php print get_stylesheet_directory_uri(); ?>/images/default_user_image.svg ?>");background-size:cover; background-repeat:no-repeat; background-position: left center'></div>
											<div class="participant_name_block">Валентина Іванівна</div>
											<div class="participant_position_block">Експортний менеджер</div>
										</div>
									</div>
								</div>							
							</div>
						</div>
						<div class="single_event_reg_step_info col-md-12 col-sm-12 col-xs-12">
						<div class="user_info_block_title">II. Перевірте актуальність даних</div>

			
						<div class="single_event_participant_info col-md-12 col-sm-12 col-xs-12">
							<div class="user_info_wrapp user_info_wrapp_with_border user_info_wrapp_with_border_last">
								<div class="user-block">
									<div class="user-block__title">П.І.Б</div>
									<div class="user-block__content">
										<input type="text" id="user_name">
									</div>
								</div>
								<div class="user-block">
									<div class="user-block__title">Посада</div>									
									<div class="user-block__content">
									<!--<input type='text' class='col-md-12 col-sm-12 col-xs-12'>-->
										<select name="" class="user_info_company_workplace_select" data-placeholder="Оберіть посаду">
											<option disabled selected></option>
										<option value="Фiзисна особа пiдприємець">Член правління</option>
											<option value="Фінасновий директор">Фінасновий директор</option>
										<option value="Експортний менеджер">Експортний менеджер</option>
										</select>
									</div>
								</div>
								<div class="user-block">
									<div class="user-block__title">Повна назва посади</div>
									<div class="user-block__content">
										<input type="text">
									</div>
								</div>
								<div class="user-block user_info_email_blocks">
									<div class="user-block__title">Email <i class="info_icon fa fa-info" aria-hidden="true"></i></div>									
									<div class="user-block__content user_info_feedback_input">
										<div class="inputs_blocks">
											<div class="inputs_block_inner inputs_block_inner_email">
												<input type="email" class="col-md-12 col-sm-12 col-xs-12 email">
												<div class="remove_this_input"><img src='<?php print get_stylesheet_directory_uri(); ?>/images/thrash.svg' alt=''></div>
												<span class="setp_main_email active_feedback_info">Основний email</span>
											</div>
										</div>
										<img class="pluss_icon" src="<?php print get_stylesheet_directory_uri(); ?>/images/add.svg" alt="">
										<span class="text_after_plus_icon">Додати e-mail</span>
									</div>
								</div>
								<div class="user-block user_info_phone_blocks">
									<div class="user-block__title">Мобільний телефон</div>									
									<div class="user-block__content user_info_feedback_input">
										<div class="inputs_blocks">
											<div class="inputs_block_inner inputs_block_inner_phone">
												<input type="phone" class="col-md-12 col-sm-12 col-xs-12 phone">
												<div class="remove_this_input"><img src='<?php print get_stylesheet_directory_uri(); ?>/images/thrash.svg' alt=''></div>
												<span class="setp_main_phone">Встановити як основний</span>
											</div>
										</div>
										<img class="pluss_icon" src="<?php print get_stylesheet_directory_uri(); ?>/images/add.svg" alt="">
										<span class="text_after_plus_icon">Додати телефон</span>
									</div>
								</div>
								<div class="user-block user_info_work_phone_blocks">
									<div class="user-block__title">Робочий телефон</div>									
									<div class="user-block__content user_info_feedback_input">
										<div class="inputs_blocks"></div>
										<img class="pluss_icon" src="<?php print get_stylesheet_directory_uri(); ?>/images/add.svg" alt="">
										<span class="text_after_plus_icon">Додати телефон</span>
									</div>
								</div>
								<div class="info_save_icons col-md-12 col-sm-12 col-xs-12 nopadding">
									<i class="info_icon fa fa-info" aria-hidden="true"></i>
									<div class="save_event_canges"><i class="fa fa-floppy-o" aria-hidden="true"></i>Зберегти зміни</div>
								</div>
								<div class="clear_kry"></div>
							</div>
						</div>
					</div>					
						
						<div class="clear_kry"></div>
						<a href="<?php echo home_url();?>/epo_profile/thanks_order_service/" class="order_service_button col-md-5 col-sm-5 col-xs-12">Замовити послугу</a>
						<div class="clear_kry"></div>
						<div class="fill_block_white"></div>
					</div>
					<div class="user_company_info services_page_company_aside single_service_page_company_aside col-md-3  col-sm-12 col-xs-12">
						<div class="user_info_block_title">Мої компанії</div>
						<div class="overlay_companies"></div>
						<div class="clear_kry"></div>
						<div class="company_info_box">
							<div class="company_info_box_title">
								Вкажіть дані вашої компанії, щоб отримати доступ до послуг:
							</div>
							<div class="company_inputs_block">
								<label for="">Найменування юридичної особи</label>
								<input type="text" placeholder="напр. Ексім-меблі 3000">
								<label for="">Реєстраційний код (ЄРДПОУ або ІПН)</label>
								<input type="text">
								<label for="">Ваша посада</label>
								<input type="text">
								<label for="">Повна назва посади</label>
								<input type="text">
							</div>
							<input type="button" class="add_company_button" value="Додати компанію">
						</div>	
						<!--  Если пользователь залогиненый -->	
						<div class="logged_user">
							<div class="company_info_box active_company_info_box col-md-12 col-sm-12 col-xs-12">
								<span class="company_side_line"></span>
								<div class='remove_this_company'>
									<img src="<?php print get_stylesheet_directory_uri(); ?>/images/thrash.svg" alt="">
								</div>
								<img src="<?php print get_stylesheet_directory_uri(); ?>/images/credi_logo.png" alt="" class="user_logged_company_img">
								<div class="logged_company_names_wrapp">
									<span class="logged_company_name">Агріколь</span>
									<span class="logged_company_reg_num">12345678</span>									
								</div>
								<div class="clear_kry"></div>
								<a href="<?php echo home_url();?>/epo_profile/company/">Оновити інформацію</a>
								<div class="popup_delet_company">							
									<div class="change_pass_popup_head">
										<span>Ви впевнені що хочете прибрати цю компанію?</span>
										<img src="<?php print get_stylesheet_directory_uri(); ?>/images/close_button.svg" alt="">
									</div>	
									<span class="confirm_delete_company_aside col-md-5 col-sm-5 col-xs-5">Так</span><span class="dont_delete_company_aside col-md-5 col-sm-5 col-xs-5">Ні</span>
								</div>
							</div>	
							<div class="clear_kry"></div>
							<div class="company_info_box col-md-12 col-sm-12 col-xs-12">
								<span class="company_side_line"></span>
								<div class='remove_this_company'>
									<img src="<?php print get_stylesheet_directory_uri(); ?>/images/thrash.svg" alt="">
								</div>
								<img src="<?php print get_stylesheet_directory_uri(); ?>/images/nothumb.png" alt="" class="user_logged_company_img">
								<div class="logged_company_names_wrapp">
									<span class="logged_company_name">ФОП Носов Дмитро Андрійович</span>
									<span class="logged_company_reg_num">12345678</span>
								</div>	
								<div class="clear_kry"></div>
								<a href="">Оновити інформацію</a>	
								<div class="popup_delet_company">							
									<div class="change_pass_popup_head">
										<span>Ви впевнені що хочете прибрати цю компанію?</span>
										<img src="<?php print get_stylesheet_directory_uri(); ?>/images/close_button.svg" alt="">
									</div>	
									<span class="confirm_delete_company_aside col-md-5 col-sm-5 col-xs-5">Так</span><span class="dont_delete_company_aside col-md-5 col-sm-5 col-xs-5">Ні</span>
								</div>					
							</div>
							<div class="clear_kry"></div>
							<div class="logged_user_add_company_button">
								<img src="<?php print get_stylesheet_directory_uri(); ?>/images/add_company_button.png" alt="">Додати компанію
							</div>
						</div>							
					</div>
					<div class="fill_block"></div>
				</div>
			</form>
			<div class="fill_block_bottom"></div>
			<!-- Галерея услуг -->
			<div class="clear_kry"></div>
			<div class="carousel">
				<div class="overlay_companies"></div>
				<div class="services_carosel_title">Інші послуги</div>
				<div class="services_carousel owl-carousel">
					<a href="<?php echo home_url();?>/epo_profile/readliness_assessment_page/">
						<div class="corusel_item">
							<div class="service_carousel_item_title">Оцінка готовності</div>
							<div class="service_carousel_item_description">Оцініть свою готовність до зовнішньоекономічної діяльності. Визначте компетенції, яких вам бракує для досягення успіху.</div>
						</div>
					</a>
					<a href="<?php echo home_url();?>/epo_profile/checking_idea_page/">
						<div class="corusel_item">
							<div class="service_carousel_item_title">Перевірка ідеї</div>
							<div class="service_carousel_item_description">Отримайте допомогу у розробці плану інтернаціоналізації вашого бізнесу.</div>
						</div>
					</a>
					<a href="<?php echo home_url();?>/epo_profile/mentorstvo_page/">
						<div class="corusel_item">
							<div class="service_carousel_item_title">Менторство</div>
							<div class="service_carousel_item_description">Зв'язок з фахівцями, які зможуть надати професійну допомогу із пошуку партнерів та аналізу ринку.</div>
						</div>
					</a>
					<a href="<?php echo home_url();?>/epo_profile/internationalization_bussiness_page/">
						<div class="corusel_item">
							<div class="service_carousel_item_title">Інтернаціоналізація бізнесу</div>
							<div class="service_carousel_item_description">Зв'язок з фахівцями, які зможуть надати професійну допомогу із пошуку партнерів та аналізу ринку.</div>
						</div>
					</a>
					<a href="<?php echo home_url();?>/epo_profile/sourcing_service_page/">
						<div class="corusel_item">
							<div class="service_carousel_item_title">Soursing</div>
							<div class="service_carousel_item_description">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</div>
						</div>
					</a>
					<a href="<?php echo home_url();?>/epo_profile/een_page/">
						<div class="corusel_item">
							<div class="service_carousel_item_title">Enterprise Europe Network</div>
							<div class="service_carousel_item_description">EEN інформує про можливості розвитку в певній сфері, допомагає в залученні інвестицій для виробництва товарів і послуг, залученні нових технологій для підвищення конкурентноспроможності українських товаровиробників та підприємств, які надають послуги розвитку технологій, які були розроблені в Україні за допомогою закордонних партнерів.</div>
						</div>
					</a>			
				</div>
			</div>
		</div>
			<div class="clear_kry"></div>
		</div>
	<div class="clear"></div>
</div>
<style>
.header{
	position: fixed;
}

.content-left-wrap{
	padding-top: 0;
}
</style>
<?php get_footer();  ?>
