jQuery(document).ready(function(){
	
	var ajax_url = epo_profile_js_mlmvn.wp_ajax_url;
	
	jQuery(".change_pass_form").submit(function(){
		var action = 'page_profile_change_password';
		var data_form = jQuery(this).serialize();
		var data = {
			'action': action,
			'data_form': data_form
		};			
		jQuery(this).find(".for_disable").hide();
		jQuery(this).parent().find(".autorization_tatus").html('<div class="autorization_tatus_preloader"></div> Триває перевірка данних ...<p></p>');
		jQuery(this).parent().find(".autorization_tatus").show();		
		$this_form = jQuery(this);
		jQuery.post(ajax_url, data).done( function(response) {
			if(response.msg == 'ok'){
				window.location.replace(response.url);
			}else{
				$this_form.parent().find(".autorization_tatus").html(response.msg);
				$this_form.find(".for_disable").show();
			}
		});		
		return false;
	});
	
	jQuery(".thanks_block_password_creator").submit(function(){
		var pass_1 = jQuery(".thanks_block_password_creator_pass_1").val();
		var pass_2 = jQuery(".thanks_block_password_creator_pass_2").val();
		if(pass_1 != pass_2){
			alert("Паролі не співпадають! Спробуйте ввести ще раз ...");
			return false;
		}else{
			jQuery(this).find("button").hide();
		}
	});
	
	jQuery(".header_profile_login_form").submit(function(){
		var action = 'header_profile_login_form';
		var data_form = jQuery(this).serialize();
		var data = {
			'action': action,
			'data_form': data_form
		};			
		jQuery(this).hide();
		jQuery(this).parent().find(".autorization_tatus").html('<div class="autorization_tatus_preloader"></div> Триває вхід в систему ...<p></p>');
		jQuery(this).parent().find(".autorization_tatus").show();
		$this_form = jQuery(this);
		jQuery.post(ajax_url, data).done( function(response) {
			if(response.msg == 'ok'){
				window.location.replace(response.url);
			}else{
				$this_form.parent().find(".autorization_tatus").html(response.msg);
				$this_form.show();
			}
		});			
		return false;
	});
	
	jQuery(".header_profile_login_remind").submit(function(){
		var action = 'header_profile_login_remind';
		var data_form = jQuery(this).serialize();
		var data = {
			'action': action,
			'data_form': data_form
		};				
		jQuery(this).hide();
		jQuery(this).parent().find(".autorization_tatus").html('<div class="autorization_tatus_preloader"></div> Триває перевірка данних ...<p></p>');
		jQuery(this).parent().find(".autorization_tatus").show();
		$this_form = jQuery(this);
		jQuery.post(ajax_url, data).done( function(response) {
			if(response.msg == 'ok'){
				$this_form.parent().find(".autorization_tatus").html(response.text);
			}else{
				$this_form.parent().find(".autorization_tatus").html(response.msg);
				$this_form.show();
			}
		});			
		return false;
	});

	jQuery(".header_profile_login_registration").submit(function(){
		var action = 'header_profile_login_registration';
		var data_form = jQuery(this).serialize();
		var data = {
			'action': action,
			'data_form': data_form
		};				
		jQuery(this).hide();
		jQuery(this).parent().find(".autorization_tatus").html('<div class="autorization_tatus_preloader"></div> Триває реєстрація нового користувача ...<p></p>');
		jQuery(this).parent().find(".autorization_tatus").show();
		$this_form = jQuery(this);
		jQuery.post(ajax_url, data).done( function(response) {
			if(response.msg == 'ok'){
				$this_form.parent().find(".autorization_tatus").html(response.text);
			}else{
				$this_form.parent().find(".autorization_tatus").html(response.msg);
				$this_form.show();
			}
		});			
		return false;
	});	
	
});