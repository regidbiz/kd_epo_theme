<?php
class CrmApi{
	
	protected $cookie_file;
	
	public function __construct(){
		$this->cookie_file = '/var/www/html/wp-content/uploads/epo_api/cookie.txt';
	}
	
	public function crm_auth(){
		$url = 'https://epo.bpmonline.com/ServiceModel/AuthService.svc/Login';
		$args = [
			'UserName' => "Supervisor",
			'UserPassword' => "Asd!2345"
		];
		if($this->crm_curl($url, $args, 10, true)){
			return true;
		}
		return false;
	}
	
	/**
	* Добавляет детали контакта по email контакта
	*/
	public function add_detail_contact($email, $name, $array, $with_first = true){
		
		if($with_first == false){
			unset($array[0]);
		}
		
		if(count($array) > 0){
			
			$url = 'https://epo.bpmonline.com/0/DataService/json/reply/UpdateQuery';
			
			foreach($array as $item){
				$arr = [
					'RootSchemaName' => "Contact",
					'OperationType' => "Insert",
					'ColumnValues' => [
						'Items' => [
							$name => [
								'ExpressionType' => 'Parameter',
								'Parameter' => [
									'DataValueType' => 'Text',
									'Value' => $item
								]
							]				
						]
					],
					'Filters' => [
						'RootSchemaName' => 'Contact',
						'FilterType' => 'FilterGroup',
						'Items' => [
								'FilterByUsrMainMail' => [
									'FilterType' => 'CompareFilter',
									'ComparisonType' => 'Equal',
									'LeftExpression' => [
										'ExpressionType' => 'SchemaColumn',
										'ColumnPath' => 'UsrMainMail'
									],
									'RightExpression' => [
										'ExpressionType' => 'Parameter',
										'Parameter' => [
											'DataValueType' => 'Text',
											'Value' => $email
										]
									]
								]
						]
					]
				];	
				
				$this->crm_curl($url, $arr, 10);
				
			}
			
			return true;
			
		}
		return false;
	}
	
	/**
	* Вывод инфы пользователя по Email
	*/
	public function get_contact_data_by_email($email){
		
		$url = 'https://epo.bpmonline.com/0/DataService/json/reply/SelectQuery';
		
		$result = [];
		
		$filters = [
			'RootSchemaName' => 'Contact',
			'FilterType' => 'FilterGroup',
			'Items' => [
				'FilterByUsrMainMail' => [
					'FilterType' => 'CompareFilter',
					'ComparisonType' => 'Equal',
					'LeftExpression' => [
						'ExpressionType' => 'SchemaColumn',
						'ColumnPath' => 'UsrMainMail'
					],
					'RightExpression' => [
						'ExpressionType' => 'Parameter',
						'Parameter' => [
							'DataValueType' => 'Text',
							'Value' => $email
						]
					]
				]								
			]
		];
		
		/**
		* Берем основные данные контакта
		*/
		$arr = [
			'RootSchemaName' => "Contact",
			'OperationType' => "Select",						
			'Columns' => [
				'Items' => [
					'Name' => [
						'OrderDirection' => 0,
						'OrderPosition' => 0,
						'Caption' => null,
						'Expression' => [
							'ExpressionType' => 0,
							'ColumnPath' => 'Name'
						]
					],	
					'Gender' => [
						'OrderDirection' => 0,
						'OrderPosition' => 0,
						'Caption' => null,
						'Expression' => [
							'ExpressionType' => 0,
							'ColumnPath' => 'Gender'
						]
					],						
					'UsrMainMail' => [
						'OrderDirection' => 0,
						'OrderPosition' => 0,
						'Caption' => null,
						'Expression' => [
							'ExpressionType' => 0,
							'ColumnPath' => 'UsrMainMail'
						]
					]							
				]
			],
			'Filters' => $filters
		];	
		$res = $this->crm_curl($url, $arr, 10);
		
		if($res){
			$res_obj = json_decode($res);
			if(isset($res_obj->rows) && count($res_obj->rows) > 0){
				$result = (array)$res_obj->rows[0];
			}
		}
		$gender_arr = $result['Gender'];
		$result['Gender'] = $gender_arr->displayValue;		
		
		/**
		* Берем данные из обьекта ContactCommunication
		*/
		$arr = [
			'RootSchemaName' => "Contact",
			'OperationType' => "Select",						
			'Columns' => [
				'Items' => [						
					'Communication' => [
						'OrderDirection' => 0,
						'OrderPosition' => 0,
						'Caption' => null,
						'Expression' => [
							'ExpressionType' => 0,
							'ColumnPath' => '[ContactCommunication:Contact].Number'
						]
					],
					'CommunicationType' => [
						'OrderDirection' => 0,
						'OrderPosition' => 0,
						'Caption' => null,
						'Expression' => [
							'ExpressionType' => 0,
							'ColumnPath' => '[ContactCommunication:Contact].CommunicationType'
						]
					]								
				]
			],
			'Filters' => $filters
		];	
		
		$res = $this->crm_curl($url, $arr, 10);	

		if($res){
			$res_obj = json_decode($res);
			$ContactCommunication = [];
			if(isset($res_obj->rows) && count($res_obj->rows) > 0){
				foreach($res_obj->rows as $res_obj_row){
					$row_item = [];
					$val = $res_obj_row->Communication;
					$type = $res_obj_row->CommunicationType->displayValue;
					$row_item['value'] = $val;
					$row_item['type'] = $type;
					array_push($ContactCommunication, $row_item);
				}
				$result['ContactCommunication'] = $ContactCommunication;
			}
		}
		
		
		/**
		* Берем данные из обьекта ContactCareer
		*/
		$arr = [
			'RootSchemaName' => "Contact",
			'OperationType' => "Select",						
			'Columns' => [
				'Items' => [						
					'AccountItem' => [
						'OrderDirection' => 0,
						'OrderPosition' => 0,
						'Caption' => null,
						'Expression' => [
							'ExpressionType' => 0,
							'ColumnPath' => '[ContactCareer:Contact].Account'
						]
					],
					'JobTitle' => [
						'OrderDirection' => 0,
						'OrderPosition' => 0,
						'Caption' => null,
						'Expression' => [
							'ExpressionType' => 0,
							'ColumnPath' => '[ContactCareer:Contact].JobTitle'
						]
					],
					'Job' => [
						'OrderDirection' => 0,
						'OrderPosition' => 0,
						'Caption' => null,
						'Expression' => [
							'ExpressionType' => 0,
							'ColumnPath' => '[ContactCareer:Contact].Job'
						]
					],
					'UsrNotApplyed' => [
						'OrderDirection' => 0,
						'OrderPosition' => 0,
						'Caption' => null,
						'Expression' => [
							'ExpressionType' => 0,
							'ColumnPath' => '[ContactCareer:Contact].UsrNotApplyed'
						]					
					]
				]
			],
			'Filters' => $filters
		];	
		
		$res = $this->crm_curl($url, $arr, 10);	

		if($res){
			$res_obj = json_decode($res);
			$ContactCareer = [];
			if(isset($res_obj->rows) && count($res_obj->rows) > 0){
				foreach($res_obj->rows as $res_obj_row){
					$res_item = [];
					$res_item['AccountName'] = $res_obj_row->AccountItem->displayValue;
					$res_item['AccountId'] = $res_obj_row->AccountItem->value;
					$res_item['JobTitle'] = $res_obj_row->JobTitle;
					$res_item['FullJobTitle'] = $res_obj_row->Job->displayValue;
					$res_item['UsrNotApplyed'] = $res_obj_row->UsrNotApplyed;
					$res_item['UsrEdrpouCode'] = $this->get_edrpou_by_company_id($res_obj_row->AccountItem->value);
					$res_item['AccountLogo'] = $this->get_logo_by_company_id($res_obj_row->AccountItem->value);
					array_push($ContactCareer, $res_item);
				}
				$result['ContactCareer'] = $ContactCareer;
			}
		}
		
		return $result;
		
	}

	/**
	* Вывод ЄДРПОУ компании по ее id
	*/	
	public function get_edrpou_by_company_id($company_id){
		
		$url = 'https://epo.bpmonline.com/0/DataService/json/reply/SelectQuery';
		
		$filters = [
			'RootSchemaName' => 'Account',
			'FilterType' => 'FilterGroup',
			'Items' => [
				'FilterByAccoundId' => [
					'FilterType' => 'CompareFilter',
					'ComparisonType' => 'Equal',
					'LeftExpression' => [
						'ExpressionType' => 'SchemaColumn',
						'ColumnPath' => 'Id'
					],
					'RightExpression' => [
						'ExpressionType' => 'Parameter',
						'Parameter' => [
							'DataValueType' => 'Guid',
							'Value' => $company_id
						]
					]
				]								
			]
		];		
		
		/**
		* Берем данные из обьекта Account
		*/
		$arr = [
			'RootSchemaName' => "Account",
			'OperationType' => "Select",						
			'Columns' => [
				'Items' => [						
					'UsrEdrpouCode' => [
						'OrderDirection' => 0,
						'OrderPosition' => 0,
						'Caption' => null,
						'Expression' => [
							'ExpressionType' => 0,
							'ColumnPath' => 'UsrEdrpouCode'
						]
					]							
				]
			],
			'Filters' => $filters
		];	
		
		$res = $this->crm_curl($url, $arr, 10);			
		if($res){
			$res_obj = json_decode($res);
			if(isset($res_obj->rows) && count($res_obj->rows) > 0){
				return $res_obj->rows[0]->UsrEdrpouCode;
			}
		}
		
		return false;
		
	}	

	/**
	* Вывод Лого компании по ее id
	*/	
	public function get_logo_by_company_id($company_id){
		
		$url = 'https://epo.bpmonline.com/0/DataService/json/reply/SelectQuery';
		
		$filters = [
			'RootSchemaName' => 'Account',
			'FilterType' => 'FilterGroup',
			'Items' => [
				'FilterByAccoundId' => [
					'FilterType' => 'CompareFilter',
					'ComparisonType' => 'Equal',
					'LeftExpression' => [
						'ExpressionType' => 'SchemaColumn',
						'ColumnPath' => 'Id'
					],
					'RightExpression' => [
						'ExpressionType' => 'Parameter',
						'Parameter' => [
							'DataValueType' => 'Guid',
							'Value' => $company_id
						]
					]
				]								
			]
		];		
		
		/**
		* Берем данные из обьекта Account
		*/
		$arr = [
			'RootSchemaName' => "Account",
			'OperationType' => "Select",						
			'Columns' => [
				'Items' => [						
					'Logo' => [
						'OrderDirection' => 0,
						'OrderPosition' => 0,
						'Caption' => null,
						'Expression' => [
							'ExpressionType' => 0,
							'ColumnPath' => 'Logo'
						]
					]							
				]
			],
			'Filters' => $filters
		];	
		
		$res = $this->crm_curl($url, $arr, 10);		
		
		if($res){
			$res_obj = json_decode($res);
			if(isset($res_obj->rows) && count($res_obj->rows) > 0){
				//return $res_obj->rows[0]->AccountLogo->value;
				if($res_obj->rows[0]->AccountLogo->value != ''){
					return 'https://epo.bpmonline.com/0/img/entity/hash/SysImage/Data/' . $res_obj->rows[0]->AccountLogo->value;
				}else{
					return 'https://epo.bpmonline.com/0/conf/content/img/AccountPageV2-DefaultLogo.svg?hash=9e87369446a2c6eeb7c62d4126b8024e';
				}
			}
		}
		
		return false;
		
	}
	
	/**
	* Проверка на существование контакта с указанным Email
	*/
	public function check_email_exist_on_contacts($emails = []){
		
		$url = 'https://epo.bpmonline.com/0/DataService/json/reply/SelectQuery';
		
		/**
		* Проверка по основному email
		*/
		foreach($emails as $email){
			$arr = [
				'RootSchemaName' => "Contact",
						'OperationType' => "Select",
						'AllColumns' => true,
						'Filters' => [
							'RootSchemaName' => 'Contact',
							'FilterType' => 'FilterGroup',
							'Items' => [
									'FilterByUsrMainMail' => [
										'FilterType' => 'CompareFilter',
										'ComparisonType' => 'Equal',
										'LeftExpression' => [
											'ExpressionType' => 'SchemaColumn',
											'ColumnPath' => 'UsrMainMail'
										],
										'RightExpression' => [
											'ExpressionType' => 'Parameter',
											'Parameter' => [
												'DataValueType' => 'Text',
												'Value' => $email
											]
										]
									]								
							]
						]
			];		
			$res = $this->crm_curl($url, $arr, 10);
			if($res){
				$res_obj = json_decode($res);
				if(isset($res_obj->rows) && count($res_obj->rows) > 0){
					return count($res_obj->rows);
				}
			}
		}

		/**
		* Проверка по второстепенным email
		*/		
		foreach($emails as $email){
			$arr = [
				'RootSchemaName' => "Contact",
						'OperationType' => "Select",
						'AllColumns' => true,
						'Filters' => [
							'RootSchemaName' => 'Contact',
							'FilterType' => 'FilterGroup',
							'Items' => [
									'FilterByEmail' => [
										'FilterType' => 'CompareFilter',
										'ComparisonType' => 'Equal',
										'LeftExpression' => [
											'ExpressionType' => 'SchemaColumn',
											'ColumnPath' => 'Email'
										],
										'RightExpression' => [
											'ExpressionType' => 'Parameter',
											'Parameter' => [
												'DataValueType' => 'Text',
												'Value' => $email
											]
										]
									]									
							]
						]
			];		
			$res = $this->crm_curl($url, $arr, 10);
			if($res){
				$res_obj = json_decode($res);
				if(isset($res_obj->rows) && count($res_obj->rows) > 0){
					return count($res_obj->rows);
				}
			}
		}		
		
		return false;
	}
	
	/**
	* Добавляет Контакт с начальной информацией. Также проверяет, есть ли контакт с таким Email
	*/	
	public function add_new_contact($args){
		
		$url = 'https://epo.bpmonline.com/0/DataService/json/reply/InsertQuery';
		
		$Name = isset($args['Name']) ? $args['Name'] : false;
		$UsrMainMail = isset($args['UsrMainMail']) ? $args['UsrMainMail'] : false;
		$Gender = isset($args['Gender']) ? $args['Gender'] : 'Чоловіча';
		$Photo = isset($args['Photo']) ? $args['Photo'] : '';
		$Skype = isset($args['Skype']) ? $args['Skype'] : '';
		$MobilePhone = isset($args['MobilePhone']) ? $args['MobilePhone'] : '';
		$Phone = isset($args['Phone']) ? $args['Phone'] : '';
		$Email = isset($args['Email']) ? $args['Email'] : '';
		
		if($Gender == 'Чоловіча' || $Gender == 'Male'){
			$Gender = 'eeac42ee-65b6-df11-831a-001d60e938c6';
		}else{
			$Gender = 'fc2483f8-65b6-df11-831a-001d60e938c6';
		}
		
		if(is_array($Skype)){
			$SkypeFirst = $Skype[0];
		}else{
			$SkypeFirst = $Skype;
		}
		
		if(is_array($MobilePhone)){
			$MobilePhoneFirst = $MobilePhone[0];
		}else{
			$MobilePhoneFirst = $MobilePhone;
		}		
		
		if(is_array($Phone)){
			$PhoneFirst = $Phone[0];
		}else{
			$PhoneFirst = $Phone;
		}	

		if(is_array($Email)){
			$EmailFirst = $Email[0];
		}else{
			$EmailFirst = $Email;
		}		
		
		$PhotoObject = [
			'itemType' => 'jpg',
			'className' => 'ImageView',
			'imageSrc' => $Photo
		];
		
		if($Name !== false && $UsrMainMail !== false){
			
			/**
			* Проверяем не существует ли Юзер по таким емейлам. Если уже гдето числится то не создаем.
			*/
			
			$check_emails = [$UsrMainMail];
			if(is_array($Email)){
				foreach($Email as $EmailItem){
					array_push($check_emails, $EmailItem);
				}
			}else{
				array_push($check_emails, $Email);
			}
			$check_emails = array_unique($check_emails);
			if($this->check_email_exist_on_contacts($check_emails) !== false){
				return false;
			}
			
			$arr = [
				'RootSchemaName' => "Contact",
				'OperationType' => "Insert",
				'ColumnValues' => [
					'Items' => [
						'Name' => [
							'ExpressionType' => 'Parameter',
							'Parameter' => [
								'DataValueType' => 'Text',
								'Value' => $Name
							]
						],
						'UsrMainMail' => [
							'ExpressionType' => 'Parameter',
							'Parameter' => [
								'DataValueType' => 'Text',
								'Value' => $UsrMainMail
							]
						],
						'Gender' => [
							'ExpressionType' => 'Parameter',
							'Parameter' => [
								'DataValueType' => 'Guid',
								'Value' => $Gender
							]
						],
						'Skype' => [
							'ExpressionType' => 'Parameter',
							'Parameter' => [
								'DataValueType' => 'Text',
								'Value' => $SkypeFirst
							]
						],		
						'MobilePhone' => [
							'ExpressionType' => 'Parameter',
							'Parameter' => [
								'DataValueType' => 'Text',
								'Value' => $MobilePhoneFirst
							]
						],		
						'Phone' => [
							'ExpressionType' => 'Parameter',
							'Parameter' => [
								'DataValueType' => 'Text',
								'Value' => $PhoneFirst
							]
						],
						'Email' => [
							'ExpressionType' => 'Parameter',
							'Parameter' => [
								'DataValueType' => 'Text',
								'Value' => $EmailFirst
							]						
						]
					]
				]
			];		
			
			/**
			* Добавляем данные
			*/
			
			if($res_str = $this->crm_curl($url, $arr, 10)){
				
				$res_object = json_decode($res_str);
				
				if(isset($res_object->id) && $res_object->id != ''){
			
					if(is_array($Skype)){
						$SkypeFirst = $Skype[0];
						$this->add_detail_contact($UsrMainMail, 'Skype', $Skype, false);
					}
					
					if(is_array($MobilePhone)){
						$MobilePhoneFirst = $MobilePhone[0];
						$this->add_detail_contact($UsrMainMail, 'MobilePhone', $MobilePhone, false);
					}	
					
					if(is_array($Phone)){
						$PhoneFirst = $Phone[0];
						$this->add_detail_contact($UsrMainMail, 'Phone', $Phone, false);
					}	

					if(is_array($Email)){
						$EmailFirst = $Email[0];
						$this->add_detail_contact($UsrMainMail, 'Email', $Email, false);
					}
					
					return $res_object->id;
				
				}
				
			}
			
		}
		return false;
	}
	
	public static function get_token_from_cookie($file){
		$lines = file( $file ); 
		$str =  $lines[6]; 
		$prefix = "BPMCSRF";
		$index = strpos($str, $prefix) + strlen($prefix);
		$token = trim(substr($str, $index));		
		return $token;
	}

	protected function crm_curl($uri, $args = false, $timeout, $auth = false){
		$cookie_file = $this->cookie_file;
		try {
			$handle = curl_init($uri);
			if($auth){
				curl_setopt($handle, CURLOPT_COOKIEJAR, $cookie_file);
			}else{
				curl_setopt($handle, CURLOPT_COOKIEFILE, $cookie_file);
			}
			curl_setopt($handle, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($handle, CURLOPT_POSTFIELDS, json_encode($args));
			curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
			if($auth){
				curl_setopt($handle, CURLOPT_HTTPHEADER, array(
					 'Content-Type: application/json',
					 'Content-Length: ' . strlen(json_encode($args)))
				);
			}else{
				$token = self::get_token_from_cookie($cookie_file);
				curl_setopt($handle, CURLOPT_HTTPHEADER, array(
					 'Content-Type: application/json',
					 'Content-Length: ' . strlen(json_encode($args)),
					 'BPMCSRF: ' . $token
				));				
			}
			$response = curl_exec($handle);
			$httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
			if ($httpCode != 200) {
				throw new Exception($httpCode);
			}
			curl_close($handle);
			return $response;
		}catch (Exception $e) {
			return false;
		}			
	}
	
	
}