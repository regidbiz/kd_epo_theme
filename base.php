<?php
/*
Template Name: base
*/
get_header(); ?>

<div class="clear"></div>

</header> <!-- / END HOME SECTION  -->
<?php zerif_after_header_trigger(); ?>
<div id="content" class="site-content">

	<div class="container">

		<?php zerif_before_page_content_trigger(); ?>

		<div class="content-left-wrap col-md-12">

			<?php zerif_top_page_content_trigger(); ?>

			<div id="primary" class="content-area">

				<main id="main" class="site-main">

					<article id="post-1683" class="post-1683 page type-page status-future hentry">

						<header class="entry-header">

							<span class="date updated published">Липень 19, 2017</span>
							<span class="vcard author byline"><a href="http://epo.org.ua/author/stanislav/" class="fn">Stanislav</a></span>

							<h1 class="entry-title" itemprop="headline">База партнерів</h1>
						</header><!-- .entry-header -->

						<div class="entry-content">

							<div class="base_block base_block1 col-md-4">
								<h3>Товар</h3>
								<div>
									<select multiple="" size="7" class="base_select base_select1"><option value="Testvalue1">Значення1</option><option value="Testvalue2">Значення2</option><option value="Testvalue3">Значення3</option><option value="Testvalue4">Значення4</option><option value="Testvalue5">Значення5</option><option value="Testvalue6">Значення6</option><option value="Testvalue7">Значення7</option><option value="Testvalue8">Значення8</option><option value="Testvalue9">Значення9</option><option value="Testvalue10">Значення10</option><option value="Testvalue11">Значення11</option><option value="Testvalue12">Значення12</option></select>
								</div>
							</div>
							<div class="base_block base_block2 col-md-4">
								<h3>Країна</h3>
								<div>
									<select multiple="" size="7" class="base_select base_select2"><option value="Testvalue1">Значення1</option><option value="Testvalue2">Значення2</option><option value="Testvalue3">Значення3</option><option value="Testvalue4">Значення4</option><option value="Testvalue5">Значення5</option><option value="Testvalue6">Значення6</option><option value="Testvalue7">Значення7</option><option value="Testvalue8">Значення8</option><option value="Testvalue9">Значення9</option><option value="Testvalue10">Значення10</option><option value="Testvalue11">Значення11</option><option value="Testvalue12">Значення12</option></select>
								</div>
							</div>
							<div class="base_block base_block3 col-md-4">
								<h3>Тип компанії</h3>
								<div>
									<select multiple="" size="7" class="base_select base_select3"><option value="Testvalue1">Значення1</option><option value="Testvalue2">Значення2</option><option value="Testvalue3">Значення3</option><option value="Testvalue4">Значення4</option><option value="Testvalue5">Значення5</option><option value="Testvalue6">Значення6</option><option value="Testvalue7">Значення7</option><option value="Testvalue8">Значення8</option><option value="Testvalue9">Значення9</option><option value="Testvalue10">Значення10</option><option value="Testvalue11">Значення11</option><option value="Testvalue12">Значення12</option></select>
								</div>
							</div>
							<form action="">
							<div class="reg-request">
								<input type="submit" value="Отримати" class="blue-button">
								<?php echo do_shortcode("[bws_google_captcha]"); ?>
								<div class="emailfeld"><span class="your-email"><input type="email" name="your-email" value="" size="40" class="base-form-control" aria-required="true" aria-invalid="false" placeholder="Ваш email"></span></div>
							</div>
							</form>
							<div class="tablecontainer">
								<table class="basetable" cellspacing="0" cellpadding="0">
									<tbody><tr class="row_0">
										<th class="clo_1">TABLE HEADER 1</th>
										<th class="clo_2">TABLE HEADER 2</th>
										<th class="clo_3">TABLE HEADER 3</th>
										<th class="clo_4">TABLE HEADER 4</th>
										<th class="clo_5">TABLE HEADER 5</th>
										<th class="clo_6">TABLE HEADER 6</th>
										<th class="clo_7">TABLE HEADER 7</th>
										<th class="clo_8">TABLE HEADER 8</th>
										<th class="clo_9">TABLE HEADER 9</th>
										<th class="clo_10">TABLE HEADER 10</th>
									</tr>
									<tr class="row_1">
										<td class="col_1">ТОВ Название компании</td>
										<td class="col_2">+38 000 000 00 00</td>
										<td class="col_3">testemail@gmail.com</td>
										<td class="col_4">www.website.com</td>
										<td class="col_5">Отрасль компании</td>
										<td class="col_6">Тип компании</td>
										<td class="col_7">Страна</td>
										<td class="col_8">Товар</td>
										<td class="col_9">Город</td>
										<td class="col_10">Имя руководителя</td>
									</tr>
									<tr class="row_2">
										<td class="col_1">ТОВ Название компании</td>
										<td class="col_2">+38 000 000 00 00</td>
										<td class="col_3">testemail@gmail.com</td>
										<td class="col_4">www.website.com</td>
										<td class="col_5">Отрасль компании</td>
										<td class="col_6">Тип компании</td>
										<td class="col_7">Страна</td>
										<td class="col_8">Товар</td>
										<td class="col_9">Город</td>
										<td class="col_10">Имя руководителя</td>
									</tr>
									<tr class="row_3">
										<td class="col_1">ТОВ Название компании</td>
										<td class="col_2">+38 000 000 00 00</td>
										<td class="col_3">testemail@gmail.com</td>
										<td class="col_4">www.website.com</td>
										<td class="col_5">Отрасль компании</td>
										<td class="col_6">Тип компании</td>
										<td class="col_7">Страна</td>
										<td class="col_8">Товар</td>
										<td class="col_9">Город</td>
										<td class="col_10">Имя руководителя</td>
									</tr>
									<tr class="row_4">
										<td class="col_1">ТОВ Название компании</td>
										<td class="col_2">+38 000 000 00 00</td>
										<td class="col_3">testemail@gmail.com</td>
										<td class="col_4">www.website.com</td>
										<td class="col_5">Отрасль компании</td>
										<td class="col_6">Тип компании</td>
										<td class="col_7">Страна</td>
										<td class="col_8">Товар</td>
										<td class="col_9">Город</td>
										<td class="col_10">Имя руководителя</td>
									</tr>
									<tr class="row_5">
										<td class="col_1">ТОВ Название компании</td>
										<td class="col_2">+38 000 000 00 00</td>
										<td class="col_3">testemail@gmail.com</td>
										<td class="col_4">www.website.com</td>
										<td class="col_5">Отрасль компании</td>
										<td class="col_6">Тип компании</td>
										<td class="col_7">Страна</td>
										<td class="col_8">Товар</td>
										<td class="col_9">Город</td>
										<td class="col_10">Имя руководителя</td>
									</tr>
									<tr class="row_6">
										<td class="col_1">ТОВ Название компании</td>
										<td class="col_2">+38 000 000 00 00</td>
										<td class="col_3">testemail@gmail.com</td>
										<td class="col_4">www.website.com</td>
										<td class="col_5">Отрасль компании</td>
										<td class="col_6">Тип компании</td>
										<td class="col_7">Страна</td>
										<td class="col_8">Товар</td>
										<td class="col_9">Город</td>
										<td class="col_10">Имя руководителя</td>
									</tr>
									<tr class="row_7">
										<td class="col_1">ТОВ Название компании</td>
										<td class="col_2">+38 000 000 00 00</td>
										<td class="col_3">testemail@gmail.com</td>
										<td class="col_4">www.website.com</td>
										<td class="col_5">Отрасль компании</td>
										<td class="col_6">Тип компании</td>
										<td class="col_7">Страна</td>
										<td class="col_8">Товар</td>
										<td class="col_9">Город</td>
										<td class="col_10">Имя руководителя</td>
									</tr>
									<tr class="row_8">
										<td class="col_1">ТОВ Название компании</td>
										<td class="col_2">+38 000 000 00 00</td>
										<td class="col_3">testemail@gmail.com</td>
										<td class="col_4">www.website.com</td>
										<td class="col_5">Отрасль компании</td>
										<td class="col_6">Тип компании</td>
										<td class="col_7">Страна</td>
										<td class="col_8">Товар</td>
										<td class="col_9">Город</td>
										<td class="col_10">Имя руководителя</td>
									</tr>
									<tr class="row_9">
										<td class="col_1">ТОВ Название компании</td>
										<td class="col_2">+38 000 000 00 00</td>
										<td class="col_3">testemail@gmail.com</td>
										<td class="col_4">www.website.com</td>
										<td class="col_5">Отрасль компании</td>
										<td class="col_6">Тип компании</td>
										<td class="col_7">Страна</td>
										<td class="col_8">Товар</td>
										<td class="col_9">Город</td>
										<td class="col_10">Имя руководителя</td>
									</tr>
									<tr class="row_10">
										<td class="col_1">ТОВ Название компании</td>
										<td class="col_2">+38 000 000 00 00</td>
										<td class="col_3">testemail@gmail.com</td>
										<td class="col_4">www.website.com</td>
										<td class="col_5">Отрасль компании</td>
										<td class="col_6">Тип компании</td>
										<td class="col_7">Страна</td>
										<td class="col_8">Товар</td>
										<td class="col_9">Город</td>
										<td class="col_10">Имя руководителя</td>
									</tr>
									<tr class="row_11">
										<td class="col_1">ТОВ Название компании</td>
										<td class="col_2">+38 000 000 00 00</td>
										<td class="col_3">testemail@gmail.com</td>
										<td class="col_4">www.website.com</td>
										<td class="col_5">Отрасль компании</td>
										<td class="col_6">Тип компании</td>
										<td class="col_7">Страна</td>
										<td class="col_8">Товар</td>
										<td class="col_9">Город</td>
										<td class="col_10">Имя руководителя</td>
									</tr>
									<tr class="row_12">
										<td class="col_1">ТОВ Название компании</td>
										<td class="col_2">+38 000 000 00 00</td>
										<td class="col_3">testemail@gmail.com</td>
										<td class="col_4">www.website.com</td>
										<td class="col_5">Отрасль компании</td>
										<td class="col_6">Тип компании</td>
										<td class="col_7">Страна</td>
										<td class="col_8">Товар</td>
										<td class="col_9">Город</td>
										<td class="col_10">Имя руководителя</td>
									</tr>
									<tr class="row_13">
										<td class="col_1">ТОВ Название компании</td>
										<td class="col_2">+38 000 000 00 00</td>
										<td class="col_3">testemail@gmail.com</td>
										<td class="col_4">www.website.com</td>
										<td class="col_5">Отрасль компании</td>
										<td class="col_6">Тип компании</td>
										<td class="col_7">Страна</td>
										<td class="col_8">Товар</td>
										<td class="col_9">Город</td>
										<td class="col_10">Имя руководителя</td>
									</tr>
									<tr class="row_14">
										<td class="col_1">ТОВ Название компании</td>
										<td class="col_2">+38 000 000 00 00</td>
										<td class="col_3">testemail@gmail.com</td>
										<td class="col_4">www.website.com</td>
										<td class="col_5">Отрасль компании</td>
										<td class="col_6">Тип компании</td>
										<td class="col_7">Страна</td>
										<td class="col_8">Товар</td>
										<td class="col_9">Город</td>
										<td class="col_10">Имя руководителя</td>
									</tr>
									<tr class="row_15">
										<td class="col_1">ТОВ Название компании</td>
										<td class="col_2">+38 000 000 00 00</td>
										<td class="col_3">testemail@gmail.com</td>
										<td class="col_4">www.website.com</td>
										<td class="col_5">Отрасль компании</td>
										<td class="col_6">Тип компании</td>
										<td class="col_7">Страна</td>
										<td class="col_8">Товар</td>
										<td class="col_9">Город</td>
										<td class="col_10">Имя руководителя</td>
									</tr>
									<tr class="row_16">
										<td class="col_1">ТОВ Название компании</td>
										<td class="col_2">+38 000 000 00 00</td>
										<td class="col_3">testemail@gmail.com</td>
										<td class="col_4">www.website.com</td>
										<td class="col_5">Отрасль компании</td>
										<td class="col_6">Тип компании</td>
										<td class="col_7">Страна</td>
										<td class="col_8">Товар</td>
										<td class="col_9">Город</td>
										<td class="col_10">Имя руководителя</td>
									</tr>
									<tr class="row_17">
										<td class="col_1">ТОВ Название компании</td>
										<td class="col_2">+38 000 000 00 00</td>
										<td class="col_3">testemail@gmail.com</td>
										<td class="col_4">www.website.com</td>
										<td class="col_5">Отрасль компании</td>
										<td class="col_6">Тип компании</td>
										<td class="col_7">Страна</td>
										<td class="col_8">Товар</td>
										<td class="col_9">Город</td>
										<td class="col_10">Имя руководителя</td>
									</tr>
									<tr class="row_18">
										<td class="col_1">ТОВ Название компании</td>
										<td class="col_2">+38 000 000 00 00</td>
										<td class="col_3">testemail@gmail.com</td>
										<td class="col_4">www.website.com</td>
										<td class="col_5">Отрасль компании</td>
										<td class="col_6">Тип компании</td>
										<td class="col_7">Страна</td>
										<td class="col_8">Товар</td>
										<td class="col_9">Город</td>
										<td class="col_10">Имя руководителя</td>
									</tr>
									<tr class="row_19">
										<td class="col_1">ТОВ Название компании</td>
										<td class="col_2">+38 000 000 00 00</td>
										<td class="col_3">testemail@gmail.com</td>
										<td class="col_4">www.website.com</td>
										<td class="col_5">Отрасль компании</td>
										<td class="col_6">Тип компании</td>
										<td class="col_7">Страна</td>
										<td class="col_8">Товар</td>
										<td class="col_9">Город</td>
										<td class="col_10">Имя руководителя</td>
									</tr>
									<tr class="row_20">
										<td class="col_1">ТОВ Название компании</td>
										<td class="col_2">+38 000 000 00 00</td>
										<td class="col_3">testemail@gmail.com</td>
										<td class="col_4">www.website.com</td>
										<td class="col_5">Отрасль компании</td>
										<td class="col_6">Тип компании</td>
										<td class="col_7">Страна</td>
										<td class="col_8">Товар</td>
										<td class="col_9">Город</td>
										<td class="col_10">Имя руководителя</td>
									</tr>
								</tbody></table>
							</div>

						</div><!-- .entry-content -->

						<footer class="entry-footer"><span class="edit-link"><a class="post-edit-link" href="http://epo.org.ua/wp-admin/post.php?post=1683&amp;action=edit">Edit</a></span></footer>
					</article>

				</main><!-- #main -->

			</div><!-- #primary -->

			<?php zerif_bottom_page_content_trigger(); ?>

		</div><!-- .content-left-wrap -->

		<?php zerif_after_page_content_trigger(); ?>

	</div><!-- .container -->

	<?php get_footer(); ?>