<?php
/*
Template Name: EN version
*/
?>

<!DOCTYPE html>

<html lang="uk">

<head>

	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="/xmlrpc.php">

	<title>Офіс з просування експорту &#8211; Консультативно-дорадчий орган при Міністерстві економічного розвитку і торгівлі України</title>
	<link rel='dns-prefetch' href='//fonts.googleapis.com' />
	<link rel='dns-prefetch' href='//s.w.org' />
	<link rel="alternate" type="application/rss+xml" title="Офіс з просування експорту &raquo; стрічка" href="/feed/" />
	<link rel="alternate" type="application/rss+xml" title="Офіс з просування експорту &raquo; Канал коментарів" href="/comments/feed/" />
	<script type="text/javascript">
		window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/epo.org.ua\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.7.5"}};
		!function(a,b,c){function d(a){var b,c,d,e,f=String.fromCharCode;if(!k||!k.fillText)return!1;switch(k.clearRect(0,0,j.width,j.height),k.textBaseline="top",k.font="600 32px Arial",a){case"flag":return k.fillText(f(55356,56826,55356,56819),0,0),!(j.toDataURL().length<3e3)&&(k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,65039,8205,55356,57096),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,55356,57096),0,0),c=j.toDataURL(),b!==c);case"emoji4":return k.fillText(f(55357,56425,55356,57341,8205,55357,56507),0,0),d=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55357,56425,55356,57341,55357,56507),0,0),e=j.toDataURL(),d!==e}return!1}function e(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i,j=b.createElement("canvas"),k=j.getContext&&j.getContext("2d");for(i=Array("flag","emoji4"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
	</script>
	<style type="text/css">
		img.wp-smiley,
		img.emoji {
			display: inline !important;
			border: none !important;
			box-shadow: none !important;
			height: 1em !important;
			width: 1em !important;
			margin: 0 .07em !important;
			vertical-align: -0.1em !important;
			background: none !important;
			padding: 0 !important;
		}
	</style>
	<link rel='stylesheet' id='dashicons-css'  href='/wp-includes/css/dashicons.min.css?ver=4.7.5' type='text/css' media='all' />
	<link rel='stylesheet' id='admin-bar-css'  href='/wp-includes/css/admin-bar.min.css?ver=4.7.5' type='text/css' media='all' />
	<link rel='stylesheet' id='contact-form-7-css'  href='/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=4.7' type='text/css' media='all' />
	<link rel='stylesheet' id='events-manager-css'  href='/wp-content/plugins/events-manager/includes/css/events_manager.css?ver=5.6624' type='text/css' media='all' />
	<link rel='stylesheet' id='pirate_forms_front_styles-css'  href='/wp-content/plugins/pirate-forms/css/front.css?ver=4.7.5' type='text/css' media='all' />
	<link rel='stylesheet' id='zerif_font_all-css'  href='//fonts.googleapis.com/css?family=Open+Sans%3A300%2C300italic%2C400%2C400italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic&#038;subset=latin&#038;ver=4.7.5' type='text/css' media='all' />
	<link rel='stylesheet' id='zerif_bootstrap_style-css'  href='/wp-content/themes/EPO/css/bootstrap.css?ver=4.7.5' type='text/css' media='all' />
	<link rel='stylesheet' id='zerif_fontawesome-css'  href='/wp-content/themes/EPO/css/font-awesome.min.css?ver=v1' type='text/css' media='all' />
	<link rel='stylesheet' id='zerif_style-css'  href='/wp-content/themes/EPO/style.css?ver=v1' type='text/css' media='all' />
	<link rel='stylesheet' id='zerif_responsive_style-css'  href='/wp-content/themes/EPO/css/responsive.css?ver=v1' type='text/css' media='all' />
	<link rel="stylesheet" href="/wp-content/themes/EPO/en-style.css">
<!--[if lt IE 9]>
<link rel='stylesheet' id='zerif_ie_style-css'  href='/wp-content/themes/EPO/css/ie.css?ver=v1' type='text/css' media='all' />
<![endif]-->
<link rel='stylesheet' id='bxslider-css-css'  href='/wp-content/themes/EPO/css/jquery.bxslider.css?ver=4.7.5' type='text/css' media='all' />
<link rel='stylesheet' id='wpgform-css-css'  href='/wp-content/plugins/wpgform/css/wpgform.css?ver=4.7.5' type='text/css' media='all' />
<link rel='stylesheet' id='msl-main-css'  href='/wp-content/plugins/master-slider/public/assets/css/masterslider.main.css?ver=2.9.8' type='text/css' media='all' />
<link rel='stylesheet' id='msl-custom-css'  href='/wp-content/uploads/master-slider/custom.css?ver=8.1' type='text/css' media='all' />

<!-- This site uses the Google Analytics by MonsterInsights plugin v 6.1.9 - https://www.monsterinsights.com/ -->
<!-- @Webmaster, normally you will find the Google Analytics tracking code here, but you are in the disabled user groups. To change this, navigate to Insights -> Settings -> Ignore Users -->
<!-- / Google Analytics by MonsterInsights -->

<script type='text/javascript' src='/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
<script type='text/javascript' src='/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<script type='text/javascript' src='/wp-includes/js/jquery/ui/core.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='/wp-includes/js/jquery/ui/widget.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='/wp-includes/js/jquery/ui/position.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='/wp-includes/js/jquery/ui/mouse.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='/wp-includes/js/jquery/ui/sortable.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='/wp-includes/js/jquery/ui/datepicker.min.js?ver=1.11.4'></script>
<script type='text/javascript'>
	jQuery(document).ready(function(jQuery){jQuery.datepicker.setDefaults({"closeText":"\u0417\u0430\u043a\u0440\u0438\u0442\u0438","currentText":"\u0421\u044c\u043e\u0433\u043e\u0434\u043d\u0456","monthNames":["\u0421\u0456\u0447\u0435\u043d\u044c","\u041b\u044e\u0442\u0438\u0439","\u0411\u0435\u0440\u0435\u0437\u0435\u043d\u044c","\u041a\u0432\u0456\u0442\u0435\u043d\u044c","\u0422\u0440\u0430\u0432\u0435\u043d\u044c","\u0427\u0435\u0440\u0432\u0435\u043d\u044c","\u041b\u0438\u043f\u0435\u043d\u044c","\u0421\u0435\u0440\u043f\u0435\u043d\u044c","\u0412\u0435\u0440\u0435\u0441\u0435\u043d\u044c","\u0416\u043e\u0432\u0442\u0435\u043d\u044c","\u041b\u0438\u0441\u0442\u043e\u043f\u0430\u0434","\u0413\u0440\u0443\u0434\u0435\u043d\u044c"],"monthNamesShort":["\u0421\u0456\u0447","\u041b\u044e\u0442","\u0411\u0435\u0440","\u041a\u0432\u0456","\u0422\u0440\u0430","\u0427\u0435\u0440","\u041b\u0438\u043f","\u0421\u0435\u0440","\u0412\u0435\u0440","\u0416\u043e\u0432","\u041b\u0438\u0441","\u0413\u0440\u0443"],"nextText":"\u041d\u0430\u0441\u0442\u0443\u043f\u043d.","prevText":"\u041f\u043e\u043f\u0435\u0440\u0435\u0434\u043d\u0456\u0439","dayNames":["\u041d\u0435\u0434\u0456\u043b\u044f","\u041f\u043e\u043d\u0435\u0434\u0456\u043b\u043e\u043a","\u0412\u0456\u0432\u0442\u043e\u0440\u043e\u043a","\u0421\u0435\u0440\u0435\u0434\u0430","\u0427\u0435\u0442\u0432\u0435\u0440","\u041f\u2019\u044f\u0442\u043d\u0438\u0446\u044f","\u0421\u0443\u0431\u043e\u0442\u0430"],"dayNamesShort":["\u041d\u0434","\u041f\u043d","\u0412\u0442","\u0421\u0440","\u0427\u0442","\u041f\u0442","\u0421\u0431"],"dayNamesMin":["\u041d\u0434","\u041f\u043d","\u0412\u0442","\u0421\u0440","\u0427\u0442","\u041f\u0442","\u0421\u0431"],"dateFormat":"MM d, yy","firstDay":1,"isRTL":false});});
</script>
<script type='text/javascript' src='/wp-includes/js/jquery/ui/menu.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='/wp-includes/js/wp-a11y.min.js?ver=4.7.5'></script>
<script type='text/javascript'>
	/* <![CDATA[ */
	var uiAutocompleteL10n = {"noResults":"\u041d\u0456\u0447\u043e\u0433\u043e \u043d\u0435 \u0437\u043d\u0430\u0439\u0434\u0435\u043d\u043e.","oneResult":"\u0417\u043d\u0430\u0439\u0434\u0435\u043d\u043e 1 \u0440\u0435\u0437\u0443\u043b\u044c\u0442\u0430\u0442. \u0412\u0438\u043a\u043e\u0440\u0438\u0441\u0442\u043e\u0432\u0443\u0439\u0442\u0435 \u043a\u043b\u0430\u0432\u0456\u0448\u0456 \u0437\u0456 \u0441\u0442\u0440\u0456\u043b\u043a\u0430\u043c\u0438 \u0432\u0433\u043e\u0440\u0443 \u0442\u0430 \u0432\u043d\u0438\u0437 \u0434\u043b\u044f \u043f\u0435\u0440\u0435\u043c\u0456\u0449\u0435\u043d\u043d\u044f.","manyResults":"\u0417\u043d\u0430\u0439\u0434\u0435\u043d\u043e \u0440\u0435\u0437\u0443\u043b\u044c\u0442\u0430\u0442\u0456\u0432: %d. \u0412\u0438\u043a\u043e\u0440\u0438\u0441\u0442\u043e\u0432\u0443\u0439\u0442\u0435 \u043a\u043b\u0430\u0432\u0456\u0448\u0456 \u0437\u0456 \u0441\u0442\u0440\u0456\u043b\u043a\u0430\u043c\u0438 \u0432\u0433\u043e\u0440\u0443 \u0442\u0430 \u0432\u043d\u0438\u0437 \u0434\u043b\u044f \u043f\u0435\u0440\u0435\u043c\u0456\u0449\u0435\u043d\u043d\u044f.","itemSelected":"\u041e\u0431'\u0454\u043a\u0442 \u043e\u0431\u0440\u0430\u043d\u0438\u0439."};
	/* ]]> */
</script>
<script type='text/javascript' src='/wp-includes/js/jquery/ui/autocomplete.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='/wp-includes/js/jquery/ui/resizable.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='/wp-includes/js/jquery/ui/draggable.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='/wp-includes/js/jquery/ui/button.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='/wp-includes/js/jquery/ui/dialog.min.js?ver=1.11.4'></script>
<script type='text/javascript'>
	/* <![CDATA[ */
	var EM = {"ajaxurl":"https:\/\/epo.org.ua\/wp-admin\/admin-ajax.php","locationajaxurl":"https:\/\/epo.org.ua\/wp-admin\/admin-ajax.php?action=locations_search","firstDay":"1","locale":"uk","dateFormat":"dd.mm.yy","ui_css":"https:\/\/epo.org.ua\/wp-content\/plugins\/events-manager\/includes\/css\/jquery-ui.css","show24hours":"1","is_ssl":"","google_maps_api":"AIzaSyBNST5jZvGkfxoR3q8pGWSi5PLBv7_8p-4","txt_search":"\u041f\u043e\u0448\u0443\u043a","txt_searching":"Searching...","txt_loading":"Loading..."};
	/* ]]> */
</script>
<script type='text/javascript' src='/wp-content/plugins/events-manager/includes/js/events-manager.js?ver=5.6624'></script>
<script type='text/javascript'>
	/* <![CDATA[ */
	var pirateFormsObject = {"errors":""};
	/* ]]> */
</script>
<script type='text/javascript' src='/wp-content/plugins/pirate-forms/js/scripts-general.js?ver=4.7.5'></script>
<!--[if lt IE 9]>
<script type='text/javascript' src='/wp-content/themes/EPO/js/html5.js?ver=4.7.5'></script>
<![endif]-->
<script type='text/javascript' src='/wp-content/themes/EPO/js/jquery.bxslider.js?ver=4.7.5'></script>
<script type='text/javascript' src='/wp-content/themes/EPO/js/jquery-custom.js?ver=4.7.5'></script>
<link rel='https://api.w.org/' href='/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 4.7.5" />
<link rel="canonical" href="/" />
<link rel='shortlink' href='/' />
<link rel="alternate" type="application/json+oembed" href="/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fepo.org.ua%2F" />
<link rel="alternate" type="text/xml+oembed" href="/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fepo.org.ua%2F&#038;format=xml" />
<meta property="fb:pages" content="1617490128568416" />
<script>var ms_grabbing_curosr = '/wp-content/plugins/master-slider/public/assets/css/common/grabbing.cur', ms_grab_curosr = '/wp-content/plugins/master-slider/public/assets/css/common/grab.cur';</script>
<meta name="generator" content="MasterSlider 2.9.8 - Responsive Touch Image Slider | www.avt.li/msf" />
<style type="text/css" media="print">#wpadminbar { display:none; }</style>
<style type="text/css" media="screen"></style>
<link rel="icon" href="/wp-content/uploads/2017/03/favicon-32x32.jpg" sizes="32x32" />
<link rel="icon" href="/wp-content/uploads/2017/03/favicon-192x192.jpg" sizes="192x192" />
<link rel="apple-touch-icon-precomposed" href="/wp-content/uploads/2017/03/favicon-180x180.jpg" />
<meta name="msapplication-TileImage" content="/wp-content/uploads/2017/03/favicon.jpg" />
<!-- versions: WordPress 4.7.5 | Strong Testimonials 2.21.2 -->


</head>




<body class="home page-template-default page page-id-4 logged-in admin-bar no-customize-support wp-custom-logo _masterslider _ms_version_2.9.8 group-blog" >



	<div id="mobilebgfix">
		<div class="mobile-bg-fix-img-wrap">
			<div class="mobile-bg-fix-img"></div>
		</div>
		<div class="mobile-bg-fix-whole-site">


			<header id="home" class="header" itemscope="itemscope" itemtype="http://schema.org/WPHeader">

				<div id="main-nav" class="navbar navbar-inverse bs-docs-nav" role="banner">

					<div class="container">

						<div class="navbar-header responsive-logo">

							<div class="langsm">
								<a href="/"><div class="lang ua-lang"></div></a>
								<div class="lang en-lang"></div>
							</div>

							<div class="navbar-brand">

								<a href="/eng" class="custom-logo-link" rel="home" itemprop="url"><img width="2048" height="931" src="/wp-content/uploads/2017/03/logo_EPO_tiff3s.png" class="custom-logo" alt="" itemprop="logo" srcset="/wp-content/uploads/2017/03/logo_EPO_tiff3s.png 2048w, /wp-content/uploads/2017/03/logo_EPO_tiff3s-300x136.png 300w, /wp-content/uploads/2017/03/logo_EPO_tiff3s-768x349.png 768w, /wp-content/uploads/2017/03/logo_EPO_tiff3s-1024x466.png 1024w" sizes="(max-width: 2048px) 100vw, 2048px" /></a>
							</div> <!-- /.navbar-brand -->

							<div class="head_stripe"><h2>Supporting businesses in navigating global markets</h2></div>

						</div> <!-- /.navbar-header -->

						<div class="langs">
							<a href="/"><div class="lang ua-lang"></div></a>
							<div class="lang en-lang"></div>
						</div>


					</div> <!-- /.container -->

				</div> <!-- /#main-nav -->
				<!-- / END TOP BAR -->
				<div class="home-header-slider">

					<!-- MasterSlider -->
					<?php masterslider(2); ?>

				</div>
				<!-- END MasterSlider -->

			</div>
		</header> <!-- / END HOME SECTION  -->
		<div id="content" class="site-content">


			<section class="focus" id="focus" style="background: #ffffff">


				<div class="container">

					<!-- SECTION HEADER -->

					<div class="section-header">

						<!-- SECTION TITLE AND SUBTITLE -->

						<h2 class="dark-text">DISCOVER NEW OPPORTUNITIES WITH EPO </h2>
					</div>

					<div class="row">

						<span id="ctup-ads-widget-1" class="widget-ourfocus col-md-4" data-scrollreveal="enter left after 0.15s over 1s">
							<div class="serv-icon" style="background:url(/wp-content/uploads/2017/03/1.png) no-repeat center;">
							</div>
							<p class="serv-name">EXPAND TO GLOBAL MARKETS</p>
							<p class="serv-description">We can help your business navigate the complexities of global markets</p>
						</span>

						<span id="ctup-ads-widget-2" class="widget-ourfocus col-md-4" data-scrollreveal="enter left after 0.15s over 1s">
							<div class="serv-icon" style="background:url(/wp-content/uploads/2017/03/2.png) no-repeat center;">
							</div>
							<p class="serv-name">CONNECT YOUR BUSINESS</p>
							<p class="serv-description">Join EPO Exporters Network</p>
						</span>

						<span id="ctup-ads-widget-3" class="widget-ourfocus col-md-4" data-scrollreveal="enter left after 0.15s over 1s">
							<div class="serv-icon" style="background:url(/wp-content/uploads/2017/03/3.png) no-repeat center;">
							</div>
							<p class="serv-name">ACCESS MARKET INTELLIGENCE</p>
							<p class="serv-description">We provide key market and industry analysis and insights of priority export areas</p>
						</span>

						<span id="ctup-ads-widget-4" class="widget-ourfocus col-md-4" data-scrollreveal="enter left after 0.15s over 1s">
							<div class="serv-icon" style="background:url(/wp-content/uploads/2017/03/4.png) no-repeat center;">
							</div>
							<p class="serv-name">RECEIVE SOURCING INFORMATION</p>
							<p class="serv-description">We search for Ukrainian partners on behalf of international firms looking for suppliers</p>
						</span>

						<span id="ctup-ads-widget-5" class="widget-ourfocus col-md-4" data-scrollreveal="enter left after 0.15s over 1s">
							<div class="serv-icon" style="background:url(/wp-content/uploads/2017/03/5.png) no-repeat center;">
							</div>
							<p class="serv-name">DISCOVER NEW OPPORTUNITIES</p>
							<p class="serv-description">Open the doors to new export opportunities with our trade missions, exhibitions, forums</p>
						</span>

						<span id="ctup-ads-widget-6" class="widget-ourfocus col-md-4" data-scrollreveal="enter left after 0.15s over 1s">
							<div class="serv-icon" style="background:url(/wp-content/uploads/2017/03/6.png) no-repeat center;">
							</div>
							<p class="serv-name">FIND KEY CONTACTS</p>
							<p class="serv-description">We help you to find your business partner abroad/ in Ukraine and get practical advices to internationalize your business</p>
						</span>

						<span id="ctup-ads-widget-7" class="widget-ourfocus col-md-4" data-scrollreveal="enter left after 0.15s over 1s">
							<div class="serv-icon" style="background:url(/wp-content/uploads/2017/03/7.png) no-repeat center;">
							</div>
							<p class="serv-name">UPGRADE YOUR EXPORT SKILLS</p>
							<p class="serv-description">Our Educational platform helps to improve your export expertise through series of seminars, webinars and workshops</p>
						</span>

						<span id="ctup-ads-widget-8" class="widget-ourfocus col-md-4" data-scrollreveal="enter left after 0.15s over 1s">
							<div class="serv-icon" style="background:url(/wp-content/uploads/2017/03/8.png) no-repeat center;">
							</div>
							<p class="serv-name">PARTICIPATE IN GLOBAL TENDERS</p>
							<p class="serv-description">EPO provides assistance and information on public procurements for over 45 counties all over the world</p>
						</span>

						<span id="ctup-ads-widget-9" class="widget-ourfocus col-md-4" data-scrollreveal="enter left after 0.15s over 1s">
							<div class="serv-icon" style="background:url(/wp-content/uploads/2017/03/9.png) no-repeat center;">
							</div>
							<p class="serv-name">COMMUNICATE ON B2G LEVEL</p>
							<p class="serv-description">Our B2G platform helps to conduct effective dialogue between your business, experts and the Government</p>
						</span>


					</div>

				</div> <!-- / END CONTAINER -->


			</section>  <!-- / END FOCUS SECTION -->



			<section class="about-us" id="aboutus">


				<div class="container">

					<!-- SECTION HEADER -->

					<div class="section-header">


					</div><!-- / END SECTION HEADER -->

					<!-- 3 COLUMNS OF ABOUT US-->

					<div class="row">

						<!-- COLUMN 1 - BIG MESSAGE ABOUT THE COMPANY-->


					</div> <!-- / END 3 COLUMNS OF ABOUT US-->

					<!-- CLIENTS -->
					<div class="our-clients"><h5><span class="section-footer-title"><h2>WE ARE SUPPORTED BY</h2></span></h5></div><div class="client-list"><div data-scrollreveal="enter right move 60px after 0.00s over 2.5s"><span id="zerif_clients-widget-9">
					<a target="_blank" href="http://www.me.gov.ua/">
						<img src="/wp-content/uploads/2017/06/ministryLogoBlackEngl.png" alt="Client">			</a>

					</span><span id="zerif_clients-widget-3">
					<a target="_blank" href="http://www.wnisef.org/">
						<img src="/wp-content/uploads/2017/03/WNISEF-1-300x141.png" alt="Client">			</a>

					</span><span id="zerif_clients-widget-5">
					<a target="_blank" href="http://edge.in.ua/">
						<img src="/wp-content/uploads/2017/03/EDGE_Logo_CMYK_final-1.png" alt="Client">			</a>

					</span><span id="zerif_clients-widget-11">
					<a target="_blank" href="https://cutisproject.org/ua">
						<img src="/wp-content/uploads/2017/03/CUTIS-2.png" alt="Client">			</a>

					</span><span id="zerif_clients-widget-12">
					<a target="_blank" href="https://www.giz.de">
						<img src="/wp-content/uploads/2017/03/logo_giz_620px.940x379-1-300x121.png" alt="Client">			</a>

					</span><span id="zerif_clients-widget-14">
					<a target="_blank" href="http://fsr.org.ua/">
						<img src="/wp-content/uploads/2017/06/Fond-logo_en.png" alt="Client">			</a>

					</span></div></div> 
				</div> <!-- / END CONTAINER -->


			</section> <!-- END ABOUT US SECTION -->


		</div><!-- .site-content -->


		<footer id="footer" itemscope="itemscope" itemtype="http://schema.org/WPFooter">


			<div class="container">


				<div class="col-md-3 company-details"><div class="icon-top red-text"><img src="/wp-content/uploads/2017/03/82a9634175e97c1feeab23ffd84718b21.png" alt="" /></div><div class="zerif-footer-address">Ukraine, 01008, Kyiv,<br />
				Grushevskogo 12/2, 307</div></div><div class="col-md-3 company-details"><div class="icon-top green-text"><img src="/wp-content/uploads/2017/03/e-mail.png" alt="" /></div><div class="zerif-footer-email">info@epo.org.ua<br />
				export@epo.org.ua
			</div></div><div class="col-md-3 company-details"><div class="icon-top blue-text"><img src="/wp-content/uploads/2017/03/tel.png" alt="" /></div><div class="zerif-footer-phone">+38 044 253 61 31
		</div></div><div class="col-md-3 copyright"><ul class="social"><li id="facebook"><a target="_blank" href="https://www.facebook.com/exportpromotionoffice/"><span class="sr-only">Facebook link</span> <i class="fa fa-facebook"></i></a></li><li id="twitter"><a target="_blank" href="https://twitter.com/EPOUkr"><span class="sr-only">Twitter link</span> <i class="fa fa-twitter"></i></a></li><li id="linkedin"><a target="_blank" href="https://www.linkedin.com/company-beta/17997794/"><span class="sr-only">Linkedin link</span> <i class="fa fa-linkedin"></i></a></li></ul><!-- .social --><p id="zerif-copyright">© 2017 Export Promotion Office</p></div>			</div> <!-- / END CONTAINER -->

	</footer> <!-- / END FOOOTER  -->


</div><!-- mobile-bg-fix-whole-site -->
</div><!-- .mobile-bg-fix-wrap -->


<!-- wp_footer called -->
<link rel='stylesheet' id='wpmtst-custom-style-css'  href='/wp-content/plugins/strong-testimonials/public/css/custom.css?ver=4.7.5' type='text/css' media='all' />
<link rel='stylesheet' id='testimonials-no-quotes-content-css'  href='/wp-content/plugins/strong-testimonials/templates/no-quotes/content.css?ver=2.21.2' type='text/css' media='all' />
<link rel='stylesheet' id='wpmtst-font-awesome-css'  href='/wp-content/plugins/strong-testimonials/public/fonts/font-awesome-4.6.3/css/font-awesome.min.css?ver=4.6.3' type='text/css' media='all' />
<link rel='stylesheet' id='wpmtst-slider-controls-sides-buttons-pager-buttons-css'  href='/wp-content/plugins/strong-testimonials/public/css/slider-controls-sides-buttons-pager-buttons.css?ver=2.21.2' type='text/css' media='all' />
<script type='text/javascript' src='/wp-includes/js/admin-bar.min.js?ver=4.7.5'></script>
<script type='text/javascript' src='/wp-content/plugins/contact-form-7/includes/js/jquery.form.min.js?ver=3.51.0-2014.06.20'></script>
<script type='text/javascript'>
	/* <![CDATA[ */
	var _wpcf7 = {"recaptcha":{"messages":{"empty":"\u0411\u0443\u0434\u044c \u043b\u0430\u0441\u043a\u0430, \u043f\u0456\u0434\u0442\u0432\u0435\u0440\u0434\u0456\u0442\u044c, \u0449\u043e \u0432\u0438 \u043d\u0435 \u0440\u043e\u0431\u043e\u0442."}}};
	/* ]]> */
</script>
<script type='text/javascript' src='/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=4.7'></script>
<script type='text/javascript' src='/wp-content/themes/EPO/js/bootstrap.min.js?ver=20120206'></script>
<script type='text/javascript' src='/wp-content/themes/EPO/js/jquery.knob.js?ver=20120206'></script>
<script type='text/javascript' src='/wp-content/themes/EPO/js/smoothscroll.js?ver=20120206'></script>
<script type='text/javascript' src='/wp-content/themes/EPO/js/scrollReveal.js?ver=20120206'></script>
<script type='text/javascript' src='/wp-content/themes/EPO/js/zerif.js?ver=20120206'></script>
<script type='text/javascript' src='/wp-includes/js/wp-embed.min.js?ver=4.7.5'></script>
<script type='text/javascript' src='/wp-content/plugins/master-slider/public/assets/js/jquery.easing.min.js?ver=2.9.8'></script>
<script type='text/javascript' src='/wp-content/plugins/master-slider/public/assets/js/masterslider.min.js?ver=2.9.8'></script>
<script type='text/javascript' src='/wp-content/plugins/strong-testimonials/public/js/lib/actual/jquery.actual.js?ver=4.7.5'></script>
<script type='text/javascript' src='/wp-content/plugins/strong-testimonials/public/js/lib/wpmslider/jquery.wpmslider.min.js?ver=2.21.2'></script>
<script type='text/javascript' src='/wp-includes/js/underscore.min.js?ver=1.8.3'></script>
<script type='text/javascript' src='/wp-content/plugins/strong-testimonials/public/js/jquery.strongslider.js?ver=2.21.2'></script>
<script type='text/javascript'>
	/* <![CDATA[ */
	var strong_slider_id_1 = {"mode":"horizontal","speed":"1000","pause":"3000","autoHover":"1","autoStart":"1","stopAutoOnClick":"1","adaptiveHeight":"0","adaptiveHeightSpeed":"500","controls":"1","autoControls":"0","pager":"0","stretch":"0","prevText":"","nextText":"","startText":"","stopText":""};
	/* ]]> */
</script>
<script type='text/javascript' src='/wp-content/plugins/strong-testimonials/public/js/slider.js?ver=2.21.2'></script>
	<!--[if lte IE 8]>
		<script type="text/javascript">
			document.body.className = document.body.className.replace( /(^|\s)(no-)?customize-support(?=\s|$)/, '' ) + ' no-customize-support';
		</script>
		<![endif]-->
		<!--[if gte IE 9]><!-->
		<script type="text/javascript">
			(function() {
				var request, b = document.body, c = 'className', cs = 'customize-support', rcs = new RegExp('(^|\\s+)(no-)?'+cs+'(\\s+|$)');

				request = true;

				b[c] = b[c].replace( rcs, ' ' );
				// The customizer requires postMessage and CORS (if the site is cross domain)
				b[c] += ( window.postMessage && request ? ' ' : ' no-' ) + cs;
			}());
		</script>
		<!--<![endif]-->




	</body>

	</html>