<?php zerif_before_our_focus_trigger(); ?>

<?php
	if(get_fields(4)['services'][0]['background_image'] != NULL) {
		$background = 'url('.get_fields(4)['services'][0]['background_image'].')';
	} elseif(get_fields(4)['services'][0]['background'] != NULL) {
		$background = get_fields(4)['services'][0]['background'];
	} else {
		$background = FALSE;
	}
?>

<section class="focus" id="focus" style="background: <?php echo $background; ?>">

	<?php zerif_top_our_focus_trigger(); ?>

	<div class="container">

		<!-- SECTION HEADER -->

<div class="consalting_step">
<h2 class="dark-text">Комплексна допомога для виходу на нові ринки</h2>
		<div class=desctop-show>
		<a class="step" href="/poslugy-eksporteram/konsaltyng/">

		<img class="raz" src="/wp-content/uploads/2017/03/con1.png">
			<img class="dva" src="/wp-content/uploads/2017/03/con1.1.png">
			<div></div>
		</a>

		<a class="step" href="/poslugy-eksporteram/konsaltyng/" style="margin-left: 25%;">
			<img class="raz" src="/wp-content/uploads/2017/03/con2.png">
			<img class="dva" src="/wp-content/uploads/2017/03/con2.1.png">
			<div></div>
		</a>

		<a class="step" href="/poslugy-eksporteram/konsaltyng/" style="margin-left: 50%;">
			<img class="raz" src="/wp-content/uploads/2017/03/con3.png">
			<img class="dva" src="/wp-content/uploads/2017/03/con3.1.png">
			<div></div>
		</a>

		<a class="step" href="/poslugy-eksporteram/konsaltyng/" style="margin-left: 75%;">
			<img class="raz" src="/wp-content/uploads/2017/03/con4.png">
			<img class="dva" src="/wp-content/uploads/2017/03/con4.1.png">
			<div></div>
		</a>
		</div>

		<div class=mobile-show>
		<a class="step" href="/poslugy-eksporteram/konsaltyng/">

		<img class="raz" src="/wp-content/uploads/2017/03/vcon1.png">
			<img class="dva" src="/wp-content/uploads/2017/03/vcon1.1.png">
			<div></div>
		</a>

		<a class="step" href="/poslugy-eksporteram/konsaltyng/">
			<img class="raz" src="/wp-content/uploads/2017/03/vcon2.png">
			<img class="dva" src="/wp-content/uploads/2017/03/vcon2.1.png">
			<div></div>
		</a>

		<a class="step" href="/poslugy-eksporteram/konsaltyng/">
			<img class="raz" src="/wp-content/uploads/2017/03/vcon3.png">
			<img class="dva" src="/wp-content/uploads/2017/03/vcon3.1.png">
			<div></div>
		</a>

		<a class="step" href="/poslugy-eksporteram/konsaltyng/">
			<img class="raz" src="/wp-content/uploads/2017/03/vcon4.png">
			<img class="dva" src="/wp-content/uploads/2017/03/vcon4.1.png">
			<div></div>
		</a>
		</div>
	</div>
		<div class="section-header">

			<!-- SECTION TITLE AND SUBTITLE -->

			<?php

			/* Title */
			zerif_our_focus_header_title_trigger();

			/* Subtitle */
			zerif_our_focus_header_subtitle_trigger();

			?>

		</div>

		<div class="row">

				<?php
				if ( is_active_sidebar( 'sidebar-ourfocus' ) ) {

					dynamic_sidebar( 'sidebar-ourfocus' );

				} elseif ( current_user_can( 'edit_theme_options' ) && ! defined( 'THEMEISLE_COMPANION_VERSION' ) ) {

					if ( is_customize_preview() ) {
						printf( __( 'You need to install the %s plugin to be able to add Team members, Testimonials, Our Focus and Clients widgets.','zerif-lite' ), 'ThemeIsle Companion' );
					} else {
						printf( __( 'You need to install the %s plugin to be able to add Team members, Testimonials, Our Focus and Clients widgets.','zerif-lite' ), sprintf( '<a href="%1$s" class="zerif-default-links">%2$s</a>', esc_url( wp_nonce_url( self_admin_url( 'update.php?action=install-plugin&plugin=themeisle-companion' ), 'install-plugin_themeisle-companion' ) ), 'ThemeIsle Companion' ) );
					}

				}
				?>

		</div>

	</div> <!-- / END CONTAINER -->

	<?php zerif_bottom_our_focus_trigger(); ?>

</section>  <!-- / END FOCUS SECTION -->

<?php zerif_after_our_focus_trigger(); ?>