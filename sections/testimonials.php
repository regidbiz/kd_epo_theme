<?php

zerif_before_testimonials_trigger();

echo '<section class="testimonial" id="testimonials">';

	zerif_top_testimonials_trigger();

	echo '<div class="container">';

		echo '<div class="section-header">';

			/* Title */
			zerif_testimonials_header_title_trigger();

			/* Subtitle */
			zerif_testimonials_header_subtitle_trigger();

		echo '</div>';

		echo '<div class="row" data-scrollreveal="enter right after 0s over 1s">';

		echo do_shortcode("[testimonial_view id=1]");		

		echo '</div>';

	

	echo '</div>';

	zerif_bottom_testimonials_trigger();

echo '</section>';

zerif_after_testimonials_trigger();

?>