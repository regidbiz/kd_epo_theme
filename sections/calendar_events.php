<?php if ( is_active_sidebar( 'sidebar-calendar-events' ) ) : ?>

<section class="sidebar-calendar-events" id="sidebar-calendar-events">
	<div class="container">

		<!-- SECTION HEADER -->

		<div class="section-header">

			<h2>Календар подій</h2>


		</div>


		<?php dynamic_sidebar( 'sidebar-calendar-events' ); ?>
	</div>
</section>

<?php endif; ?>