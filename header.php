<?php
/**
 * The Header for our theme.
 * Displays all of the <head> section and everything up till <div id="content">
 */
?><html <?php language_attributes(); ?>>

<head>

	<?php zerif_top_head_trigger(); ?>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/css/plugin.css">
	<?php wp_head(); ?>

	<?php zerif_bottom_head_trigger(); ?>
</head>                           
<?php if(isset($_POST['scrollPosition'])): ?>

	<body <?php body_class(); ?> onLoad="window.scrollTo(0,<?php echo intval($_POST['scrollPosition']); ?>)">
	<?php do_action( 'body_open' ); ?>
	<?php else: ?>

		<body <?php body_class(); ?> >

		<?php endif;

		zerif_top_body_trigger();

		/* Preloader */

		if(is_front_page() && !is_customize_preview() && get_option( 'show_on_front' ) != 'page' ):

			$zerif_disable_preloader = get_theme_mod('zerif_disable_preloader');
		
		if( isset($zerif_disable_preloader) && ($zerif_disable_preloader != 1)):
			echo '<div class="preloader">';
		echo '<div class="status">&nbsp;</div>';
		echo '</div>';
		endif;	

		endif; ?>


		<div id="mobilebgfix">
			<div class="mobile-bg-fix-img-wrap">
				<div class="mobile-bg-fix-img"></div>
			</div>
			<div class="mobile-bg-fix-whole-site">


				<header id="home" class="header" itemscope="itemscope" itemtype="http://schema.org/WPHeader">
					<div class="header_overflow"></div>
					<div id="main-nav" class="navbar navbar-inverse bs-docs-nav" role="banner">

						<div class="container">

							<div class="navbar-header responsive-logo">

							<div class="langsm">
									<div class="lang ua-lang"></div>
									<a href="/eng/"><div class="lang en-lang"></div></a>
								</div>

								<button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">

									<span class="sr-only"><?php _e('Toggle navigation','zerif-lite'); ?></span>

									<span class="icon-bar"></span>

									<span class="icon-bar"></span>

									<span class="icon-bar"></span>

	
									<div><span class="navbar_toggle_name">Меню сайту</span></div>

								</button>
								<!--<span class="navbar_toggle_name">Закрити меню</span>-->


								<!--<button id="nav-icon2" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
									<span></span>
									<span></span>
									<span></span>
									<span></span>
									<span></span>
									<span></span>
								</button>-->

								<div class="navbar-brand">

									<?php

									if( has_custom_logo() ) {

										the_custom_logo();

									} else {

										?>
										<div class="site-title-tagline-wrapper">
											<h1 class="site-title">
												<a href=" <?php echo esc_url( home_url( '/' ) ) ?> ">
													<?php bloginfo( 'title' ) ?>
												</a>
											</h1>

											<?php

											$description = get_bloginfo( 'description', 'display' );

											if ( ! empty( $description ) ) : ?>

											<p class="site-description">

												<?php echo $description; ?>

											</p> <!-- /.site-description -->

										<?php elseif( is_customize_preview() ): ?>

											<p class="site-description"></p>

										<?php endif; ?>

									</div> <!-- /.site-title-tagline-wrapper -->

									<?php } ?>

								</div> <!-- /.navbar-brand -->

							</div> <!-- /.navbar-header -->


							<div class="langs">
								<div class="lang ua-lang"></div>
								<a href="/eng/"><div class="lang en-lang"></div></a>
							</div>
							<span class="phone_number_in_mob_menu"></span>


							<!--<div class="header__search">
								<?php 
								if ( function_exists('dynamic_sidebar') ) {
									dynamic_sidebar('header-saerch');
								}
								?>
								<span class="header__search-icon search-icon">
									<img class="search-icon__img" src="<?php bloginfo('template_url') ?>/images/musica-searcher.svg" alt="">
								</span>
							</div>

							<?php zerif_primary_navigation_trigger(); ?>-->

							<?php zerif_primary_navigation_trigger(); ?>

							<div class="header__search">
								<?php 
								if ( function_exists('dynamic_sidebar') ) {
									dynamic_sidebar('header-saerch');
								}
								?>
								<span class="header__search-icon search-icon">
									<img class="search-icon__img" src="<?php print get_stylesheet_directory_uri(); ?>/images/search_icon.svg" alt="">
								</span>
							</div>	

							<div class="user_autorization_form_wrapp">
								<div class="autorization_inputs_wrapp">
									<?php if(!is_user_logged_in()){ ?>
										<input type="button" class="enter_autorization" value="Вхід">/
										<input type="button" class="registration_autorization" value="Реєстрація">
									<?php }else{ ?>
										<a href="<?php print get_home_url(); ?>/epo_profile/dashboard/" class="registration_profile">Перейти в кабінет</a>/
										<a href="<?php echo wp_logout_url( home_url() ); ?>" class="registration_profile_logout">Вихід</a>
									<?php } ?>
								</div>
								<?php if(!is_user_logged_in()){ ?>
									<div class="user_autorization_photo" style='background-image: url("<?php print get_stylesheet_directory_uri(); ?>/images/user2.svg ?>");background-size:cover; background-repeat:no-repeat; background-position: center'></div>
								<?php }else{ ?>
									<a href="<?php print get_home_url(); ?>/epo_profile/dashboard/"><div class="user_autorization_photo" style='background-image: url("<?php echo esc_url( get_avatar_url( get_current_user_id() ) ); ?>");background-size:cover; background-repeat:no-repeat; background-position: center'></div></a>
								<?php } ?>	
								<div class="sign_in_popup">	
									<div class="sign_in_popup_title">Вхід в особистий кабінет <img class="close_autorization_popup" src="<?php print get_stylesheet_directory_uri(); ?>/images/close_button.svg" alt=""></div>
									<div class="autorization_tatus"></div>
									<form action="" method="post" class="header_profile_login_form">
										<label for="autorization_email">E-mail</label>
										<input type="email" id="autorization_email" name="autorization_email" required>
										<label for="autorization_password">Пароль</label>
										<div class="autorization_password_input_wrapp">
											<input type="password" id="autorization_password" name="autorization_password" required>										
											<i class="fa fa-eye-slash" aria-hidden="true"></i>
											<i class="fa fa-eye" aria-hidden="true"></i>
										</div>
										<span class="autorization_forgot_password">Забули пароль?</span>
										<input type="submit" value="Увійти">
									</form>
								</div>	
								<div class="forgot_password_in_popup">	
									<div class="sign_in_popup_title"><img src="<?php print get_stylesheet_directory_uri(); ?>/images/back-button-circular-left-arrow-symbol.svg" alt="" class="forgot_password_in_popup_back">Відновлення паролю<img class="close_autorization_popup" src="<?php print get_stylesheet_directory_uri(); ?>/images/close_button.svg" alt=""></div>
									<span class="repair_password">Введіть e-mail адресу вказану при реєстрації</span>
									<div class="autorization_tatus"></div>
									<form action="" method="post" class="header_profile_login_remind">
										<label for="forgot_pass_email">E-mail</label>
										<input type="email" id="forgot_pass_email" name="forgot_pass_email" required>
										<input type="submit" value="Відновити пароль">
									</form>
								</div>
								<div class="registration_on_epo">	
									<div class="sign_in_popup_title">Створення кабінету експортера<img class="close_autorization_popup" src="<?php print get_stylesheet_directory_uri(); ?>/images/close_button.svg" alt=""></div>
									<div class="autorization_tatus"></div>
									<form action="" method="post" class="header_profile_login_registration">
										<label for="reg_name">П.І.Б.</label>
										<input type="text" id="reg_name" name="reg_name" placeholder="напр. Сергій Петренко" required>
										<label for="registration_password">E-mail</label>									
										<input type="email" id="registration_password" name="registration_password" placeholder="mail@example.com" required>
										<input type="submit" value="Створити особистий кабінет">
									</form>
								</div>
							</div>	

						</div> <!-- /.container -->

					</div> <!-- /#main-nav -->

	<!-- / END TOP BAR -->
	<div class="autorization_overflow"></div>